# Planetary Sound Machine (PSM) #

Author: *jt@jtbullitt.com*

Last revised: __2025.01.20__ (a work in progress)
 
---
# 0.0 What PSM is #

Planetary Sound Machine (PSM) is a suite of Macintosh command-line utilities designed to collect seismic and other geophysical data from existing sources around the world; transform those signals into audio; stream that audio to the Internet; deliver associated metadata (spectrograms, event lists, state-of-health info, etc.) to a website; and perform autonomous state-of-health checks. It can run unattended for long periods of time on "headless" Macs (no monitor, keyboard, or mouse). You can even run it on Macs in the cloud using a service like [MacStadium](https://www.macstadium.com).

This repository contains most of the requisite software (including executables, libraries, and `bash` scripts) needed to run an instance of a PSM on a networked cluster of Mac computers that use the Apple Silicon M1 chip, with macOS 14 or better. PSM does not run on any other hardware or OS. In its basic configuration, it can generate and stream up to two audio channels (i.e., two mono or one stereo). To generate more than two channels on a single Mac, additional virtual audio devices must be created. The commercial program [Loopback](https://rogueamoeba.com/loopback/) works well for this purpose (and for rerouting audio through external hardware audio interfaces).

Included in this distribution are several thousand lines of `bash` shell scripts and a handful of precompiled binary executables. The C++ source code for those executables is not yet available on a public repository.

If you have any questions, please feel free to contact me (jt bullitt) at jt@jtbullitt.com . 

## 0.1 About this document ##

This document -- like the code itself -- is a work in process. The descriptions of the module functions are overviews only; other commands may be available that are not listed here. For a more comprehensive and up-to-date description of the commands, use the built-in help (e.g., `pod/help` , `slink/help`, etc.).

In the code examples below, the `$` symbol stands for the shell prompt character.

---

# 1 PSM Overview #

The latest version of PSM runs on any Mac that uses the Apple Silicon M1 chip, using macOS 15 ("Sequoia") or better. 

PSM consists of several **modules**, each of which performs a specific task. The **pod** module provides the tools needed to manage all the other modules. In fact, I'll often refer to a Mac with the PSM software installed as a "pod" in and of itself. The modules are all controlled from the command line. There is no GUI.

## 1.1 Introducing the modules ##

Each PSM **module** is dedicated to a particular function (e.g., data collection, streaming audio, uploading files, etc.) Each **module** has an associated config file, which you can edit with a simple text editor. The default configs contain ample documentation (comments) to get you started.

Each **pod** can serve one or more functions, according to which other **modules** have been activated. Upon installation of PSM, only the **pod** module is active and ready to run. You can activate the others as needed by editing the **pod** config file. 

These are the PSM modules:

* **pod** : controls the other modules on this computer
* **slink** : fetches data from a remote SeedLink server, stores data to a buffer on disk, performs periodic housekeeping to remove unneeded ("old") data
* **program** : collects seismic data from a directory, resamples it to audio, delivers an audio stream to a SHOUTCast server, and prepares metadata (spectra, event lists, SOH summaries, etc.) for dispatch to a web server.
* **dispatcher** : supervises pods on other computers by polling them periodically, monitoring their health status, uploading their generated data to a web server, etc.
* **rtmon** (under development):  monitors the status of background Ref Tek data-collection processes (e.g., `rtcc` or `rtpd`).
* **radio** (under development): plays a scheduled program of multiple audio feeds through an outboard MIDI-controlled (hardware) mixer to a single two-channel output.


## 1.1 What can **pods** do? ##

### 1.1.1 A pod can install and manage other modules  ###

The **pod** module must be installed before anything else useful can happen. Once installed, it can be used to create, start, stop, and manage other modules via simple command-line commands (`pod/new`,  `pod/start`, `pod/stop`, `pod/status`, etc.).

### 1.1.2 A pod can harvest data from remote data sources ###

With the **slink** module activated, a pod can download data from a remote SeedLink server (e.g., the [IRIS Data Management Center](https://ds.iris.edu/ds/nodes/dmc/services/seedlink/)) or from a [Raspberry Shake](https://raspberryshake.org/) connected to the local network. **slink** retains on disk a user-configurable retrospective buffer of data -- only what is necessary to generate the desired audio programs. Each **slink** module may be controlled from the command line using simple commands (`slink/start`,  `slink/stop`,  `slink/status`, etc.).

### 1.1.3 A pod can generate audio and stream it to a SHOUTcast server ###

The command `pod/start` can launch one or more concurrent audio **program**s. Each **program** runs an instance of the `bash` script `eLoop`, which resamples a window of seismic data to an audio file, plays that audio file, and generates associated spectrograms, seismic event lists, and related data files. The particulars of each audio program (data source, sample rate conversion, login credentials to a SHOUTcast server, etc.) are defined in a text-based config file -- one config per program. Each program may be controlled from the command line using simple commands (`foo/start`, `foo/stop`, `foo/status`, etc., where `foo` is the name of the particular audio program.).

To generate more than two audio channels on a single Mac, you must create one or more virtual audio devices, using a program.  Each pod has its own config file that identifies the programs it manages and specifies the destination url for the files generated by each of the pod's audio streams. Each pod may be controlled from the command line using simple commands (`pod/start`, `pod/stop`, `pod/status`, etc.).

### 1.1.4 A pod can deliver files to a website ###

As programs are running and streaming audio to a SHOUTcast server, they also generate spectrograms, seismic event lists, and other useful data files. A pod collects these files into an `sftp` queue for subsequent delivery, on demand, to a remote server -- typically, a website.


### 1.1.5 A pod can issue commands to other pods ###

If the **dispatcher** module is activated, a pod can periodically poll other pods -- whether they are on the local network or elsewhere, triggering them to send their data in an orderly fashion to a remote server (e.g., a website). Each **dispatcher** module may be controlled from the command line using simple commands (`dispatcher/start`,  `dispatcher/stop`,  `dispatcher/status`, etc.). 

---

# 2 Installation #

### 2.0 General considerations ###

You can install and run PSM on any Mac that uses the Apple Silicon M1 chip, using macOS 15 ("Sequoia") or better. Sadly, the software no longer works on older Macs.

These instructions assume that you're installing PSM on a factory-fresh Mac, and that you've already created a user with admin privileges. Everything that follows assumes that you're logged in as that user.


### 2.1 Initial server setup###

0. __Enable `ssh`.__ Open a Terminal window and run:
```
	$ ssh-keygen -t rsa
```
>Follow the prompts and then paste the public key of your local computer into the `authorized keys` file:
```
	$ vim ~/.ssh/authorized_keys
```
> Now you can `ssh` into the server without a username/password.

> __NOTE:__ For the pod to upload data to a remote website (using `sftp`), this server's public key must be pasted into the remote server's `authorized_keys` file. Likewise, for this server to be reachable by a dispatcher pod on another server, the dispatcher server's public key must be installed in this server's `authorized_keys` file.

From now on you can run and manage PSM remotely via `ssh`. 

Some of the following initial setup is much easier to do via Screen Sharing. We'll start with that first.

1. __Connect via Screen Sharing__.

2. __Set up an administrator user.__ Keep it simple: don't setup iCloud, Messages, Mail, etc.; do clean up the Dock; etc.

3. __Check if `git` is installed.__ Open a terminal window and do:
```bash
	$ git --version
	$ xcode-select: note: no developer tools were found at '/Applications/Xcode.app', requesting install. Choose an option in the dialog to download the command line developer tools.
	$
``` 
4. __Follow the instructions in the dialog box__ to install git. This make take awhile.

5. __Check `git` installation.__ When `git` finishes the installation you should see something like this:
```bash
	$ git --version
	git version 2.37.1 (Apple Git-137.1)
	$
```

6. __Install the `git` extension `git-lfs` (git Large File Storage)__. This allows you to download PSM's required binary executables. To see if you already have it, type `which git-lfs`. If you see a filename path to `git-lfs`, you're all set; if not, you'll need to launch Safari and  [download git-lfs](https://git-lfs.github.com/). To install `git-lfs`: Open a Terminal window, change to the directory `~/Downloads/git-lfs*` and run `sudo ./install.sh`. The `git-lfs` executable will be installed to `/usr/local/bin`. (If that directory is not in your shell's PATH variable, you should add it and export it now in your `.bashrc` or `.zshrc` file, depending on your shell.) Finally, you must activate `git-lfs`:
```
	$ git lfs install
	Git LFS initialized.
	$
```
7. <a id='install-loopback'>__Install Loopback.__</a> Point Safari to [Loopback's website](https://rogueamoeba.com/loopback/) and download the latest version of Loopback. Move Loopback from your `Downloads` folder to your `Applications` folder. Purchase a license. Launch Loopback. Allow the installation. Follow the instructions.  Register your copy (getting a license requires purchase). Enter the license key ("Loopback/License...").


8. __Unplug any HDMI devices__, including dummy plugs. These interfere with the virtual audio devices created by Loopback. If you're  on a remote server, you will need to ask for "remote hands" to perform this action. if you're on a remote server, you can tell if an HDMI device (or plug) is attached by running the `audiodev` command once you've installed the pod (see below).

> __NOTE:__ The presence of an audio device named `28E850` often indicates the presence of an HDMI dummy plug.

9. __Grant microphone access to Terminal__. Open System Settings > Privacy and Security >  Microphone and allow the microphone to access both ARK and the Terminal. Although the PSM doesn't actually use the microphone, permission must be granted for the virtual audio devices to work properly.

### 2.2 Clone a copy of PSM ###

It's a good idea to install PSM into its own dedicated directory -- let's call it `~/psm_project`. Open Terminal and clone a copy of PSM from the [BitBucket repo](https://bitbucket.org/jtbullitt/psm) into that directory:
```
	$ mkdir ~/psm_project
	$ cd ~/psm_project
	$ git clone https://bitbucket.org/jtbullitt/psm.git
	...
	$
```

### 2.3 Install `brew`  ###

Install `brew`:

```
	$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

### 2.4 Run the `brew` app installer ###

Run PSM's `brew` app installer:

```
	$ /bin/bash ~/psm_project/psm/sh/installBrew.sh
```


### 2.5 Install the **pod**  ###

Creating a fully functional PSM **pod** is straightforward:
```
	$ cd ~/psm_project
	$ psm/installpod
	...
	$
```
The installer creates some files and directories in your project directory. It also modifies your `~/.profile` and `~/.zshrc` by: (1) adding the environment variable `PSM_PROJECT` (the full path to `~/psm_project`); (2) modifying the `PATH` environment variable to include the path to the directory wherein the PSM binaries reside; and (3) adding the line `cd ~/psm_project` so that, upon logging in, you'll find yourself in the project directory. (You can safely delete that line if you prefer.)

To test the installation, run `pod/val verbose`. Any errors in the installation will show up there.

Once you quit your shell and launch a new one, all the PSM tools will be available to you. 

That's it!

---

# 3 Module quick-start guide #

You control a pod by logging into the Mac at a Terminal window or via `ssh` and issuing simple typed commands. For easy access to all the commands, it's handy to be in the `$PSM_PROJECT` directory (e.g., `~/psm_project/pod` in our example), from where you can type PSM commands such as `pod/start` or `dispatcher/status` etc.

## 3.1 Pod module ##

Each pod's controlling software resides in the directory `$PSM_PROJECT/pod` (`~/psm_project/pod` in our example). This directory contains a config file (`pod.conf`), a number of `bash` scripts (`start`, `stop`, `status`, etc.), and several other files and directories that you don't need to worry about.

### 3.1.1 Pod commands ###

Pod commands are invoked by typing the path to the relevant script in pod directory. The available commands are:

* `start`	: start the pod and all its associated modules (e.g., `pod/start`)
* `stop`	: stop the pod (`pod/stop`)
* `restart`	: restart the pod (`pod/restart`)
* `status`	: print a report of the current state of the pod (`pod/status`)
* `sendbatch`	: send any queued batch files to the remote webserver (`pod/sendbatch`)
* `buttctl`	: control `butt` (Broadcast Using This Tool) (`pod/buttctl start | stop | restart`)
* `clean`	: delete each stream's temporary work files and clean up the log files (e.g., `pod/clean`)
* `peek`	: print out the last few lines of the pod's log file to see what it's doing
* `new`	: install a new program (e.g., `pod/install prog1` to create an audio stream named `prog1`)
* `colorterm` : change the Terminal color scheme for login sessions into this pod  (e.g., `prog1/colorterm show`). (Handy to give each pod its own color scheme if you're likely to be logged into multiple pods at once.)
* `update`	: update the pod software to the latest version (`pod/update`)
* `pushup`	: update the pod software and also push the updates to all pods under the dispatcher's control (`pod/pushup`)
* `backup`	: generate a backup file of this pod's config files and deliver them to the backup site
* `val`	: validate the config file
* `help`	: print info about this command


### 3.1.2 Pod config ###

The file `pod/pod.conf` contains several parameters, including:

```
MODULES		a list of modules to activate when the pod starts
POD_SFTP	the sftp address (`user@host:port`) where generated files should be delivered
POD_BACKUP_SFTP	the sftp address (`user@host:port`) where backup files should be delivered
TERMCOLORS	the Terminal color palette (foreground & background) for login sessions into
		this pod (see `colorterm` command, above)  
POD_NAME	the name of the pod (overrides the network name of the machine)
```


## 3.2 Program module ##

Each PSM **program** resides in its own directory whose name is determined when you first create the program. For example, if you create the program `prog1`, its commands and supporting files will be installed to the directory `$PSM_PROJECT/prog1` (`~/psm_project/prog1` in our example).

To create your first audio program, here named `prog1`:
```
	$ cd ~/psm_project # (or cd $PSM_PROJECT)
	$ pod/newprogram prog1
	$
```
This installs the required code into the directory `~/psm_project/prog1`. You'll want to edit the default config file (`~/psm_project/prog1/program.conf`) that gets installed here. To start the program, do this:
```
	$ cd ~/psm_project
	$ prog1/start
	$
```
This launches the program in a virtual terminal (`screen` in Mac parlance), sending its diagnostic output to a log file. The virtual terminal allows you to safely log out, leaving the program to continue running, unattended, in the background. You can reattach it to the Terminal at any time via the `attach` command (e.g.,  `prog1/attach`).

### 3.2.1 Program commands ###

* `start`	: start the stream (e.g., `prog1/start`)
* `stop`	: stop the stream (e.g., `prog1/stop`)
* `restart`	: stop and start the stream (e.g., `prog1/restart`)
* `status`	: print a report of the current state of the stream (e.g., `prog1/status`)
* `buttctl`	: BUTT controller (Broadcast Using This Tool) (e.g.: `prog1/buttctl start | stop | restart`, )
* `attach`	: attach a running stream to the current Terminal (e.g.: `prog1/attach`, )
* `clean`	: delete the temporary work files (e.g., `prog1/clean`)
* `peek`	: print out the last few lines of the programs's log file to see what it's doing
* `val`	: validate the config file
* ...

### 3.2.2 Program config ###

The file `program.conf` contains a bewildering assortment of parameters. Some parameters needed for collecting data and converting it to audio; others are needed for constructing natural language descriptions that appear on spectrograms and on the website. They include:

	STATION		name of the station (e.g., WRAB)
	STATION_LAT	station latitude
	STATION_LON	station longitude
	STATION_URL	official station URL 
	LOCATION	geographical description of the station location
	NETWORK		name of the network to which the station belongs
	SIGNAL_SOURCE	origin of typical signals (solid earth, atmosphere, etc.)
	SIGNAL_TYPE	description of a typical signal (seismic, atmospheric infrasound, etc.)
	DATATYPE	type of data (miniSeed, Ref Tek, etc)
	INSTRUMENT	description of the sensor
	INSTRUMENT_TYPE	type of sensor (seismic, infrasound, etc.)
	RECORDER	description of the digitizer
	SAMPLE_RATE	digitizer sample rate (Hz)
	CHANNELS	names of the data channels (depends on DATATYPE)
	SOURCEDIRS	full path to the directories wherein data files reside (directories correspond to CHANNELS)
	FIXRATE		sampling rate of audio files
	GAMMA		time scaling factor
	HOURS		duration of data time window
	MAXFREQUENCY	highest frequency (Hz) to display in the spectrograms

Copious descriptions (comments) about each parameter are contained in the demo config files.


---

## 3.3 Dispatcher module ##

The PSM **dispatcher** module is responsible for periodically checking in with other pods (which may themselves reside on the local network or on remote servers), assessing their state-of-health, taking remedial action if necessary, and delivering files to a remote host (typically, a website). The **dispatcher**'s commands reside in the directory `$PSM_PROJECT/dispatcher` (`~/psm_project/dispatcher` in our example).


### 3.3.1 Dispatcher commands ###

* `start`	: start the dispatcher and all its pods and streams (e.g., `dispatcher/start`)
* `stop`	: stop the dispatcher (e.g., `dispatcher/stop`)
* `status`	: print a report of the current state of the dispatcher and its pods (e.g., `dispatcher/status`)
* `login`	: open an `ssh` login session with a pod (e.g., `dispatcher/login mnemosyne`)
* `val`	: validate the config file
* ...


### 3.3.2 Dispatcher config ###


	PODS			list of pods under this dispatcher's control
	DISPATCHER_WAIT		wait time (secs) before moving on to the next pod in the list
	DISPATCHER_BACKUP_SFTP	the sftp address (`user@host:port`) where backup files should be delivered
	

---

## 3.4 Slink module ##

The **slink** module provides for the collection and temporary storage of real-time data from one or more [SeedLink](http://ds.iris.edu/ds/nodes/dmc/services/seedlink/) data servers. When started, **slink** launches two processes: `slarchive`, which collects the desired data (as specified in the **slink** config and streamlist files),	 and `slinksweeper`, which deletes data files that are no longer needed by the audio programs in the **pod**.

A single instance of **slink** can retrieve data from multiple SeedLink servers. Data requests for each server are specified in user-configurable streamlist files, one for each server. The **slink** config file specifies which streamlist files to use.

### 3.4.1 Slink commands ###
* `start` 	: start the module (and slarchive and slinksweeper) (e.g., `slink/start`)
* `stop`	: stop the module (e.g., `slink/stop`)
* `restart`	: stop and start slink (e.g., `slink/restart`)
* `status`	: print the status of the module (e.g., `slink/status`)
* `val`	: validate the config file  (e.g., `slink/val`)
* `clean`	: delete any temporary or stray files (e.g., `slink/clean`)
* `peek`	: print out the last few lines of slink's log file to see what it's doing
* `help`	: print info about this command (e.g., `slink/help`)
* ...


### 3.4.2 Slink config ###

	SERVERS		space-delimited list of urls to SeedLink servers 
	STREAMLISTS	space-delimited list of streamlist files that correspond to SERVERS
	KEEPDAYS	number of days to keep the data

### 3.4.3 Streamlist files ###

Each `streamlist` file (named in the `STREAMLISTS` parameter in the config file) contains a list of the data streams that you wish `slink` to collect. Each line consists of space-delimited [SeedLink selectors](https://ds.iris.edu/ds/nodes/dmc/manuals/slarchive/#seedlink-selectors) that describe the network, station, and channel of the desired data. The channel and instrument codes contained in the SeedLink selectors are exhaustively described in the [SEED Reference Manual](https://www.fdsn.org/seed_manual/SEEDManual_V2.4.pdf). 

 you  represents a stream of data from one or more data channels from one or more  stations in one or more seismic networks. In practice, it's often simpler to reserve each line to data from a single station. 


Examples:
* "`II KDAK 00BH?`" refers to broadband (`B`) high-gain (`H`) seismic data from array location `00` at station `KDAK` (Kodiak, Alaska) in the Iris-IDA network (`II`). The `?` is a wildcard to indicate that all three components of motion are desired.

* "`NE EMMW`" refers to all the data from station `EMMW` (Machias, Maine) in network `NE` (New England Seismic Network). The absence of a third (channel) field in this line indicates that all data from this station should be collected.

It's up to you to figure out the abbreviations for the specific data you want `slink` to collect. For some tips, see section __x.x Retrieving miniSEED data from the IRIS DMC__, below. 

FIXME Lorem ipsum coming soon

---


## 3.5 Rtmon module ##
The **rtmon** module provides some basic tools for monitoring the performance of any Ref Tek software ([rtpd](https://www.reftek.com/ref-tek-protocol-daemon-rtpd/),  [rtcc](https://www.reftek.com/ref-tek-commandcontrol-rtcc/), etc.) that is running on the Mac. If you don't know what Ref Tek is, you don't need to worry about **rtmon**.

### 3.5.1 Rtmon commands ###

FIXME Lorem ipsum coming soon

### 3.5.2 Rtmon config ###

FIXME Lorem ipsum coming soon


___

# 4 Setup examples #

The following examples walk you through the basics of setting up a pod to collect real-time seismic data and play it on your Mac.

## 4.1 How do I set up a `slink` module to pull broadband seismic data from a station in Ukraine? ##

First you need to select which data to collect. Browse the [FDSN Stations map](http://www.fdsn.org/stations/) and click on the red station triangle in the vicinity you want. You'll discover that network `IU` operates station `KIEV` in Ukraine. Look up this station in the [SeismiQuery Data Holdings]( http://ds.iris.edu/SeismiQuery/by_station.html) by clicking the network code (`IU`) in the left frame and clicking the station code (`KIEV`) in the station list frame that appears. Click on today's date in the calendar, and a list will appear showing the data channels currently available. Write down the `NET`, `STATION`, `CHANNEL`, and `LOCATION` you want. Add this line to the `streamlist.txt` file in the `slink` directory:

```
	IU KIEV BH?
```

   (Note the use of the wildcard character `?` to select all three components of motion: vertical, N-S, and E-W.)

   Next, you need to set up the `slink` config file to connect to the IRIS SeedLink server, capture data from the stations you selected, and keep the data for some period of time:

```
	SERVERS=("rtserve.iris.washington.edu:18000")
	STREAMLISTS=(streamlist.txt)
	KEEPDAYS=10
```

   When `slink` starts, it will start harvesting real-time data from the station in Kiev, Ukraine and store it in `slink's` `inbound` directory (`~/psm_project/slink/inbound`) for 10 days.

__NOTE:__ You can receive data from more than one miniSEED server by using multiple streamlist files. For example, to receive data from IRIS and from a Raspberry Shake on the local network, you might set up the `slink.conf` file as follows: 

```
	SERVERS=("rtserve.iris.washington.edu:18000" "rs.local:18000")
	STREAMLISTS=("iris.txt" "rs.txt")
```
Here `iris.txt` contains the network, station, and channel codes for the desired IRIS channels, and `rs.txt` contains the codes for the desired Raspberry Shake channels.


## 4.2 How do I set up a `program` module to play the data from station `KIEV`? ##

   First, create the `program` module:

```
	$ pod/install kiev1
```

   Now you need to edit the default config file (`kiev1/program.conf`) and customize it to this station. You already know the network (`IU`), station (`KIEV`), and channels (`BH1` and `BH2`) from __Example 1__ (above), so you can set these parameters:

```
	STATION=KIEV
	CHANNELS="IU.KIEV.00.BH1 IU.KIEV.00.BH2"
	NETWORK="IRIS/USGS"
```

   From the [GSN page about station `KIEV`](https://earthquake.usgs.gov/monitoring/operations/stations/IU/KIEV/), you can set these parameters: 
```
	LOCATION="Kiev, Ukraine"
	STATION_LAT=50.6944
	STATION_LON=29.2083
	STATION_URL="https://www.fdsn.org/station_book/IU/KIEV/kiev.html"
	NETWORK_URL="https://earthquake.usgs.gov/monitoring/gsn/"
	SIGNAL_TYPE="seismic"
	SIGNAL_SOURCE="solid Earth"
	INSTRUMENT_TYPE="seismometer"
	INSTRUMENT="STS-1V/VBB"
	RECORDER="Q680"
```
   Since GSN broadband data is usually digitized at 20Hz, you can set
```
	SAMPLE_RATE=20
```

   Because you'll be harvesting data from the IRIS SeedLink server, you set
```
	DATATYPE="miniseed"
```
   Next, `SOURCEDIRS` determines where to find the inbound data:

```
	SOURCEDIRS="~/psm_project/slink/inbound/IU/KIEV"
```
   (If `slink` is running on a different machine, set `SOURCEDIRS` to the local mountpoint of that remote inbound directory. For convenience, you can mount that directory each time the `pod` launches by editing the `pod/prelaunch.sh` script.)

   Now choose your desired data window duration (`HOURS`) and time acceleration factor (`GAMMA`). Let's take a week (168 hours) of data and accelerate it so that it plays back in 10 minutes. `GAMMA` is therefore `(168*60)/10 = 1008`. 
```
	HOURS=168
	GAMMA=1008
```

   Set the maximum frequency displayed in the spectrograms:

```
	MAXFREQUENCY=1
```

   In most cases, you can leave the `FIXRATE` parameter alone:

```
	FIXRATE=44100
```

To confirm that the settings in the program config file are valid, type:

```
	$ kiev1/val
```
If this reports any errors, you can get more information by typing:

```
	$ kiev1/val verbose
```

## 4.3 I've set up the `slink` and `kiev1` modules. How do I start them? ##

   Attach the `slink` and `kiev1` modules to the `pod`. For this, edit the pod config file (`pod/pod.conf`) and edit the `MODULES` parameter:

```
	MODULES=(slink kiev1)
```

   Validate (test) your configuration by doing

```
	$ pod/val verbose
```

   If there are no errors, go ahead and start the pod:

```
	$ pod/start
```

## 4.4 I can hear the audio playing on my Mac. How do I stream it online? ##

You'll need to set up a SHOUTcast audio stream server, which is way beyond the scope of this document. To get started, try Googling "shoutcast server" or "how to start an internet radio station". Once you've sorted it all out and have access to a SHOUTcast server, insert your stream credentials into the `SHOUTCAST` parameter in the config file:

```
	SHOUTCAST=PASSWORD@HOST:PORT
```
and restart the program (`kiev1/restart`).

## 4.5 How do I add more audio programs to the pod? ##

The Mac natively provides only two audio channels, which allows you to create one stereo program or  two mono programs. You can, however, create an unlimited number of additional "virtual" audio channels with the third-party software [Loopback](https://rogueamoeba.com/loopback/). Although this is paid (commercial) software, its license allows you to run it on multiple machines, so you can use it on all the pods in your PSM cluster. Once you run it to create some virtual devices on a Mac, you shouldn't need it run it again. 

Basic idea: use Loopback to create a half-dozen devices named `psmdev1`, `psmdev2`, ..., `psmdev6`. In the program config file, you can assign each program to a virtual device by setting the `AUDIO_DEVICE` parameter accordingly:

```
	AUDIO_DEVICE=psmdev3
```

When you start the program, the audio will be routed to that virtual device. Although you won't actually hear the audio on the Mac's speakers, PSM will deliver each program's audio to its associated SHOUTCAST feed. (If you'd like to monitor the outgoing multichannel audio, you can easily install an external hardware audio interface -- for example, the [Behringer U-Phoria 1820](https://www.sweetwater.com/store/detail/UMC1820), provides 16 channels of audio via a single USB connection to the Mac.)

---


# Appendix A:  Selecting data from the IRIS DMC #

An excellent source of data for the PSM is the [International Federation of Digital Seismograph Networks (FDSN)](https://www.fdsn.org), which maintains a catalog of research seismograph stations around the world. Much of the data from these stations are archived in real time by the [IRIS Data Management Center](https://ds.iris.edu/ds/nodes/dmc/) and made available to the public in miniSEED format via their SeedLink server. 

In order to access data from the IRIS DMC, you'll need to know the abbreviations for the particular data channels you want. In particular, you'll need the station code, the network code, and the channel code.

Here are some tips and links to get you started.

__1) Find a network.__ Search the &rArr; [list of FDSN Network Codes](https://fdsn.org/networks). There are more than a thousand networks listed here. To streamline your search, select the "Show Network Types: Permanent" button; this reduces the list to 400+ networks. Some good global seismic networks to start your explorations are [II (IRIS/IDA)](http://fdsn.org/networks/detail/II/) and [IU (GSN)](http://fdsn.org/networks/detail/IU/). For infrasound, take a look at [IM (International Miscellaneous Stations)](http://fdsn.org/networks/detail/IM/).

__2) Find a station.__ After selecting the desired network, browse the map or the list of stations to find one that interests you. For detailed metadata about the available stations (lat, lon, sample rate, sensor type, etc.) click the "Full fedcatalog information for this network" link in the "Data Access" section.

__3) Find the data channels.__ Browse the &rArr; [SeismiQuery Data Holdings By Station]( http://ds.iris.edu/SeismiQuery/by_station.html). In the left frame, click your network code under the current year. This populates the middle frame with a list of available stations. Click the station of interest. This reveals a calendar of available data. Days with links have data; those without do not. If the calendar for this station contains multiple "holes", consider choosing another station with more reliable data. Click a recent date on the calendar to view a list of available data channels.

__4) Choose the data channel.__ To decipher the channel codes, see &rArr; [SEED Reference Manual](https://www.fdsn.org/seed_manual/SEEDManual_V2.4.pdf), especially "Appendix A: Channel Naming". *Hint*: for audification of seismic data, channel codes `BHZ`, `BH1`, and `BH2` (<b>B</b>roadband <b>H</b>igh-gain, vertical and horizontal components) work well; for audification of atmospheric infrasound, look for channel `BDF` (<b>B</b>roadband /pressure/infrasound).

__5) Choose the location code.__ This code (`00`, `10` etc.) identifies individual deployment locations (piers, vaults, etc.) at the same station. You'll need this code when setting the `CHANNELS` parameter in `program.conf`.

__6) Get more info about a station.__ It can take some [shrewd detective work](https://google.co) to find more detailed info about a given station. For stations in networks `CU`, `GT`, `IE`, `II`, and `IU`, check out the [USGS's list of GSN and ANSS stations](https://earthquake.usgs.gov/monitoring/operations/network.php), which offers some minimal info about each station.


---

# Appendix B:  A note about dependencies #

Most of the required binaries, libraries, etc. are contained in the PSM package. These include my custom (XCode C++) executables:

* `esound` : converts data files to audio files
* `soxOverlay` :  creates a graphical overlay with scaled time- and freq- ticks, etc.
 for the sox-generated spectrogram 
* `eventOverlay` : creates a graphical overlay with USGS event labels for the sox-generated spectrogram  

Then there is a pile of open source stuff:

* [butt (Broadcast Using This Tool)](https://sourceforge.net/projects/butt/files/) : streams audio to a SHOUTCast server (I've made some minor tweaks to the original `butt` code for better integration with PSM). 
* `sox` : does audio processing (filtering, companding, etc.) and generates spectrogram image
* `gnuplot`: does the plotting for `soxOverlay` and `eventOverlay`
* `boost::program_options` and a bunch of other libraries that are required by `esound`, etc.

The commercial program Loopback is required when streaming more than two channels of audio. See "<a href="#install-loopback">Install Loopback</a>", above.


