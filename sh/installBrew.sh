#! /bin/bash

# Get the constants that concern the apps and libs
APPS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/APPS.sh" 
source "${APPS}"

# Install the brew commands and formulae
function installBrew() {
	printf "Installing homebrew apps and formulae...\n"
	
	local isBrewInstalled=0
	# Is brew already installed?
	if command -v ${BIN_BREW} 2>&1 >/dev/null ; then
		printf "Brew already installed.\n"
	else
		printf "Installing brew...\n"
		/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	fi
	
	# if the previous curl failed for some reason...
	if ! command -v ${BIN_BREW} 2>&1 >/dev/null ; then
		printf "Something went wrong. Brew installation is broken. Can't continue.\n"
		exit
	fi
	
	printf "Installing all the required formulae. This may take awhile...\n"
	local f
	for f in ${BREW_FORMULAE[@]} ; do
		printf "Installing $(basename ${f})...\n"
		${BIN_BREW} install $f
	done	
}


installBrew
