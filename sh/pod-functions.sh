#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
# Various functions used by the pod/* scripts

MODULE_TYPE="pod"
MODULE_NAME="pod" # "$( basename $( cd $(dirname ${BASH_SOURCE[1]}); pwd) )" # the directory that contained the shell command that brought us here
MODULE_FUNCTION_PATH="${BASH_SOURCE[0]}" # path to this module function file
MODULE_FUNCTION_FILENAME="$( basename ${BASH_SOURCE[0]} )" # handy name of this module function file

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # the containing dir of this script (http://stackoverflow.com/a/246128/5011245)
source "${SCRIPTS_DIR}/DEFAULTS.sh" # get the default params
source "${SCRIPTS_DIR}/APPS.sh" # get the apps
source "${SCRIPTS_DIR}/shared.sh" # get the common functions and set some crucial variables
MODULE_COMMANDS=(backup buttctl clean colorterm cstop help housecall logstring makefacts install peek pstop pushstart pushstop pushup restart sendbatch start status stop syncstart update val ver _name) # commands to be installed from templates
MODULE_SPECIAL_DIRS=($DEFAULT_POD_QUEUE_DIR $DEFAULT_MODULE_BACKUP_DIR) # special dirs (unique to this module) that must be present 
MODULE_SPECIAL_FILES=(demo-prelaunch.sh)	# special files to be installed from templates
MODULE_REQUIRED_BINARIES=($BIN_ESOUND $BIN_AUDIODEV $BIN_EVENTOVERLAY $BIN_SOXOVERLAY $BIN_SOX $BIN_GNUPLOT $BIN_MAGICK $BIN_ID3V2 $BIN_SLARCHIVE $BIN_BUTT)
MODULE_USER_PREFERENCES=("${MODULE_TYPE}.conf" prelaunch.sh) # user-configurable files (to be saved with the "backup" command)
MODULE_PROTECTED_FILES=("${MODULE_USER_PREFERENCES[@]}" "${MODULE_COMMANDS[@]}" "${MODULE_SPECIAL_FILES[@]}" "demo-${MODULE_TYPE}.conf") # files that will never be clean'ed
MODULE_PRELAUNCHER="${MODULE_DIR}/prelaunch.sh"

# List of parameters to expect in the config file
CONFIG_REQUIRED_VARIABLES=()
CONFIG_REQUIRED_ARRAYS=()
CONFIG_OPTIONAL_VARIABLES=(POD_BACKUP_SFTP POD_SFTP POD_WEBHOST_ROOT_DIR)
CONFIG_DEPRECATED_VARIABLES=(MOUNTPOINT AFP)
CONFIG_OPTIONAL_ARRAYS=(TERMCOLORS MODULES)

################################
# FIXME/TODO:
# new commands:
#   pstop       Stop only the program modules.
#   cstop       Stop only the control modules.
################################


#############
# Pod_help() -- print a help message
#############
function Pod_help () { 

#	echo "   Inbound : $(GetWebhostInboundDir)"
#	echo "      Pods : $(GetWebhostPodsDir)"
#	echo "Dispatchers: $(GetWebhostDispatchersDir)"
#	echo "   Archive : $(GetWebhostArchiveDir)"
#	exit

	cat << EOT
A PSM ${MODULE_TYPE} module may be controlled from the command line.
Commands are bash scripts; to invoke a command, type its path. 
For example, to generate this help message from the PSM project directory (${PSM_PROJECT}), 
type '${MODULE_TYPE}/help'. Some commands take optional arguments.

   Command     What it does
   =======     ============
   buttctl     Control each program's instance of BUTT (Broadcast Using This Tool).  
               Usage: ${MODULE_NAME}/buttcl [ start | stop | restart | status]
                   start   : start BUTT
                   stop    : stop BUTT (if it's running)
                   restart : stop, then start BUTT
                   status  : print BUTT's status (running, not-running, etc.)
   clean       Delete any temporary or stray files from the ${MODULE_TYPE} directory and roll over the logfiles.
               (Reminder: be careful adding your own files to this directory, as 'clean' will
               delete them, unless their names end in ".txt".)
   colorterm   Manage the pod's Terminal color themes
   help        Print this help info.
   housecall   Test the health of each module, take remedial action if necessary, and (optionally) deliver results
               to the remote web server
               Usage: ${MODULE_NAME}/housecall [send]
   install     Install a new module
               Usage: pod/install MODULENAME
               Where MODULENAME can be a program name, or a control module of one of these types: ${DEFAULT_RESERVED_POD_CONTROL_MODULES[*]}
   logstring   Add a short message to the pod's log file.
   pushstart   Start this pod and all downstream pods that under Dispatcher management
   pushstop    Stop this pod and all downstream pods that under Dispatcher management
   pushup      Update this pod and push updates to all downstream pods
   restart     Restart all programs in this pod.
   sendbatch   Manually send any queued files to the webserver via sftp.
               (This is typically invoked automatically by the Dispatcher.)
   start       Launch the modules associated with this pod. Each starts in a virtual terminal (screen(1)), and then
               returns control to the shell.
   status      Print the run status of this pod and its associated modules
               Usage: pod/status [verbose]
                   verbose -- also print the last few lines of the log file
   stop        Stop all modules associated with this pod.
   update      Update the script codebase of this pod to the latest version.
   val         Validate the ${MODULE_TYPE} module's config file and print the result.
               Usage: ${MODULE_NAME}/val [count | verbose | oneliner]
                  count    :  print the number of errors
                  verbose  :  print a detailed report
                  oneliner :  print a one-line summary [default]
   ver         Show the codebase version number
               Usage: ${MODULE_NAME}/ver [full]
                      full : print the full (major and minor) version numbers
               (By default, just print the major version number.)

EOT
}


#############
# Pod_install() -- Install a new module on this pod 
#############
function Pod_install () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	if [[ $# -ne 1 ]] ; then
		cat << EOT
Usage: ${callingFrom} MODULENAME
Installs the named PSM module.
MODULENAME must only contain characters in this regex set: ${DEFAULT_MODULE_NAME_REGEX}.
New 'program' modules may not be assigned any of these names: ${DEFAULT_RESERVED_MODULE_NAMES[*]}.
EOT
		exit
	fi
	
	MODULE_NAME="$1"
	
	# if module name does NOT match the regex pattern of all allowed characters, then quit	
	if [[ ! "$MODULE_NAME" =~ ^${DEFAULT_MODULE_NAME_REGEX}+$ ]] ; then
		printf "${callingFrom}: *** Can't create module '${MODULE_NAME}'. Its name contains invalid character(s). \n"
		printf "${callingFrom}: *** The only allowed characters are those in this regex set: ${DEFAULT_MODULE_NAME_REGEX}\n"
		exit
	fi
	
	# if the directory already exists, then quit
	if [[ -d "${PSM_PROJECT}/${MODULE_NAME}" ]] ; then
		printf "${callingFrom}: *** Module '${MODULE_NAME}' already installed.\n"
		exit
	fi	
	
	# # if the module is "slink", "dispatcher",  etc., then that's its type; otherwise, it's going to be an audio program
	if ElementIn "$MODULE_NAME" "${DEFAULT_RESERVED_MODULE_NAMES[@]}" ; then
		MODULE_TYPE="${MODULE_NAME}"	
	else 
		MODULE_TYPE="program"
	fi

	# Now source shared.sh, using the new MODULE_NAME and MODULE_TYPE identities
	source "${SCRIPTS_DIR}/shared.sh" 

	UpdateModule nogit create
	# nogit		suppress a "git pull..." that would refresh the local codebase from the git repo
	# create	only try create a new one; don't touch an existing one
		
	printf "${callingFrom}: Done.\n"

}


#############
# Pod_ver() -- print the version number of the installed scripts
#############
function Pod_ver () { 
	Version $@
}

#############
# Pod__name() -- print a nice machine name of this pod (e.g., "hera")
# This is used when a remote dispatcher on another network wants to fetch the local hostname of this pod
#############
function Pod__name () { 
	PodName $@
}



#############
# Pod_status() -- report the status of each of the programs that are defined in the pod's config file 
# If no second argument equal to "verbose" is present , then print a minimal one-line status report
# If a second argument is present and is equal to "verbose", then print out detailed info
#############
function Pod_status () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	# get the argument	
	local doVerbose=0
	if [[ $# -ge 1 ]] ; then
		if [[ "$1" = "verbose" ]] || [[ "$1" = "v" ]] ; then
			doVerbose=1
		fi
	fi

	# print the version info
	local hostname="$(hostname -s)"
	printf "Pod $(PodName full) ($(hostname)) version $(Version full)\n"
	local loadval=$(SystemLoad | sed -e "s/ .*//") # get the numerical part of the system load report
	local alert=""
	[[ $(echo "$loadval > ${DEFAULT_SAFE_LOAD_AVERAGE}" | bc -lq) -eq 1 ]] && alert="*** Warning: 5-minute system load average exceeds safe limit (${DEFAULT_SAFE_LOAD_AVERAGE} sec)"

	printf "%${DEFAULT_FORMAT_SPACING}s : %s %s\n"  "5-min system load avg" "$(SystemLoad)" "$alert"
		
	# print the control module status	
	local m n na nr s isare pod
	na=$(CountActiveModules control)
	nr=$(CountRunningModules control)
	printf "%${DEFAULT_FORMAT_SPACING}s : %d active, %d running\n"  "Control modules" $na $nr

	for m in "${DEFAULT_POD_CONTROL_MODULES[@]}" ; do
		s=$(ModuleState $m)
		printf "%${DEFAULT_FORMAT_SPACING}s : %4s\n" ${m} $s
		if [[ $m = "dispatcher" ]] && [[ $s != $DEFAULT_MODULE_STATE_INACTIVE ]] ; then # for a dispatcher module, also list its pods
			(
				printf "%${DEFAULT_FORMAT_SPACING}s : " "dispatcher pods"
				source "${SCRIPTS_DIR}/dispatcher-functions.sh" 
				SourceConfig # (to get PODS)
				for pod in "${PODS[@]}" ; do
					printf "%s  " ${pod}
				done
				printf "\n"
			)
		fi
	done
	


	# which programs are installed?
	SourceConfig $callingFrom  # get PROGRAMS
	na=${#PROGRAMS[@]}
	nr=$(CountRunningModules program)
	printf "%${DEFAULT_FORMAT_SPACING}s : %d active, %d running\n"  "Program modules" $na $nr
	
	# get status report on each running program
	for program in "${PROGRAMS[@]}" ; do
		[[ $doVerbose -eq 1 ]] && printf "Status of program '${program}':\n"
		local programDir="${PSM_PROJECT}/${program}"
		if [[ ! -e "${programDir}" ]] ; then
			printf "%${DEFAULT_FORMAT_SPACING}s : %s\n" "${program}" "*** Not installed. No such directory (${programDir})."
		else
			if [[ $doVerbose -eq 1 ]] ; then
				"${programDir}/status" verbose
			else
				"${programDir}/status"
			fi
		fi
		[[ $doVerbose -eq 1 ]] && printf "\n"
	done
	
	# see what's in the sftp queue
	local sftpFiles=()
	local status="empty"
	while IFS=  read -r -d $'\0'; do
		sftpFiles+=("$REPLY")
	done < <(find "${POD_QUEUE_DIR}" -name \*sftp -depth 1 -print0)
	if [[ ${#sftpFiles[@]} -ne 0 ]] ; then
		[[ ${#sftpFiles[@]} -eq 1 ]] && s="" || s="s" # pluralizer
		status="${#sftpFiles[@]} undelivered batch file${s}"	
	fi
	printf "%${DEFAULT_FORMAT_SPACING}s : %s\n"  "SFTP queue" "${status}"
	

	local errCount=$(Pod_val count)
	if [[ $errCount -gt 0 ]] ; then
		[[ $errCount -eq 1 ]] && local s="" || local s="s" # pluralizer
		printf "${callingFrom}: *** Warning: ${MODULE_NAME} has ${errCount} error${s} that must be fixed before pod can start any program modules.\n"
		printf "${callingFrom}: *** To troubleshoot the error, try '${MODULE_NAME}/val verbose'.\n"
	fi	


}



#############
# Pod_pstop() -- kill any running program modules
# Does NOT invoke sendbatch to deliver any queued files
#############
function Pod_pstop () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $callingFrom
	ModStop "${callingFrom}" program 
}

#############
# Pod_cstop() -- kill any running control modules
# Does NOT invoke sendbatch to deliver any queued files
#############
function Pod_cstop () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $callingFrom	
	ModStop "${callingFrom}" control 
}





#############
# Pod_stop() -- kill all of the running modules
#############
function Pod_stop () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $callingFrom
	
	ModStop "${callingFrom}" program  # stop the program modules
	ModStop "${callingFrom}" control # stop the control modules

	n=$(CountRunningModules all)
	[[ $n -eq 1 ]] && { s="";isare="is"; } || { s="s";isare="are"; } # pluralizer
	[[ $n -eq 0 ]] && string="${callingFrom}: All modules are stopped. (OK)" || string="${callingFrom}: ${n} module${s} ${isare} still running ($(ListRunningModules control)). (OK)" 
	LogMe "${MODULE_LOGFILE}" "${string}"
	
	PurgeQueue ${callingFrom} # try to deliver any leftovers from the sftp queue
	
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_NAME} stopped. (OK)"

	printf "\n"
}

# PurgeQueue() -- send off any residual queued sftp files
function PurgeQueue() {
	callingFrom=$1
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: calling sendbatch to deliver residual queued files..."
	Pod_sendbatch
	local errCode=$?
	if [[ $errCode -ne 0 ]] ; then  #   failed
		printf "${callingFrom}: *** Unable to send files (err=${errCode}).\n"
	fi
}


#############
# Pod_colorterm() -- set the terminal color based on the TERMCOLORS config variable
# With no argument, set the terminal color theme to that given in the pod.conf
# With a single argument:
#
#############
Pod_colorterm() {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	source "${SCRIPTS_DIR}/colors.sh"
	# with two args, just set the theme to those two colors
	if [[ $# -eq 2 ]] ; then
 		TerminalTheme $1 $2	&& tput clear 
		return4
		
	fi

	SourceConfig $callingFrom  # get TERMCOLORS
	if [[ -z ${TERMCOLORS+x} ]]; then # If TERMCOLORS is set, then apply it
		local termColors=$TERMCOLORS # convert the (possibly space-delimited) string variable to an array
	fi	
	
	local foreground="" background=""
	if [[ ${#TERMCOLORS[@]} -eq 1 ]] ; then # only one variable; so it's the BACKGROUND
		background="${TERMCOLORS[0]}"
	else
		foreground="${TERMCOLORS[0]}"	# first is foreground color
		background="${TERMCOLORS[1]}"	# second is background color
	fi
	
	
	
	case "$1" in
	help | h )
		cat << EOT
Usage: ${callingFrom} [ l[ist] | f[ore[ground]] | b[ack[ground]] | h[elp] ]
Manage the Terminal color theme associated with this pod. 
By default, ${callingFrom} sets the Terminal color theme to TERMCOLORS in pod.conf.
The optional arguments:
	show | s | list | l    : show a table of available colors
	foreground | fore | f  : print the ANSI color value of the foreground
	background | back | b  : print the ANSI color value of the background
	help                   : print this help
EOT
		;;
	show | s | list | l  ) # show all the available colors
		source ${SCRIPTS_DIR}/colors.sh
		colorTable
		;; 
	foreground | fore | f) 
		#printf "$(get_apple_color ${foreground})\n"
		printf "${foreground}"
		;;
	background | back | b) 
		#printf "$(get_apple_color ${background})\n"
		printf "${background}"
		;;
	*)  # by default, set the terminal color theme to TERMCOLORS
 		TerminalTheme $foreground $background		
		;;
	esac

}

#############
# Pod_logstring() -- log a short string to the log file 
#############
function Pod_logstring () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	if [[ $# -eq 0 ]] ; then
		cat << EOT
Usage: ${callingFrom} TEXT
Where TEXT is the short message you'd like to add to the logfile.
EOT
		return
	fi
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: $*"
}

#############
# Pod_restart() -- restart all programs in this pod
#############
function Pod_restart () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $callingFrom  # get PROGRAMS

	LogMe "${MODULE_LOGFILE}" "${callingFrom} : executing..."

	Pod_stop $@
	printf "\n[wait]..."
	sleep 2	# allow the processes to finish up 
	printf "\n"
	Pod_start $@
}

#############
# Pod_start() -- launch the programs that are defined in the pod's config file 
#############
function Pod_start () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $callingFrom  # get PROGRAMS
	
	errCount=$(Pod_val count)
	if [[ $errCount -gt 0 ]] ; then
		printf "${callingFrom}: *** ${MODULE_TYPE} has errors. Can't start.\n"
		Pod_val verbose
		printf "${callingFrom}: *** ${MODULE_TYPE} has errors. Can't start.\n"
		exit
	fi

	local n s isare m mDir
	local list=($(ListRunningModules all))
	n=${#list[@]}
	[[ $n -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	if [[ ${n} -gt 0 ]] ; then
		printf "${callingFrom}: *** There ${isare} $n module${s} running (${list[*]}). Please stop ${MODULE_NAME} first.\n"
		exit
	fi

	! SystemLoadIsTolerable && LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** WARNING: high load average: $(SystemLoad). Can't start pod."
		
	RotateLogFiles "${MODULE_LOG_DIR}/${DEFAULT_POD_SENDBATCH_LOGFILE}" "${MODULE_LOGFILE}"

	# launch the control programs
	list=($(ListActiveModules control))
	for m in "${list[@]}" ; do
		printf "========= Starting ${m} =========\n"
		local mDir="${PSM_PROJECT}/${m}"
		if [[ ! -e "${mDir}" ]] ; then
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Can't find module ${m}. Nothing more to do."
		else
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Launching ${m}."
			${mDir}/start 
		fi
		printf "...[wait]..."
		sleep 5 # A delay, just to spread them out a tad
		printf "\n\n"
	done

	
	# start the program modules
	n=${#PROGRAMS[@]}
	printf "${callingFrom}: Found ${n} programs: ${PROGRAMS[*]}\n"
	if [[ $n -gt $DEFAULT_POD_MAX_PROGRAM_COUNT ]] ; then
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Too many programs (${n}). No more than $DEFAULT_POD_MAX_PROGRAM_COUNT allowed."
		exit
	fi
	if [[ -e "${MODULE_PRELAUNCHER}" ]] ; then
		LogMe  "${MODULE_LOGFILE}"  "${callingFrom}: running prelaunch script..."
		bash "${MODULE_PRELAUNCHER}"
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: prelaunch script done. (OK)"
	else 
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: no prelaunch script. (OK)"
	fi

	##### This chunk is adapted from a similar chunk in dispatcher-functions.sh
	#
	# We're ready to go. First, generate and upload the factoids about this pod
	MakeFactoids
	Pod_sendbatch
	local errCode=$?
	if [[ $errCode -ne 0 ]] ; then  #   failed
		printf "${callingFrom}: *** Unable to send startup factoid files (err=${errCode}). Continuing anyway.\n"
	fi
	#
	##### This chunk is adapted from a similar chunk in dispatcher-functions.sh

	# now launch the individual program modules
	for m in "${PROGRAMS[@]}" ; do
		printf "========= Starting ${m} =========\n"
		local mDir="${PSM_PROJECT}/${m}"
		if [[ ! -e "${mDir}" ]] ; then
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Can't find module ${m}. Nothing more to do."
		else
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Launching ${m}."
			${mDir}/start 
		fi
		printf "...[wait]..."
		sleep 5 # A delay, just to spread them out a tad
		printf "\n\n"
	done
	
	list=($(ListRunningModules))
	n=${#list[@]}
	[[ $n -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${n} module${s} ${isare} running."

	list=($(ListRunningModules program))
	n=${#list[@]}
	[[ $n -eq 1 ]] && { s="" ; isare="has" ; } || { s="s" ; isare="have" ; } # pluralizer
	# if any program modules were launched, print some handy user tips
	if [[ $n -gt 0 ]] ; then 
		printf "${n} program module${s} ${isare} been launched.\n"
		printf "How to do some some common tasks:\n"
		printf "%30s: %s\n" "Get pod status" "pod/status"
		printf "%30s: %s\n" "Stop the pod" "pod/stop"
		printf "%30s: %s\n" "Reattach a program to the terminal" "PROGNAME/attach"
	fi
		
	printf "\n"

	
}


#############
# Pod_makefacts() -- make a factoids file manually
#############
function Pod_makefacts () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $callingFrom  # get PROGRAMS
	MakeFactoids
}


#############
# MakeFactoids() -- deposit some pod factoids to the sftp upload queue
# No argument required.
############# 
function MakeFactoids () {
	if [[ "$#" -ne 0 ]] ; then
		printf "*** bad arg count ($#) in MakeFactoids\n"
		exit 1
	fi
	
	SourceConfig
	
	# step 1: Create a factoids file with info about this pod
	local factoidFileName="pod.$(PodName).factoids.txt" 
	local localPath="${POD_QUEUE_DIR}/${factoidFileName}" # path to the factoid file on this pod
	local remotePath="$(GetWebhostPodsDir)/${factoidFileName}" # path on the destination (remote) server
	
	# Create the factoids file and fill it with the basic info
	InitFactoids "${localPath}"
	
	# list the active modules 
	local module mList=()
	for module in "${MODULES[@]}" ; do
		ModuleIsPresent $module && mList+=( "$module" )
	done
	cat << EOT >> $localPath
modules: ${mList[*]}
EOT

	# get the status codes for each stream
	local theStreamStatsA=()
	for program in "${PROGRAMS[@]}" ; do
		local programDir="${PSM_PROJECT}/${program}"
		theStreamStatsA+=( $("${programDir}/status" code) )
	done
	local theStreamStats="${theStreamStatsA[*]}"
	local theStreams="${PROGRAMS[*]}"
	
	# Append the factoids specific to this module
	cat << EOT >> $localPath
streams: $theStreams
streamstats: $theStreamStats
exception1: $(NthException 1)
exception2: $(NthException 2)
exception3: $(NthException 3)
exception4: $(NthException 4)
exception5: $(NthException 5)
EOT


	# step 2: create an sftp batch file for sending the factoids up to the web server
	local sftpBatchfile="${POD_QUEUE_DIR}/pod.sftp"
	cat << EOT > $sftpBatchfile
put ${localPath} ${remotePath}
EOT

}

#############
# Pod_backup() -- make a backup of the essential config files
#############
function Pod_backup () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $callingFrom  # get PROGRAMS

	LogMe "${MODULE_LOGFILE}" "${callingFrom}: starting..."
	
	# First create a temporary working backup dir. When we're all done, we'll copy the backup over to a time-stamped directory .
	local tmpBackupDir="${MODULE_DIR}/${DEFAULT_MODULE_BACKUP_DIR}/tmp.bak"  # /.../bak/tmp.bak
	local finalBackupDir="${MODULE_DIR}/${DEFAULT_MODULE_BACKUP_DIR}/$(PodName).$(date -u +%Y%m%d%H%M%S).bak" # a time-stamped dirname

	if [[ -d "${tmpBackupDir}" ]] ; then # if dir already exists, roll it over
		rm -fr "${tmpBackupDir}~"  # toss out the old (~) one
		mv "${tmpBackupDir}" "${tmpBackupDir}~"	# roll over the previous one to ~
	fi
	mkdir "${tmpBackupDir}"

	# save this pod's own backup-able files
	local backupModuleDir="${tmpBackupDir}/pod"
	mkdir -p "${backupModuleDir}"
	for f in "${MODULE_USER_PREFERENCES[@]}" ; do # make a copy of each file (if it exists)
		[[ -f "${MODULE_DIR}/${f}" ]] && cp "${MODULE_DIR}/${f}" "${backupModuleDir}/."
	done
	
	# backup the essential files for each of this pod's modules
	local m f mType mDir
	for m in "${MODULES[@]}" ; do
		if ! ModuleIsPresent $m ; then # if there's no module by this name, then skip it
			continue
		fi
		backupModuleDir="${tmpBackupDir}/${m}"
		mkdir "${backupModuleDir}"
		mType=$(ModuleType $m)  # deduce the type of this module
		mDir="${PSM_PROJECT}/${m}"
		# launch a subshell to get the MODULE_USER_PREFERENCES for this module
		(
			source "${SCRIPTS_DIR}/${mType}-functions.sh" # load its functions
			for f in ${MODULE_USER_PREFERENCES[@]} ; do  # make a copy of each file (if it exists)
				[[ -f "${mDir}/${f}"  ]] && cp "${mDir}/${f}" "${backupModuleDir}/."
			done
		) # end of subshell
		
		
		# if the module that we're backing up is a dispatcher, then we also need to collect the backup files from all its downstream pods!
		if [[ $m = "dispatcher" ]] ; then 
			# launch a subshell to get the PODS for this module
			(
				source "${SCRIPTS_DIR}/dispatcher-functions.sh" 
				SourceConfig # (to get PODS)
				for pod in "${PODS[@]}" ; do
					LogMe "${MODULE_LOGFILE}" "${callingFrom}: backing up ${pod}..."
					# if the dispatcher manages localhost, then there's nothing further needing backup
					if [[ "${pod}" = "localhost" ]] ; then		# if it's "localhost"
						: # skip it
					else 	# ...if it's not "localhost" , then use ssh 
						
						# first get the name of the remote pod					
						local execReturnValue="$( ExecPodSilently $pod '_name full')" # get the full name of the remote pod
						if ! ExtractReturnCode "${execReturnValue}" ; then
							LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** Unable to connect to remote pod at ${pod}"
							LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** (return value=${execReturnValue})."
							exit
						else 
							local podName="$(ExtractOutputString "$execReturnValue" )"
							LogMe "${MODULE_LOGFILE}" "${callingFrom}: backing up ${podName} (${pod})..."
							execReturnValue="$( ExecPodSilently $pod backup )" # issue pod/backup on the remote pod
							if ! ExtractReturnCode "${execReturnValue}" ; then
								LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** backup failed of ${podName} (${pod})."
								:
							else # backup succeeded, so fetch the files over the network
								local remotePodDir="${tmpBackupDir}/${podName}"
								mkdir -p "${remotePodDir}"
								# POD is generally of the form USER@HOST:PORT, so parse it to get USER@HOST and PORT:
								local uhp=(${pod//:/ })  # break it at the ":"
								local userAtHost="${uhp[0]}" # USER@HOST
								[[ ${#uhp[@]} -eq 2 ]] && local sshPort="-p ${uhp[1]}" || local sshPort=""
								ssh -o ConnectTimeout=${DEFAULT_SSH_CONNECTTIMEOUT} "${userAtHost}" "${sshPort}" \
									"source ~/.profile; cd \${PSM_PROJECT}/pod/${DEFAULT_MODULE_BACKUP_DIR}/tmp.bak; tar -czf - ./" | tar -xzf - -C "${remotePodDir}" 2>/dev/null
							fi
						fi
					fi
				done
			)
		fi
	done
	
	# rename the backup to its final name
	cp -R "${tmpBackupDir}" "${finalBackupDir}"
	
	# create a zip file of everything we found
	local zipFile=psm.$(date -u +%Y%m%d%H%M%S).zip 
	( cd "${finalBackupDir}/.." && zip -r $zipFile $(basename ${finalBackupDir}) )
	
	
#	rm -fr "${tmpBackupDir}" # clean up the tmp dir  ### DON'T DELETE IT! We need to know the name of the tmp backup dir created on a remote machine.

	# send the backup to a remote host, if it's specified
	if [[ ! -z ${POD_BACKUP_SFTP+x} ]] ; then
		local remoteDir="${POD_BACKUP_SFTP##*:}" # get the part after the last ':'
		if [[ "${remoteDir}" = "${POD_BACKUP_SFTP}" ]] ; then # if no directory was specified, call it the current (login) directory
			remoteDir="."
		fi
		local userAtHost="${POD_BACKUP_SFTP%:*}" #strip off the part after the last ':' 
		local backupName="$(PodName)-$(date -u +${DEFAULT_FILE_TIME_FORMAT}).bak" 
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: sending ${backupName} to ${userAtHost}:${remoteDir} ..."
		# Send the backup over to the web host:
		# 1. zip it with tar, first cleaning up the directory structure (put a time stamp in the top directory name and get rid of '.bak' in dir names).
		# 2. make ssh connection 
		# 3 cd to the dir and un-tar the tarball there
		tar -czf - -C "$(dirname "${finalBackupDir}")"  "$(basename "${finalBackupDir}")"  \
			| ssh "${userAtHost}" "cd ${remoteDir}; tar -xzf -" 
	fi

	LogMe "${MODULE_LOGFILE}" "${callingFrom}: done."
}



# Pod_pushstop() -- stop this pod and all downstream pods under dispatcher management
function Pod_pushstop () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Stopping..."
	Pod_stop
	# if a dispatcher is present in this pod, then instruct it to forward the update message to downstream pods
	if ModuleIsPresent dispatcher ; then 
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Stopping downstream pods..."
		${PSM_PROJECT}/dispatcher/_pushstop $DEFAULT_PROGRAM_RUN_KEY # (the "key" discourages direct invocation of dispatcher/_pushstop from the console)
	fi
	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Done"
}

# Pod_pushstart() -- start this pod and all downstream pods under dispatcher management
function Pod_pushstart () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Resuming..."
	Pod_start
	
	# if a dispatcher is present in this pod, then instruct it to forward the update message to downstream pods
	if ModuleIsPresent dispatcher ; then 
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Resuming downstream pods..."
		${PSM_PROJECT}/dispatcher/_pushstart $DEFAULT_PROGRAM_RUN_KEY # (the "key" discourages direct invocation of dispatcher/pushstart from the console)
	fi
	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Done"
}

#
# Pod_pushup() -- update this pod (and its modules) AND if a dispatcher is present, 
# push updates downstream to all of its modules 
#
function Pod_pushup () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Pushing software update to this pod"
	
	# save this pod's run state
#	local oldState=$( ModuleIsRunning pod && printf "${DEFAULT_MODULE_STATE_UP}" || printf "${DEFAULT_MODULE_STATE_DOWN}" )
	
	# stop the pod
	Pod_stop
	
	# update the pod
	Pod_update
	
	# if a dispatcher is present in this pod, then instruct it to forward the update message to downstream pods
	if ModuleIsPresent dispatcher ; then 
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Pushing update to downstream pods..."
		${PSM_PROJECT}/dispatcher/_pushup $DEFAULT_PROGRAM_RUN_KEY # (the "key" discourages direct invocation of dispatcher/_pushup from the console)
	fi
	
	# if the pod was running before the pushup, then restart it now
#	[[ $oldState = "${DEFAULT_MODULE_STATE_UP}" ]] && Pod_start

	Pod_start
	
	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Done"
}


#
# Pod_update() -- update the module
#
function Pod_update () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	local n s isare

	# Are any modules already running?
	local list=($(ListRunningModules all))
	local n=${#list[@]}
	local s isare
	[[ $n -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	if [[ ${n} -gt 0 ]] ; then
		printf "${callingFrom}: *** There ${isare} $n module${s} running (${list[*]}). Please stop ${MODULE_NAME} first.\n"
		exit
	fi
	
	SourceConfig $callingFrom  

	# (See comments at end of this function for this 'exit' business)
	ArrayContainsElement "exit" $@ \
		&& printf "\n============================ Updating $(PodName) (second pass) ============================\n" \
		|| printf "\n============================ Updating $(PodName) (first pass) ============================\n"
	
	# first update the pod itself
	UpdateModule $@
		
	# Now update the control modules (or install them, if missing)
	local module moduleDir mToUpdate=() mToCreate=()
	for module in "${DEFAULT_POD_CONTROL_MODULES[@]}" ; do
		if ModuleIsPresent "${module}" ; then
			mToUpdate+=("$module")
		else 
			mToCreate+=("$module")
		fi
	done
	
	n=${#mToCreate[@]}
	[[ $n -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	[[ $n -ge 1 ]] && printf "Need to create $n control module${s}: ${mToCreate[*]} (OK)\n"
	for module in "${mToCreate[@]}" ; do
		Pod_install $module
	done
	
	n=${#mToUpdate[@]}
	[[ $n -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	printf "Need to update $n control module${s}"
	[[ $n -ge 1 ]] && printf ": ${mToUpdate[*]}"
	printf " (OK)\n\n"
	for module in "${mToUpdate[@]}" ; do
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: updating '${module}'"
		moduleDir="${PSM_PROJECT}/${module}"
		"${moduleDir}/_update" $DEFAULT_PROGRAM_RUN_KEY
		printf "\n"
	done
		
	# Now update the installed (and activated) programs
	n=${#PROGRAMS[@]} # (these are the programs defined in the config file)
	[[ $n -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	printf "Found $n program${s} to update"
	if [[ $n -gt 0 ]] ; then 
		printf " (${PROGRAMS[*]})"
	fi
	printf " (OK)\n\n"
	for module in "${PROGRAMS[@]}" ; do
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: updating '${module}'"
		moduleDir="${PSM_PROJECT}/${module}"
		if [[ ! -e "${moduleDir}" ]] ; then
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Can't find program directory ${module}. Nothing more to do."
		else
			"${moduleDir}/_update" $DEFAULT_PROGRAM_RUN_KEY
			
		fi
		printf "\n"
	done
	
	printf "\n"

	# We want this function to execute twice: 
	# first to install the latest codebase, then to rebuild everything based on the new code.
	# When the first pass is complete, re-run it with the 'exit' argument to avoid an infinite loop.
	
	# if the argument list included the word "exit", then exit now.
	ArrayContainsElement "exit" $@ && exit 0
		
	# re-execute the 'update' command, using the updated codebase (to install new commands, etc.)
	${MODULE_DIR}/update nogit exit
}

#############
# Pod_housecall() -- evaluate the health of each module on this pod
# With the optional "send" argument, also invokes Sendbatch to deliver factoids, etc to a remote web server
#############
function Pod_housecall () { 
	local callingFrom=$(TracePath)
	LogMe "${MODULE_LOGFILE}" "${callingFrom} $@ : executing..."
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	HouseCall $@
	return $?
}

#############
# Pod_sendbatch() -- Execute via sftp all the queued batch files
#############
function Pod_sendbatch () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $callingFrom  # get POD_SFTP	

 	if [[ -z ${POD_SFTP+x} ]] ;  then # if POD_SFTP not set
		printf "${callingFrom}: POD_SFTP is not set in ${MODULE_NAME} config file. Nothing to do.\n"
		return 1
	fi
	
	# POD_SFTP is generally of the form USER@HOST:PORT, so parse it to get USER@HOST and PORT
	local uhp=(${POD_SFTP//:/ })  # break it at the ":"
	local userAtHost="${uhp[0]}" # USER@HOST
	[[ ${#uhp[@]} -eq 2 ]] && local sftpPort="-p ${uhp[1]}" || local sftpPort=""
	local podBatchLogFile="${MODULE_LOG_DIR}/${DEFAULT_POD_SENDBATCH_LOGFILE}" 
	
	# gather all the sftp files into an array (see https://stackoverflow.com/a/23357277/5011245)
	local sftpFiles=()
	while IFS=  read -r -d $'\0'; do
		sftpFiles+=("$REPLY")
	done < <(find "${POD_QUEUE_DIR}" -name \*sftp -depth 1 -print0)
	if [[ ${#sftpFiles[@]} -eq 0 ]] ; then
		LogMe "${podBatchLogFile}" "${callingFrom}: sftp queue is empty. Nothing to do."
		return 0
	else
		# assemble all the files into a single batch file
		local tmpBatchFile="${POD_QUEUE_DIR}/tmpBatchFile.$$"
		> "${tmpBatchFile}"	# create and init the tmp batchfile to an empty file
		for f in "${sftpFiles[@]}" ; do
			cat "${f}" >> "${tmpBatchFile}"
		done
		
		local resultMessage errCode n s isare shortName
		resultMessage=$( sftp -b "${tmpBatchFile}" "${userAtHost}" "${sftpPort}" 2>&1) #!! doing "local resultMessage=..." always has exit code 0 !!
		errCode=$?
		if [[ $errCode -eq 0 ]] ; then  # sftp succeeded!
			cat "${tmpBatchFile}" >> "${podBatchLogFile}" # record the uploaded files to the logfile
			for f in "${sftpFiles[@]}" ; do		# delete the queued batch files
				rm "${f}"
			done
			n="$(wc -l ${tmpBatchFile} | awk '{print $1}')"
			[[ $n -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
			shortName="$(basename ${tmpBatchFile})"
			LogMe "${podBatchLogFile}" "${callingFrom}: sftp delivered $n file${s} in ${shortName} to ${userAtHost}." 
			rm "${tmpBatchFile}"	# delete the tmp batchfile
			return 0	# success
		else
			LogMe "${podBatchLogFile}"  "${callingFrom}: *** sftp failed to send files to ${userAtHost} (${sftpFiles[*]}) : ${resultMessage}"
			rm "${tmpBatchFile}"	# delete the tmp batchfile
			return $errCode	# error
		fi
	fi
	
	return 0
}

#############
# HouseCall() -- visit the pod's modules, evaluate their health, and take remedial action, if necessary
# With the optional "send" argument, also invokes sendbatch to deliver factoids, etc to a remote web server
#############
function HouseCall() {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	local doSend="NO"
	if [[ $# -eq 1 ]] ; then
		if [[ $1 = "send" ]] ; then 
			doSend="YES" 
		else
			printf "${callingFrom}: *** Unrecognized argument '$1'. Goodbye. \n"
			return
		fi
	fi
	
	MakeFactoids
	
	# check the system load average
	if ! SystemLoadIsTolerable ; then 
		local loadWarning="${callingFrom}: *** WARNING: high load average: $(SystemLoad). Pod restart recommended."
		LogException "${loadWarning}"
		LogMe "${MODULE_LOGFILE}" "${loadWarning}"
	fi

	SourceConfig $callingFrom
	for program in "${PROGRAMS[@]}" ; do
		local programDir="${PSM_PROJECT}/${program}"
		local buttState=$(${programDir}/status butt)
		local soxState=$(${programDir}/status sox)
		local dirState=$(${programDir}/status dir)
		local programState=$(${programDir}/status program)

		# NB  in the commands below (e.g., buttctl/restart), there is an extra argument -- the reason for
		# invocation of that command. This will log an EXCEPTION to the exception log file.

		# whenever sox is HUNG, restart the program
		if [[ "${soxState}" = "${DEFAULT_MODULE_STATE_HUNG}" ]] ; then
			"${PSM_PROJECT}/${program}/restart" "pod housecall found sox=${soxState}"
		
		# if the program is UP... 
		elif [[ "$programState" = "${DEFAULT_MODULE_STATE_UP}" ]] ; then
			if [[ "${buttState}" = "${DEFAULT_MODULE_STATE_HUNG}" ]] || [[ "${buttState}" = "${DEFAULT_MODULE_STATE_DOWN}" ]] ; then  # ... but BUTT is  HUNG or DOWN,
				"${PSM_PROJECT}/${program}/buttctl" restart "pod housecall found butt=${buttState}" # then restart BUTT
			else # ... BUTT is running OK
				: # do nothing
			fi
					
		# if the program is DOWN and the inbound directory is EMPTY...
		elif [[ "$programState" = "${DEFAULT_MODULE_STATE_DOWN}" ]] && [[ "${dirState}" = "${DEFAULT_MODULE_STATE_EMPTY}" ]] ; then
			if [[ -e "${MODULE_PRELAUNCHER}" ]] ; then # then try to remount the directory and restart the program
				bash "${MODULE_PRELAUNCHER}"
				"${PSM_PROJECT}/${program}/restart" "pod housecall found dir=${dirState}, program=${programState}"
			fi
		fi
		
		# NB We don't automatically restart a DOWN program, because it may be down for a very good (bug in eLoop, esound, etc.)
		# and we don't want to hammer the pod with fruitless and repetitive restarts. 
		# FUTURE FIXME: HouseCall should be able to detect WHY esound etc failed, and then decide whether to restart the program.
		
	done
	
	CollectExceptions	# update a master list of exception log messages that may have been generated from the above

	# with the optional "send" argument, deliver factoids, etc. to the webserver
	if [[ $doSend = "YES" ]] ; then
		Pod_sendbatch
		return $?  # return the sendbatch error
	fi
	
	return 0
}

#############
# CollectExceptions() -- collect the exception log messages from all the programs into one list 
#############
function CollectExceptions() {
	RotateLogFiles "${MODULE_EXCEPTION_LOGFILE}" > /dev/null # (no need to talk about it)
	local tmpFile="${MODULE_WORKSPACE_DIR}/exceptions.tmp"
	> "${tmpFile}" # create and init an empty tmp file
	SourceConfig
	for program in "${PROGRAMS[@]}" ; do
		local progExceptions="${PSM_PROJECT}/${program}/${DEFAULT_MODULE_LOG_DIR}/exception.log"
		if [[ -e "${progExceptions}" ]] ; then
			grep "EXCEPTION" $progExceptions >>  "${tmpFile}"
		fi
	done
	sort $tmpFile | uniq >> "${MODULE_EXCEPTION_LOGFILE}"
}


#############
# Pod_buttctl() -- Execute buttctl on each one of the programs that are defined in the config file
#############
function Pod_buttctl () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if [[ $# -lt 1 ]] ; then
		cat << EOT
Usage: pod/buttcl [start | stop | restart | status]
	start    : start BUTT
	stop     : stop BUTT (if it's running)
	restart  : stop, then start BUTT
	status   : print BUTT's status (running, not-running, etc.)

EOT
		exit
	fi

	SourceConfig $callingFrom  # get PROGRAMS
	
	local buttArg="$1"		
	
	# Is the PROGRAMS variable set?
	if [[ -z ${PROGRAMS+x} ]]; then 
		printf "${callingFrom}: No programs defined in pod's config (${MODULE_CONFIG_FILE}). Nothing to do.\n"
		exit
	fi
	local nPrograms=${#PROGRAMS[@]}
	if [[ $nPrograms -lt 1 ]] ; then 
		printf "${callingFrom}: No programs defined in ${MODULE_CONFIG_FILE}. Nothing to do.\n"
		exit
	fi
	LogBreak "${MODULE_LOGFILE}" "${callingFrom}"
	printf "${callingFrom}: Found $nPrograms programs: ${PROGRAMS[*]}\n"

	for program in "${PROGRAMS[@]}" ; do
		printf "========= Program: ${program} =========\n"
		local programDir="${PSM_PROJECT}/${program}"
		if [[ ! -e "${programDir}" ]] ; then
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** ${program}/buttctl ${buttArg} failed. Can't find program directory ${program}."
		else
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${program}/buttctl ${buttArg}"
			"${programDir}/buttctl" "${buttArg}" | tee -a "${MODULE_LOGFILE}"
		fi
		printf "\n"
	done

}

#############
# Pod_peek() -- view  screen.log
#############
function Pod_peek () { 
	Module_peek $@
}


#############
# Pod_clean() -- clean up log files,  queued batchfiles, and the workspace in each program
#############
function Pod_clean () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	SourceConfig $callingFrom  # get PROGRAMS

	# Are any modules already running?
	local list=($(ListRunningModules all))
	local n=${#list[@]}
	local s isare
	[[ $n -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	if [[ ${n} -gt 0 ]] ; then
		printf "${callingFrom}: *** There ${isare} $n module${s} running (${list[*]}). Please stop ${MODULE_NAME} first.\n"
		exit
	fi
	
	# no modules are running, so it's safe to continue 
	printf "\n"

	# first clean up the pod's own files
	LogBreak "${MODULE_LOGFILE}" "${callingFrom}"
	printf "${callingFrom}: "
	DeleteAllExcept 8675309 ${MODULE_PROTECTED_FILES[*]} 	# delete all but the protected files
	find "${MODULE_WORKSPACE_DIR}" -depth 1 -type f -delete  # clean out the workspace
	
	local podBatchLogFile="${MODULE_LOG_DIR}/${DEFAULT_POD_SENDBATCH_LOGFILE}" 
	RotateLogFiles "${podBatchLogFile}" "${MODULE_EXCEPTION_LOGFILE}"  
	# (don't rotate "${MODULE_LOGFILE}" here; UpdateModule's RecreateModule will take care of that)
	
	# delete any previously queued sftp batchfiles
	if [[ -e "${POD_QUEUE_DIR}" ]] ; then 
		printf "${callingFrom}: Purging upload queue..."
		find "${POD_QUEUE_DIR}" -depth 1 -type f -delete
	else 
		printf "${callingFrom}: Upload queue is already clean." 
	fi
	printf "(OK)\n"

	# now clean each program module
	printf "${callingFrom}: Now cleaning program modules (OK)\n" | tee -a "${MODULE_LOGFILE}"
	printf "\n"
	local m mDir
	for m in "${PROGRAMS[@]}" ; do
		printf "${callingFrom}: cleaning program '${m}' (OK)\n" | tee -a "${MODULE_LOGFILE}"
		mDir="${PSM_PROJECT}/${m}"
		if [[ ! -e "${mDir}" ]] ; then
			printf "${callingFrom}: Can't find module directory ${m}. Nothing more to do.\n" | tee -a "${MODULE_LOGFILE}"
		else
			"${mDir}/clean"
		fi
		printf "\n"
	done
	
	# now clean each control module
	printf "${callingFrom}: Now cleaning control modules (OK)\n" | tee -a "${MODULE_LOGFILE}"
	printf "\n"
	local mm=($(ListActiveModules control))
	for m in ${mm[@]} ; do
		mDir="${PSM_PROJECT}/${m}"
		if [[ ! -e "${mDir}" ]] ; then
			printf "${callingFrom}: Can't find module directory ${m}. Nothing more to do.\n" | tee -a "${MODULE_LOGFILE}"
		else
			"${mDir}/clean"
		fi
		printf "\n"
	done
	
	printf "${callingFrom}: Refreshing ${MODULE_TYPE} files (OK)\n" | tee -a "${MODULE_LOGFILE}"
	UpdateModule nogit && LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_TYPE} module cleaned. (OK)"
}


function Pod_val () {
	ValidateModule $@ # i.e., shared.sh::ValidateModule
}

#
# PrepareParams() -- tidy up config params and construct any needed ones from those in the config
# If $1="verbose" then printf any (non-fatal) warnings
# Returns the number of (non-fatal) warnings
function PrepareParams () {
	local warningCount=0
	
	# derive a PROGRAMS array from the MODULES array
	# (PROGRAMS are all the module names that are NOT slink, dispatcher, etc.)
	PROGRAMS=()
	local m
	for m in ${MODULES[@]} ; do
		if ! ArrayContainsElement "${m}" ${DEFAULT_RESERVED_MODULE_NAMES[@]} ; then
			if [[ ! -d "${PSM_PROJECT}/${m}" ]] ; then  # if the named module directory doesn't exist
				: 			# then skip it
			else
				PROGRAMS+=( "${m}" )  # it exists, so add it to the list
			fi
		fi
	done
	
	# override the default parameter
	[[ ! -n "${POD_WEBHOST_ROOT_DIR}" ]] && POD_WEBHOST_ROOT_DIR="${DEFAULT_POD_WEBHOST_ROOT_DIR}" # CAUTION: this line also appears in shared-functions.sh
	
	return $warningCount
}


#############
# ValidateChildrenCallback() -- add a list of invalid child modules to the global $_OFFENSIVE_DESCENDANTS
#############
function ValidateChildrenCallback () {
	SourceConfig # get PROGRAMS
	if [[ ${#PROGRAMS[@]} -gt 0 ]] ; then
		for program in "${PROGRAMS[@]}" ; do	
			local programDir="${PSM_PROJECT}/${program}"
			if [[ ! -d "${programDir}" ]] ; then # Program is missing. Can't find directory
				_OFFENSIVE_DESCENDANTS=("${_OFFENSIVE_DESCENDANTS[@]}" "$program")
				continue
			fi
			if [[ ! -f "${programDir}/val" ]] ; then # if the program doesn't have the "val" command
				# Program is incomplete and needs updating 
				_OFFENSIVE_DESCENDANTS=("${_OFFENSIVE_DESCENDANTS[@]}" "$program")
				continue
			fi
			local childErrCount="$( ${programDir}/val count )" 	# count up the errors in the program
			if [[ ! -z ${childErrCount+x} ]] && [[ "${childErrCount}" != "" ]]; then
				if [[ $childErrCount != 0 ]] ; then
					_OFFENSIVE_DESCENDANTS=("${_OFFENSIVE_DESCENDANTS[@]}" "$program")
				fi
			fi
		done
	fi
}


#
# ValidateParamCallback() -- Validate a single parameter
# This is called by shared.sh::ValidateModule.
# It examines a single variable (name supplied in $1) to see if its value ($@) is appropriate
# If there is an error, print a single line that describes the error.
# If no error, print nothing. 
# shared.sh::ValidateModule takes care of tallying up errors and printing the result.
# For cleaner output formatting, do not include colons in the error message.
function ValidateParamCallback () {
	local variableName="$1"
	shift ; local args=($@)  # $args is everything after $1
	
	local s isare
	local reply=""
	case "$variableName" in
	FOO) # (for example)
		if [[ "${args[0]}" != "1234" ]] ; then
			reply="*** Wrong value (should be 1234, but is ${args[@]})"
		fi
		;; 
				
	MODULES)
		# we expect the arguments to be the names of installed modules
		# let's count up the number of missing modules
		local m nInstalled=0
		local mMissing=()
		for m in "${args[@]}" ; do 
			local moduleDir="${PSM_PROJECT}/$m"
			if [[ ! -d "$moduleDir" ]] ; then
				mMissing+=($m)
			else 
				(( nInstalled++ ))
			fi
		done
		if [[ ${#mMissing[@]} -gt 0 ]] ; then
			[[ ${#MODULES[@]} -eq 1 ]] && { s="";isare="is"; } || { s="s"; isare="are"; } # pluralizer
			reply="*** ${#args[@]} module${s} declared,"
			[[ $nInstalled -eq 1 ]] && { s="";isare="is"; } || { s="s"; isare="are"; } # pluralizer
			reply="${reply} but only $nInstalled ${isare} actually installed"
			[[ ${#mMissing[@]} -eq 1 ]] && { s="";isare="is"; } || { s="s"; isare="are"; } # pluralizer
			reply="${reply} (missing module${s}: ${mMissing[*]})"
		fi
		;;
	TERMCOLORS)
		if [[ "${#args[@]}" -ne 2 ]] ; then
			reply="*** Wrong number of colors (${#args[@]}). 2 expected."
			break
		fi

		local color
		local nBad=0
		source "${SCRIPTS_DIR}/colors.sh"
		for color in "${args[@]}" ; do 
			if ! isColorValid ${color} ; then 
				(( nBad++ ))
			fi
		done
		if [[ $nBad -gt 0 ]] ; then
			[[ $nBad -eq 1 ]] && { s=""; isare="is"; } || { s="s"; isare="are"; } # pluralizer
			reply="*** $nBad invalid color${s}"
		fi
		;;
	*)
		;;
	esac
	
	printf "${reply}"
}

# ListRunningModules() -- print a space-delimited list of the modules that are running
# With no argument, prints all the running modules. With an argument, print a list of specific
# module types: "program" or "control"
function ListRunningModules () {
	local list=""  # a list of running modules

	# if no args, list running modules of all types
	if [[ $# -eq 0 ]] ; then 
		ListRunningModules all
		return
	fi
	
	case "$1" in
	program) #  Audio programs
		SourceConfig # get PROGRAMS
		for m in "${PROGRAMS[@]}" ; do
			ModuleIsRunning $m && list="${list}${m} "
		done
		;; 
	control)  # control modules
		for m in "${DEFAULT_RESERVED_POD_CONTROL_MODULES[@]}" ; do
			ModuleIsRunning $m && list="${list}${m} "
		done
		;;
	all) # list them all
		list="$(ListRunningModules program) $(ListRunningModules control)"
		;;
	*)  # list them all
		;;
	esac
	list="${list/# /}" #strip off leading ' ' 
	list="${list/% /}" #strip off terminal ' ' 
	printf "${list}" 
}


# CountRunningModules() -- print the number of running modules
# This relies on ListRunningModules() generating a space-delimited string of module names:
#	 "" (zero-length string, no spaces) ==> no modules
#	"foo" (0 spaces) ==> 1 module 
#	"foo bar" (1 space) ==> 2 modules
# 	etc.
function CountRunningModules() {
	local list="$(ListRunningModules $1)"
	[[ "${#list}" -eq 0 ]] && printf "0" && return  # if list is empty, print '0'
	
	local s=$(printf "${list}" | sed -e "s/[^ ]//g") # strip all chars except the spaces
	local n=$(( ${#s} + 1 )) # module count is 1 + number of spaces.
	printf "${n}"
}


# ListActiveModules() -- print a space-delimited list of the "active" modules of the given type.
# A module is "active" if it is both (a) installed and (b) listed in the MODULES array in the config file.
# With no argument, prints all the active modules. 
# With an argument, print a list of specific module types: i.e., "program" or "control"
function ListActiveModules () {
	local list=""  # a list of modules

	# if no args, list modules of all types
	if [[ $# -eq 0 ]] ; then 
		ListActiveModules all
		return
	fi
	
	case "$1" in
	program) #  Audio programs
		SourceConfig  # get PROGRAMS
		for m in "${PROGRAMS[@]}" ; do
			ModuleIsPresent $m && list="${list}${m} "
		done
		;; 
	control)  # control modules
		SourceConfig  # get MODULES
		for m in "${MODULES[@]}" ; do
			if ArrayContainsElement "${m}" ${DEFAULT_RESERVED_MODULE_NAMES[@]} && ModuleIsPresent $m ; then
				list="${list}${m} "
			fi
		done
		;;
	all) # list them all
		list="$(ListActiveModules program) $(ListActiveModules control)"
		;;
	*)  # list them all
		;;
	esac
	list="${list/# /}" #strip off leading ' ' 
	list="${list/% /}" #strip off trailing ' ' 
	printf "${list}" 
}

# CountActiveModules() -- print the number of active modules
# This relies on ListActiveModules() generating a space-delimited string of module names:
#	 "" (zero-length string, no spaces) ==> no modules
#	"foo" (0 spaces) ==> 1 module 
#	"foo bar" (1 space) ==> 2 modules
# 	etc.
function CountActiveModules() {
	local list=($(ListActiveModules $1))
	printf ${#list[@]}
}



#############
# ModStop() -- kill one or more of the pod's running modules
# Does NOT invoke sendbatch to deliver any queued files
#############
function ModStop () {
	if [[ $# -ne 2 ]] ; then
		printf "***** Usage: ModStop MODULETYPE *****\n"
		exit
	fi
	
	local callingFrom="${1}"
	local modType="${2}"
	
	# make sure modType is reasonable
	[[ "${modType}" != "control" ]] && [[ "${modType}" != "program" ]] && LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** Unknown module type '${modType}'. Can't continue." && exit
	
#	shift ; shift
#	local modules=($@) # the list of modules to stop ( if ar
	
	local m n s isare string modules
	modules=($(ListRunningModules ${modType}))
	n=${#modules[@]}
	[[ $n -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	string="${callingFrom}: ${n} ${modType} module${s} ${isare} running"
	[[ $n -gt 0 ]] && string="${string} ${modules[@]}" || string="${string}. Nothing to stop."
	LogMe "${MODULE_LOGFILE}" "${string}"
	if [[ $n -gt 0 ]] ; then
		for m in "${modules[@]}" ; do
			ModuleIsRunning $m && {
				printf "========= Stopping ${m} =========\n" ; 
				LogMe "${MODULE_LOGFILE}" "${callingFrom}: stopping ${m}." ;
				"${PSM_PROJECT}/${m}/stop" ; 
				printf "\n" ; 
			}
		done
	fi
}


#############
# Pod_syncstart() -- Play the program modules in sync with each other
# To do this, we launch each program, wait for the audio files to appear, then play them
# all "simultaneously" (i.e., invoke the 'play' command on each one, in the background)
#############
function Pod_syncstart() {
	local callingFrom=$(TracePath)
	# if the pod's sync directory doesn't exist, then create it
	if [[ ! -d "${POD_SYNC_DIR}" ]] ; then  
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Creating pod sync dir at ${POD_SYNC_DIR}..."
		mkdir $POD_SYNC_DIR
	fi
	
	# clear the sync dir of any audio files
	find $POD_SYNC_DIR -name *.aif -exec rm {} \;

	# Start the programs
	SourceConfig # get PROGRAMS from the config
	
	local loopCount=0
	
	# loop forever: launch programs, wait for audio files to appear, then play the audio files...
	while true ; do
		(( loopCount++ ))
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Starting syncplay loop ${loopCount}..."
		
		local referenceTime=`date -u "+%Y%m%d%H%M%S"`	# ...use the present time as the reference time (note -u for "Universal" (GMT) time)
		# launch all the programs
		for program in "${PROGRAMS[@]}" ; do
			local programDir="${PSM_PROJECT}/${program}"
			local logFile="${programDir}/log/sync.log"
			# start the program
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Reference time ${referenceTime} : starting program ${program}..."

			# delete the previous first-pass audio file from the workspace directory
			### !! FIXME? CAUTION! the audio file name (*.1.post.aif) is defined in eLoopLaunch() !!
			### This means that the filename here must match that given in eLoopLaunch!
			/bin/rm -f "${programDir}/workspace/${program}.1.post.aif"	
			
			# launch it
			
			# launch a subshell to get the right parameters and functions for this program
			(
				MODULE_NAME="${program}"
				cd "${programDir}"
				source "${SCRIPTS_DIR}/program-functions.sh" # load its functions
				source "${SCRIPTS_DIR}/program-eLoop.sh"
			
				# launch the program
				eLoopLaunch "${DEFAULT_PROGRAM_RUN_KEY}" LOOPMAX=1 PLAYCHIME=NO PLAYOUT=NO SKIP_GRAPHICS=YES >> "${logFile}"
			) & # end of subshell (run in background)

		done
		

		wait # wait for the above background processes to terminate
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Reference time ${referenceTime} : finished generating audio files."

		local devices=()
		
		# collect audio files and move them into the pod's "sync" directory		
		for program in "${PROGRAMS[@]}" ; do
			local programDir="${PSM_PROJECT}/${program}"
			/bin/mv "${programDir}/workspace/${program}.1.post.aif" "${POD_SYNC_DIR}/."
		done

		# collect the audioFile names into an array
		local audioFiles=( $(find ${POD_SYNC_DIR} -name *.aif) )
		local devices=() #the audio out device name for each program
		for aFile in "${audioFiles[@]}" ; do
			# extract the program name from the audio file
			program=$(echo `basename $aFile` | cut -d . -f 1) # (i.e., /usr/bin/cut using delimiter "." and return 1st field)
			device=$(GetParam $program "AUDIO_DEVICE") # get the value of the AUDIO_DEVICE parameter from this program
			devices+=($device) # push the device name into the array
		done

		# Now we have two parallel arrays: the audio files, and their associated audio device names

		# if there aren't any audio files, then something went wrong
		if [[ ${#audioFiles[@]} -eq 0 ]] ; then
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: No audio files found. Can't play anything."
			exit
		fi
		
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Reference time ${referenceTime} : playing audio files."

		# Now start playing each file, one after the other	(play each in background)
		# This is as close to "simultaneous" as we can get. My tests (using the 'time' command)
		# suggest that it takes ~10 msec between each invocation of sox. With GAMMA=1000, for example,
		# this corresponds to a real (Earth) time delay of 10 secs, which is within the range of the period 
		# of many seismic surface waves. This is a tolerable delay and shouldn't adversely affect the 
		# illusion of simultaneity too much.
		for (( i=0; i<${#audioFiles[@]}; i++ )) ; do 
			aFile=${audioFiles[$i]}
			program=$(echo `basename $aFile` | cut -d . -f 1) # (i.e., /usr/bin/cut using delimiter "." and return 1st field)
			device=${devices[$i]}
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Reference time ${referenceTime} : playing ${program} to '${device}'."
#			"${BIN_SOX}" --buffer 100000 -q ${aFile} -t coreaudio "${device}" &
			"${BIN_SOX}" --buffer 100000 -q ${aFile} -t coreaudio "${device}" compand 0.3,0.8 6:-70,-60   &
		done

		wait # wait for all the audio files to finish playing
		
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Reference time ${referenceTime} : finished playing audio."

	done # main while loop
	
}

# GetParam( MODULE_NAME PARAMETER )
# Returns the value of the named PARAMETER from MODULE_NAME's config file
function GetParam() {
	if [[ $# -ne 2 ]] ; then
		printf "*** GetParam: Bad argument count ($#). Exactly two required.\n"
		exit
	fi
 	# execute the following in a sub-shell so as not to collide with pod's own parameters 
 	# (such as "MODULE_NAME")
 	( # begin sub-shell
	MODULE_NAME=$1
	param=$2 	# e.g., "STATION"
	source "${SCRIPTS_DIR}/program-functions.sh" # get the program functions
	source "${PSM_PROJECT}/$MODULE_NAME/program.conf"
	PrepareParams # clean up the config file's parameters
	value=$(eval echo \$$param) # i.e., convert "STATION" to $STATION and eval it
	echo $value
	) #end of sub-shell
}