#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
# Plots the transfer function of multiple SoX filters
# SoX's 'plot' function only plots the xfer function of the first filter in its command line.
# This script runs SoX several times, once per filter, to collect the pieces of the xfer function for each filter,
# edits them (via sed), and merges them into a single gnuplot file.

ME="$(basename $0 )"	# the name of this script

function UsageExit () {
	cat << EOT
Usage: ${ME} audio_file
Makes a valiant attempt to plot the resultant transfer function of multiple SoX filters.

For example, to plot the companders, do this:
	% sox --plot gnuplot myfile.aif -n compand  0.001,0.001 -80,-70,-20,-10,0,-3 | gnuplot
	
EOT
	exit
}

case "$#" in
"1")
	inFile="$1"
	;; 
*)
	UsageExit
    ;;
esac


###############
# insert filters here:

# phme1a
filters=("highpass  -2 90" "equalizer  300 2q -6"	"equalizer 400 2q -6" 	"equalizer  90 12q 6" )

#phme2a
filters=("highpass -2 60" "highpass -2 60" "highpass -2 60")
###############
printf "Got ${#filters[@]} filters:\n"


comboPlotfile="comboPlot.txt"

# insert the first chunk of the plot file
cat << EOT  > $comboPlotfile
# gnuplot file
set title 'SoX transfer function (${#filters[@]} combined filters)'
set xlabel 'Frequency (Hz)'
set ylabel 'Amplitude Response (dB)'
Fs=44100
o=2*pi/Fs
EOT


# get the app path variables
APPS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/APPS.sh" 

# now get the transfer function bits
TRANSFERFUNCTION="1"
index=0
xPos=15;	# x-coord of labels
yPos=20;	# initial y coord of labels
for filter in "${filters[@]}" ; do
	(( index++ ))
	yPos=`expr $yPos - 2`
	plotFile="filter-${index}.txt"
	printf "${index}. ${filter}\n" 
	printf "set label 100${index} at ${xPos},${yPos} \"${index}. ${filter}\" front left tc rgb \"#00000\" font \",10\"\n" >> $comboPlotfile
	"${BIN_SOX}" --plot gnuplot "${inFile}" -n $filter > $plotFile
	grep "b0=" $plotFile | sed -e "s|\([ab]\)|K${index}\1|g" >> $comboPlotfile
	grep "H(f)=sqrt" $plotFile | sed -e "s|\([ab]\)|K${index}\1|g" -e "s/H(f)=/H${index}(f)=/" >> $comboPlotfile
	TRANSFERFUNCTION="${TRANSFERFUNCTION}*H${index}(f)"
	printf "#\n" >>  $comboPlotfile
done

# insert the last chunk of the plot file
cat << EOT  >> $comboPlotfile
set logscale x
set samples 250
set grid xtics ytics
set key off
plot [f=10:Fs/2] [-35:25] 20*log10(${TRANSFERFUNCTION})
#pause -1 'Hit return to continue'
EOT


"${BIN_GNUPLOT}" $comboPlotfile
