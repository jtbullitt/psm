#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
ME="$(basename $0 )"	# the name of this script

# Given an audio file, this script generates a low-fi version for the website 
# Script should be executed at the end of the day, after the archive files are saved.

# Exactly two arguments are required: the input AIFF file and the output mp3 file
if [[ $# -eq 4 ]]
then
	aiffPath="$1"
	mp3Path="$2"
	year="$3"
	songName="$4"
else
	printf "Usage: ${ME} inputFile outputFile year songName\n"
	exit
fi

# if the audio file doesn't exist, there's nothing we can do. 
if [ ! -f $aiffPath ]; then
	printf "${ME}: File '${aiffPath} doesn't exist.\n"
	exit
fi

# get the app path variables
APPS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/APPS.sh" 
source "${APPS}"

# create the mp3 File
printf "${ME}: creating mp3...\n"
"${BIN_SOX}" $aiffPath $mp3Path

#edit the ID3 tags
printf "${ME}: editing ID3V2 tags...\n"

"${BIN_ID3V2}" --artist "JT Bullitt" --album "The Earthsound Project" \
	--year "${year}" --song "${songName}" \
	--TCON "Ambient" \
	--TCOM "Planet Earth" \
	--TCOP "Copyright ${year} jtb" \
	--WOAR "jtbullitt.com" \
	--WOAF "earthsound.org" \
	--COMM "earthsound.org" \
	$mp3Path

printf "${ME}: done.\n"
