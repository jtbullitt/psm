#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)

# These are the common constants and functions shared by other scripts
# To set the variables correctly, be sure to set "MODULE_TYPE=..." before source'ing this file.
# Also, the PSM_PROJECT environment variable must be set

if [[ -z ${PSM_PROJECT+x} ]] ; then 
	printf "*** Fatal error in shared.sh (line ${LINENO}): Environment variable PSM_PROJECT must be set before source'ing shared.sh!\n"
	exit
fi
if [[ -z ${MODULE_TYPE+x} ]] ; then 
	printf "*** Fatal error in shared.sh (line ${LINENO}): MODULE_TYPE must be set before source'ing shared.sh!\n"
	exit
fi
if [[ -z ${MODULE_NAME+x} ]] ; then 
	printf "*** Fatal error in shared.sh (line ${LINENO}): MODULE_NAME must be set before source'ing shared.sh!\n"
	exit
fi

#PSM_PROJECT_DIR="$( cd $( dirname ${BASH_SOURCE[0]})/../.. && pwd)"
# The above is just a note to self, in case someday I want to get rid of the need for PSM_PROJECT as an environment variable

PSM_DIR="$( cd ${PSM_PROJECT}/psm && pwd )"	# the dir wherein psm dirs reside
SCRIPTS_DIR="$( cd ${PSM_DIR}/sh && pwd)"	# the dir wherein the scripts reside
ASSETS_DIR="$( cd ${SCRIPTS_DIR}/assets && pwd)"	# the dir wherein miscellaneous script assets reside
BIN_DIR="$( cd ${PSM_DIR}/bin && pwd )" # the dir wherein the binary executables reside
LIB_DIR="$( cd ${PSM_DIR}/lib && pwd )" # the dir wherein the dylibs reside
TEMPLATE_DIR="$( cd "${SCRIPTS_DIR}/templates" && pwd )" # the dir wherein the script templates reside
MODULE_DIR="${PSM_PROJECT}/${MODULE_NAME}"
source "${SCRIPTS_DIR}/DEFAULTS.sh" # get the default params
source "${SCRIPTS_DIR}/APPS.sh" # get the apps

if [[ ${#DEFAULT_AUDIO_DEVICE_PREFIX} -lt 6 ]] ; then
	printf "*** Fatal error in shared.sh (line ${LINENO}): DEFAULT_AUDIO_DEVICE_PREFIX (${DEFAULT_AUDIO_DEVICE_PREFIX}) must be at least 6 characters long.\n"
	printf "*** (This is due to a bug in SoX, whereby SoX misreads the names of virtual audio devices.)\n"
	exit
fi

MODULE_ESSENTIAL_DIRS=( $DEFAULT_MODULE_LOG_DIR $DEFAULT_MODULE_WORKSPACE_DIR ) # dirs that exist in all modules
MODULE_LOG_DIR="${MODULE_DIR}/${DEFAULT_MODULE_LOG_DIR}"
MODULE_WORKSPACE_DIR="${MODULE_DIR}/${DEFAULT_MODULE_WORKSPACE_DIR}"
MODULE_LOGFILE="${MODULE_LOG_DIR}/${MODULE_TYPE}.log"
MODULE_EXCEPTION_LOGFILE="${MODULE_LOG_DIR}/exception.log" # a logfile containing the "reasons" that were supplied to commands (usu to address detected by a pod)
MODULE_CONFIG_FILE="${MODULE_DIR}/${MODULE_TYPE}.conf"
MODULE_SCREENRC_FILE="${MODULE_DIR}/${DEFAULT_MODULE_SCREENRC_FILE}" # e.g., "/Users/jtb/project/dispatcher/.screenrc"
MODULE_SCREEN_LOGFILE="${MODULE_LOG_DIR}/${DEFAULT_MODULE_SCREEN_LOGFILE}"
# grep "signatures" for use with 'ps' for detecting which processes are running
DISPATCHER_PROCESS_SIGNATURE="source ${SCRIPTS_DIR}/dispatcher-functions.sh; Dispatcher" # search pattern for getting the Dispatcher pid from ps | grep ...
SLINKSWEEPER_PROCESS_SIGNATURE="source ${SCRIPTS_DIR}/slink-functions.sh; SlinkSweeper" # for searching output of 'ps'
SLARCHIVE_PROCESS_SIGNATURE="slarchive -l .* -A " # for searching output of 'ps'
DISPMON_PROCESS_SIGNATURE="source ${SCRIPTS_DIR}/dispatcher-functions.sh; DispMon"

# This global array that will contain names of descendant modules that contain errors
_OFFENSIVE_DESCENDANTS=()


#############
# CheckEnvironment() -- validate shell environment variables, etc.
#############
function CheckEnvironment () {
	local outputMode="$1"
	local errors=()

	if [[ -z ${PSM_PROJECT+x} ]] ; then 
		errors=( "${errors[@]}" "Environment variable PSM_PROJECT is not set.")
	else	# no error
		: # do-nothing
	fi
	
	# check that XCode command-line tools are available 
	if [[ $((xcrun --version 2>&1) > /dev/null; echo $?) -ne 0 ]] ; then
		errors=( "${errors[@]}" "XCode command line tools are missing. Please run 'xcode-select --install'.")
	fi

	local e
	case "$outputMode" in
	"verbose")
		if [[ ${#errors[@]} -gt 0 ]] ; then
			for e in "${errors[@]}" ; do
					printf "%10s\n" "*** ${e}"
			done
			printf "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
			printf "!!! This pod will not be reachable from its dispatcher until you fix this.\n"
			printf "!!! Please do this:\n"
			printf "!!!   1. Edit ~/.bash_profile and insert this line at the top:\n"
			printf "!!!      source ~/.profile ; source ~/.bashrc\n"
			printf "!!!   2. Edit ~/.profile and add this line:\n"
			printf "!!!      PSM_PROJECT='${TOP_DIR}' ; export PSM_PROJECT\n"
			printf "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
		else
			printf "Environment is OK\n"
		fi
		;;
	"count")
		printf "${#errors[@]}"
		;;
	"*")
		printf "${#errors[@]}"
		;;
	esac
}



#############
# RotateLogFiles() -- create clean logfiles (and save previous, if any)
# arguments are the full paths to the log file(s)
#############
function RotateLogFiles () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	if [[ $# -lt 1 ]] ; then
		printf "*** ${thisFunction}: missing arguments (no log files!).\n"
		exit
	fi
	 
	# create log files if none exist
	local logfiles=($@)
	printf "${callingFrom}: Setting aside old logfiles: "
	
	for logfile in ${logfiles[@]} ; do
		if [[ -f "${logfile}" ]] ; then  # if one already exists,
			printf "[%s] " $(basename "${logfile}")
			printf "$(date -u "+${DEFAULT_TIME_FORMAT}"): Setting aside this logfile.\n" >> "${logfile}"
			mv "${logfile}" "${logfile}~"	# set aside the previous one
		else
			printf "%s " $(basename "${logfile}")
		fi
		printf "Logfile created $(date -u "+${DEFAULT_TIME_FORMAT}") by ${thisFunction}\n\n" > "${logfile}"
	done
	printf "(OK)\n"
}

#############
# LogMe() -- append a timestamped message to the logfile (and stdout)
# first arg is the full path to the log file
#############
function LogMe () {
	if [[ $# -lt 2 ]] ; then
		printf " *** ${FUNCNAME[0]} requires 2 or more arguments.\n"
		exit
	fi
	local logfile="$1"
	shift
	
	# if the logfile doesn't exist (i.e., we've started a brand-new install),
	# print the message only to the terminal.
	if [[ -f "${logfile}" ]] ; then	
		printf "$(date -u "+${DEFAULT_TIME_FORMAT}") @ $(PodName): $@\n" | tee -a "$logfile"
	else
		printf "$(date -u "+${DEFAULT_TIME_FORMAT}") @ $(PodName): $@\n" 
	fi
}

#############
# LogException() -- append a timestamped message to the reason logfile
#############
function LogException () {
	if [[ $# -lt 1 ]] ; then
		printf " *** ${FUNCNAME[0]} requires 1 or more arguments.\n"
		exit
	fi
	printf "$(date -u "+${DEFAULT_TIME_FORMAT}") @ $(PodName): EXCEPTION $@\n" | tee -a "${MODULE_EXCEPTION_LOGFILE}"
}

#############
# NthException() -- get the n'th most recent entry from the exception log file
#	Ex: if the exception log file contains 20 entries:
#		NthException 1	# prints the most recent entry
#		NthException 2	# prints the second-most recent entry
#		NthException 20	# prints the first (oldest) entry
#		NthException 21	# prints ""
#		NthException 0	# prints ""
#		NthException	# prints ""
#		NthException 1 678 XYZ # prints ""
#############
function NthException () {
	if [[ $# -ne 1 ]] ; then
		printf ""
		return
	fi
	local nth="$1"
	if [[ $nth -le 0 ]] ; then
		printf ""
		return
	fi
	
	# how many entries are present in the log file
	if [[ -e $MODULE_EXCEPTION_LOGFILE ]] ; then # if the exception file exists
		local nExceptions=$(grep "EXCEPTION" $MODULE_EXCEPTION_LOGFILE | wc -l | sed -e "s/ *//g")
		if [[ $nExceptions -lt $nth ]] ; then
			printf ""
			return
		fi	
		local result=$(grep "EXCEPTION" $MODULE_EXCEPTION_LOGFILE | tail -n $nth | head -1)
		printf "${result}\n"
	else # exception file does not exist
		printf ""
		return
	fi	
	return
}


#############
# LogBreak() -- insert a timestamped visual break to the logfile
# first arg is the full path to the log file
#############
function LogBreak () {
	if [[ $# -lt 2 ]] ; then
		printf " *** ${FUNCNAME[0]} requires 2 or more arguments.\n"
		exit
	fi
	local logfile="$1"
	shift
	printf "===========================\n$(date -u "+${DEFAULT_TIME_FORMAT}") @ $(PodName): $@\n" | tee -a "$logfile"
}

#############
# InstallCommand COMMAND
# Installs a fresh version of the COMMAND script into the MODULE_TYPE's dir.
#############
function InstallCommand () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if [[ $# -ne 1 ]] ; then
		printf "*** InstallCommand requires exactly 1 argument; got $#.\n"
		printf "Usage: InstallCommand TEMPLATE \n"
		exit
	fi
	local theCommand="$1"	# e.g., "start"
	local src="${TEMPLATE_DIR}/module-command.txt" # e.g., /foo/bar/templates/module-command.txt
	if [[ ! -e "${src}" ]] ; then
		printf "*** ${callingFrom}: InstallCommand can't find source template file '${template}'. Nothing to do.\n"
		exit
	fi

	FilterTemplate "command" "${theCommand}"
	
	chmod +x "${MODULE_DIR}/${theCommand}"
}

#
# FilterTemplate() -- installs a template file into the directory MODULE_DIR
# Usage:
#		FilterTemplate NAME [ALTNAME]
# where NAME is the installed basename of the file.
# The file will be generated from a template whose name is either
#		${MODULE_NAME}-NAME
# or	module-NAME
# Example:
#		FilterTemplate foo  [called by program-functions.sh]
# will look for these templates, in order, until it finds one:
#		1. program-foo.txt
# 		2. module-foo.txt	
# In each case, the template will be filtered, resulting in a file named "foo" in the module directory.
# With the optional ALTNAME, the file will be named ALTNAME instead.
# The optional arg ALTNAME causes the template to be installed to a difference file basename.
# Example:
#		FilterTemplate .screenrc		:	module-.screenrc.txt ===> .screenrc
#		FilterTemplate command start  	:	module-command.txt ===>	start
#		FilterTemplate jamaseis.txt  	:	program-jamaseis.txt.txt ===>	jamaseis.txt
#
function FilterTemplate () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if [[ $# -lt 1 ]] && [[ $# -gt 2 ]] ; then
		printf "*** FilterTemplate: 1 or 2 args expected; got $#.\n"
		exit
	fi
	
	
	local dest
	if [[ $# -eq 1 ]] ; then
		dest="${MODULE_DIR}/$1"
	else
		dest="${MODULE_DIR}/$2"
	fi

	
	
	local basename1="${MODULE_TYPE}-${1}.txt" 	# e.g.: first try look for template "program-.screenrc.txt" or "program-program.conf"
	local basename2="module-${1}.txt" 			# if NG, then try look for template "module-.screenrc.txt" or "module-program.conf"
	if [[ ! -f "${TEMPLATE_DIR}/${basename1}" ]] ; then 
		if [[ ! -f "${TEMPLATE_DIR}/${basename2}" ]] ; then 
			printf "*** ${thisFunction}: can't find either template '${basename1}' or '${basename2}'.\n"
			return
		else 
			src="${TEMPLATE_DIR}/${basename2}"
		fi
	else
		src="${TEMPLATE_DIR}/${basename1}"
	fi
	
	# If installing a program config file and there's a custom station template, then use that instead
	# (Will not install if there's more than one arg -- e.g., when installing the demo config file
	templatePath="${TEMPLATE_DIR}/stations/${MODULE_NAME}.conf.txt"
	if [[ $# -eq 1 ]] && [[ "${1}" = "program.conf" ]] && [[ -f "${templatePath}" ]] ; then
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Installing special station config template from `basename \"${templatePath}\"` (OK)"
		src="${templatePath}"
	fi
	
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: version $(Version full) installed to '${MODULE_NAME}'."

	
	
		
	local ModuleTitleized="$(tr '[:lower:]' '[:upper:]' <<< ${MODULE_TYPE:0:1})${MODULE_TYPE:1}" # e.g., "program" ==> "Program"
	local SCRIPTS_DIR_UNEXPANDED="\$PSM_PROJECT/psm/sh" # to insert "$PSM_PROJECT/psm/sh" into the template

	sed -e "/^#%#%#%/d" \
		-e "s|__MODULE__|${MODULE_TYPE}|g" \
		-e "s|__MODULETITLEIZED__|${ModuleTitleized}|g" \
		-e "s|__COMMAND__|${theCommand}|g" \
		-e "s|__NOWSTRING__|$(date -u "+${DEFAULT_TIME_FORMAT}")|g" \
		-e "s|__AUDIO_DEVICE_PREFIX__|${DEFAULT_AUDIO_DEVICE_PREFIX}|g" \
		-e "s|__THISFUNCTION__|${thisFunction}|g" \
		-e "s|__SCRIPTS_DIR__|${SCRIPTS_DIR_UNEXPANDED}|g" \
		-e "s|__BIN_DIR__|${BIN_DIR}|g" \
		-e "s|__CONFIGFILE__|${MODULE_CONFIG_FILE}|g" \
		-e "s|__PATH_TO_LOGFILE__|${MODULE_LOGFILE}|g" \
		-e "s|__SCREEN_LOGFILE__|${MODULE_SCREEN_LOGFILE}|g" \
		"${src}" > "${dest}" 
}






####################
# getNiceFileAge() -- prints the age of the given file
####################
function getNiceFileAge () {
	if [[ $# -ne 1 ]] ; then
		printf "x"
#		printf "${FUNCNAME[0]}: *** Missing filename.\n"
		return
	fi
	
	local theFile="$1"
	if [[ ! -e "${theFile}" ]] ; then
		printf "x"
#		printf "${FUNCNAME[0]}: *** Can't find file '${theFile}'.\n"
		return
	fi

	local d=$( stat -f%m ${theFile} )
	local secs=$(( $( date +%s ) - $d ))
	
	local days=$(( $secs / 86400 )) # get the days (integer)
	secs=$(( $secs - $(( $days * 86400)) )) # subtract out the days
	
	local hours=$(( $secs / 3600 ))
	secs=$(( $secs - $(( $hours * 3600)) )) # subtract out the hours
	
	local mins=$(( $secs / 60 ))
	secs=$(( $secs - $(( $mins * 60)) )) # subtract out the minutes
	
	printf "%dd %02d:%02d:%02d\n" $days $hours $mins $secs
}



# convert relative path to absolute (from https://superuser.com/a/218684/551666)
function abspath () { pushd . > /dev/null; if [ -d "$1" ]; then cd "$1"; dirs -l +0; else cd "`dirname \"$1\"`"; cur_dir=`dirs -l +0`; if [ "$cur_dir" = "/" ]; then echo "$cur_dir`basename \"$1\"`"; else echo "$cur_dir/`basename \"$1\"`"; fi; fi; popd > /dev/null; }



#############
# InitFactoids() -- Create a new factoid file and fill it with some basic info
# Single arg (required) is the full path to the factoid file 
#############
function InitFactoids() {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if [[ $# -ne 1 ]] ; then
		printf "${callingFrom}: *** InitFactoids bad arg count  ($#).\n"
		printf "Usage: ${callingFrom} /path/to/factoids/file \n"
		exit
	fi
	
	# get the sshfs version number (sshfs is only required for the radio module)
	local sshfsVersion
	local sshfs=($(which sshfs))
	[[ ${#sshfs[@]} -eq 0 ]] && sshfsVersion="NA" || sshfsVersion=$( sshfs --version 2> /dev/null | head -1)
	# (NB: redirect "fuse: no mount point" warning to /dev/null)
	
	local factoidPath="$1"
	local podName=$(PodName)
	local podNameFull=$(SanitizeIP $(PodName full))
	local now="$(date -u)"
	local diskFree=( $(df -g / | tail -1 ) ) # get df info about the root filesystem (e.g., get "/dev/disk1s1 465 381 81 83% 2764386 9223372036852011421 0% / "
	cat << EOT > $factoidPath
# Factoids for ${MODULE_NAME} running on '${podNameFull}' generated on ${now}
date: $(date -u "+${DEFAULT_TIME_FORMAT}")
machine: $podName	
podname: $podName	
lanip: $(SanitizeIP $(PodLANIP))
wanip: $(SanitizeIP $(PodWANIP))
os: $(sw_vers -productVersion)
uptime: $(/usr/bin/uptime 2>/dev/null)
loadavg: $(SystemLoad)
swapusage: $(sysctl vm.swapusage)
disk_size_gb: ${diskFree[1]}
disk_used_gb: ${diskFree[2]}
disk_used_pct: $(echo ${diskFree[4]} | sed -e "s/%//") 
version: ${CURRENT_SCRIPT_VERSION}
fullversion: $(Version full)
version_esound: $(${BIN_ESOUND} --version | head -1)
version_butt: $(${BIN_BUTT} -version)
version_audiodev: $(${BIN_AUDIODEV} -v)
version_magick: $(${BIN_MAGICK} --version | sed -e "s/Version: //" | head -1)
version_eventoverlay: $(${BIN_EVENTOVERLAY} --version)
version_gitlfs: $(${BIN_GIT_LFS} --version | head -1)
version_gnuplot: $(${BIN_GNUPLOT} --version | head -1)
version_id3v2: $(${BIN_ID3V2} --version | head -1)
version_radiomix: NA
version_slarchive: $(${BIN_SLARCHIVE} -V 2>&1)
version_sox: $(${BIN_SOX} --version | head -1)
version_sshfs: $sshfsVersion
version_soxoverlay: $(${BIN_SOXOVERLAY} --version)
audio_device_prefix: ${DEFAULT_AUDIO_DEVICE_PREFIX}

EOT
}



#############
# TestApp() -- Tests the named app (full path); returns 0 if no error, non-zero otherwise
# (To "test" an app, execute it with the --version flag and look at the error code.)
#############
function TestApp() {
	appPath=$1
	versionFlag="--version"
	[[ $(basename $appPath) = "slarchive" ]] && versionFlag="-V" # this one is special
	[[ $(basename $appPath) = "audiodev" ]] || [[ $(basename $appPath) = "buttj" ]] && versionFlag="-v" # this one is special
	printf $( ("${appPath}" "${versionFlag}" 2>&1) > /dev/null ; echo $?)
}

#############
# ValidateModule() -- Validate a module's config file and related bits and pieces
# Usage: ValidateModule [MODE]
#
# Output format depends on the optional MODE arg:
#	MODE			Action
#	[NONE]		prints a one-line summary
#	oneliner	prints a one-line summary
#	count		prints a count of the number of errors
#	verbose		prints out detailed info
#
#
# Each module must have its own version of two callback functions. Both are required:
#
#  ValidateChildrenCallback() -- adds a list of invalid child modules to the global 
#	variable $_OFFENSIVE_DESCENDANTS
#
#  ValidateParamCallback() -- validates a single parameter in the module's config
#	Arguments: PARAMETER VAL1 VAL2 VAL3...
# 	Where PARAMETER is the name of the parameter to be tested, and VAL1 etc. are the values found 
# 	in the config file. 
# 	Returns: a string containing an error message. If no error, returns the empty string.
# 	To make it a no-op:  make it return the empty string.
#
#############
function ValidateModule () {

	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	# several modes of verbosity, depending on the second arg:
	# 	"oneliner" (or no arg) ==> a one-line summary  ("Config file is OK" or "Config contains 3 errors", etc.)
	#	"count"		==> print just the number of errors 
	#	"verbose" 	==> detailed printout of each parameter in the config
	local outputMode
	case "$1" in
	"oneliner" | "")
		outputMode="oneliner"
		;; 
	"count" | "c" )
		outputMode="count"
		;; 
	"verbose" | "v")
		outputMode="verbose"
		;; 
	*)
		printf "Usage: ${callingFrom} oneliner | count | c | verbose | v\n"
		exit
		;;
	esac
	
	[[ $outputMode = "verbose" ]] && printf "${callingFrom}: Validating the ${MODULE_TYPE} module...\n"
	
	local s isare message
	local envErrors=()		# for keeping track of errors associated with the PSM environment
	local missingRequired=() # for keeping track of which required params are missing
	local otherErrors=()	# for keeping track of assorted error messages

	# check the PSM environment
	[[ $outputMode = "verbose" ]] && printf "Checking environment..."
	local envErrCount="$(CheckEnvironment count)"
	[[ $envErrCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	if [[ $envErrCount -gt 0 ]] ; then
		envErrors=( "${envErrors[@]}" "Shell environment is not set correctly.")
	fi
	if [[ $outputMode = "verbose" ]] ; then 
		if [[ $envErrCount -eq 0 ]] ; then
			printf " (OK)\n"
		else
			printf " *** ${envErrCount} error${s}.\n"
			# let CheckEnvironment print the full error message
			CheckEnvironment verbose 
			printf "\n"
		fi
	fi	

	# check for the requisite binaries
	[[ $outputMode = "verbose" ]] && printf "Looking for essential binaries... "
	if [[ "${#MODULE_REQUIRED_BINARIES[@]}" -eq 0 ]] ; then
		[[ $outputMode = "verbose" ]] && printf " none required (OK)\n"
	else
		[[ $outputMode = "verbose" ]] && printf "${#MODULE_REQUIRED_BINARIES[@]} required... "
		# Check that the requisite binary executables exist
		if [[ ! -z ${MODULE_REQUIRED_BINARIES+x} ]] ; then
			local binary
			local nMissing=0 nBroken=0
			for binary in "${MODULE_REQUIRED_BINARIES[@]}" ; do
				if [[ ! -f "${binary}" ]] ; then
					otherErrors=( "${otherErrors[@]}" "Essential executable '$(basename ${binary})' is missing.")
					(( nMissing++ ))
				else
					if [[ $(TestApp "${binary}") -ne 0 ]] ; then
						otherErrors=( "${otherErrors[@]}" "Essential executable '$(basename ${binary})' is broken.")
						(( nBroken++ ))
					fi
				fi
			done
		fi
		if [[ $outputMode = "verbose" ]] ; then
			[[ $nMissing -gt 0 ]] && printf "$nMissing missing "
			[[ $nBroken -gt 0 ]] && printf "$nBroken broken "
			[[ $nMissing -eq 0 ]] && [[ $nBroken -eq 0 ]] && printf "(OK)\n" || printf "***\n"
		fi
	fi
		
	# verify that git-lfs is present (required for updates -- only need do this for the pod module)
	if [[ $MODULE_NAME = "pod" ]] ; then
		[[ $outputMode = "verbose" ]] && printf "Looking for git-lfs..."
		local gitlfs=$(which ${BIN_GIT_LFS})
		if [[ $gitlfs = "" ]] ; then 
			[[ $outputMode = "verbose" ]] && printf "*** MISSING! Please install git-lfs from https://git-lfs.github.com\n"
			otherErrors=( "${otherErrors[@]}" "Missing app: git-lfs. Please install it from https://git-lfs.github.com")
		else
			[[ $outputMode = "verbose" ]] && printf " ${gitlfs} (OK)\n"
		fi
	fi
	
	[[ $outputMode = "verbose" ]] && printf "Reading config file... "

	# Check the config file
	if [[ ! -e "${MODULE_CONFIG_FILE}" ]] ; then
		message="*** Config file is missing. Can't validate"
		(( envErrCount ++))
		if [[ $outputMode = "count" ]] ; then
			printf $envErrCount
		else
			printf "${message}.\n"
		fi
		exit
	fi 
	
	SourceRawConfig

	[[ $outputMode = "verbose" ]] && printf "(OK)\n"
	

	# Check CONFIG_REQUIRED_ARRAYS
	[[ $outputMode = "verbose" ]] && printf "Checking required config arrays..."
	if [[ "${#CONFIG_REQUIRED_ARRAYS[@]}" -eq 0 ]] ; then
		[[ $outputMode = "verbose" ]] && printf " none required (OK)\n"
	else
		[[ $outputMode = "verbose" ]] && printf "${#CONFIG_REQUIRED_ARRAYS[@]} required\n"
	
		# We have an array filled with names of arrays. This is a case of indirect referencing, which is tricky in bash.
		# For help, see https://stackoverflow.com/a/11180835/5011245 and https://stackoverflow.com/a/25880676/5011245
		# Example: CONFIG_REQUIRED_ARRAYS=(FRUITS VEGGIES)
		local arrayName status tmp
		for arrayName in "${CONFIG_REQUIRED_ARRAYS[@]}" ; do
			tmp="${arrayName}[@]" # e.g., the string "FRUITS[@]"
			local array=( "${!tmp}" )	# get the values of $FRUITS into an array (e.g., array=(banana kumquat jackfruit) )
			if [[ ${#array[@]} -gt 0 ]] ; then	# if there's at least one value in this array, then this variable is present.
				# Now let's examine it more closely by calling the module's own param validator
				local reply=$(ValidateParamCallback $arrayName "${!tmp}")
				if [[ "${reply}" = "" ]] ; then  # if no error in the variable's value
					status="(OK)"	
				else 
					otherErrors=( "${otherErrors[@]}" "${arrayName}: ${reply}.")
					status="${reply}"			
				fi
			else
				missingRequired=( "${missingRequired[@]}" "${arrayName}" )
				status="*** Missing ***"
			fi
			[[ $outputMode = "verbose" ]] && printf "%${DEFAULT_FORMAT_SPACING}s : [%d] %s  %s\n" "${arrayName}" ${#array[@]} "${array[*]}" "$status"
		done
	fi



	# Check CONFIG_REQUIRED_VARIABLES
	[[ $outputMode = "verbose" ]] && printf "Checking required config variables... "
	if [[ "${#CONFIG_REQUIRED_VARIABLES[@]}" -eq 0 ]] ; then
		[[ $outputMode = "verbose" ]] && printf "(OK)\n"
	else
		[[ $outputMode = "verbose" ]] && printf "${#CONFIG_REQUIRED_VARIABLES[@]} required...\n"
		local variableName value
		for variableName in "${CONFIG_REQUIRED_VARIABLES[@]}" ; do
			value=$(eval echo \$$variableName)
			if [[ -z ${value+x} ]] || [[ ${value} = "" ]] ; then  # if this required variable is missing
				status="*** Missing ***"
				missingRequired=( "${missingRequired[@]}" "${variableName}" )
			else  	# required variable is present. 
					# Now let's examine it more closely by calling the module's own param validator
				local reply=$(ValidateParamCallback $variableName $value)
				if [[ "${reply}" = "" ]] ; then  # if no error in the variable's value
					status="(OK)"	
				else 
					otherErrors=( "${otherErrors[@]}" "${variableName}: ${reply}.")
					status="${reply}"
				fi
			fi
		
			[[ $outputMode = "verbose" ]] && printf "%${DEFAULT_FORMAT_SPACING}s : %s %s\n" "${variableName}" "${value}" "$status"
		done

		if [[ $outputMode = "verbose" ]] ; then 
			if [[ "${#missingRequired[@]}" -eq 0 ]] ; then
				printf "All required config parameters are present (OK)"
			fi
			printf "\n"
		fi 
	fi
		

	# Do the CONFIG_OPTIONAL_ARRAYS
	[[ $outputMode = "verbose" ]] && printf "Checking optional config arrays... "
	if [[ "${#CONFIG_OPTIONAL_ARRAYS[@]}" -eq 0 ]] ; then
		[[ $outputMode = "verbose" ]] && printf "none expected (OK)\n"
	else
		[[ $outputMode = "verbose" ]] && printf "\n"
		for arrayName in "${CONFIG_OPTIONAL_ARRAYS[@]}" ; do
			tmp="${arrayName}[@]" # e.g., the string "FRUITS[@]"
			local array=( "${!tmp}" )	# get the values of $FRUITS into an array (e.g., array=(banana kumquat jackfruit) )
			if [[ ${#array[@]} -gt 0 ]] ; then	# if there's at least one value in this array, then this variable is present.
				# Now let's examine it more closely by calling the module's own param validator
				local reply=$(ValidateParamCallback $arrayName "${!tmp}")
				if [[ "${reply}" = "" ]] ; then  # if no error in the variable's value
					status="(OK)"	
				else 
					otherErrors=( "${otherErrors[@]}" "${arrayName}: ${reply}.")
					status="${reply}"			
				fi
			fi
			[[ $outputMode = "verbose" ]] && printf "%${DEFAULT_FORMAT_SPACING}s : [%d] %s  %s\n" "${arrayName}" ${#array[@]} "${array[*]}" "$status"
		done
	fi
	

	# Check the CONFIG_OPTIONAL_VARIABLES
	[[ $outputMode = "verbose" ]] && printf "Checking optional config variables... "
	if [[ "${#CONFIG_OPTIONAL_VARIABLES[@]}" -eq 0 ]] ; then
		[[ $outputMode = "verbose" ]] && printf " none expected (OK)\n"
	else
		[[ $outputMode = "verbose" ]] && printf "\n"		
		if [[ ${#CONFIG_OPTIONAL_VARIABLES[@]} -gt 0 ]] ; then 
			for variableName in "${CONFIG_OPTIONAL_VARIABLES[@]}" ; do
				value=$(eval echo \$$variableName)
				if [[ -z ${value+x} ]] || [[ ${value} = "" ]] ; then  # if this optional variable is missing
					value="[not set]"
					status="(OK)"  # it's ok for an optional variable to be not set
				else  	# optional variable is set. 
						# Now let's examine it more closely by calling the module's own param validator
					local reply=$(ValidateParamCallback $variableName $value)
					if [[ "${reply}" = "" ]] ; then  # if no error in the variable's value
						status="(OK)"	
					else 
						otherErrors=( "${otherErrors[@]}" "${variableName}: ${reply}")
						status="${reply}"
					fi
				fi
		
				[[ $outputMode = "verbose" ]] && printf "%${DEFAULT_FORMAT_SPACING}s : %s %s\n" "${variableName}" "${value}" "$status"
			done
		fi
	fi

	# Now execute the child-validator that's defined in the module-functions file
	# (Each module has its own way of validating its descendants.)
	_OFFENSIVE_DESCENDANTS=() # clear the global variable
		
	# if ValidateChildrenCallback() is a function (i.e., it's been defined for the current module), then execute it
	if type -t ValidateChildrenCallback | grep -q '^function$' 2>/dev/null; then  # https://stackoverflow.com/a/1007605
		ValidateChildrenCallback  # This fills the global variable _OFFENSIVE_DESCENDANTS with a list of names of offending descendants
	fi

	# Now construct a message regarding the descendants.
	# In "count" mode it's simply the number of children with errors.
	# In any other mode (default) it's a line saying how many errors
	# there were, followed by a list of names of offenders.
	# This is followed by instructions on how to get more info.
	# The first word of the message string is ALWAYS the number of erroneous children.
	local nChildErrors=${#_OFFENSIVE_DESCENDANTS[@]}
	local childErrMsg s isare have
	case "$outputMode" in
	"count") # in "count" mode, the message is just the number of offenders
		childErrMsg="$nChildErrors"
		;; 
	*)  # if any other mode, add more info to the message
		if [[ $nChildErrors -eq 0 ]] ; then
			childErrMsg="0 children have errors"
		else		
			[[ $nChildErrors -eq 1 ]] && have="dependent module contains" || have="dependent modules contain" # pluralizer
			childErrMsg="$nChildErrors $have errors:"
			for child in "${_OFFENSIVE_DESCENDANTS[@]}" ; do
				childErrMsg="$childErrMsg $child"
			done
			[[ $nChildErrors -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
			childErrMsg="$childErrMsg\nFor details, try validating the offending module${s}, like so:\n"
			for child in "${_OFFENSIVE_DESCENDANTS[@]}" ; do
				childErrMsg="$childErrMsg      ${child}/val verbose\n"
			done
		fi
		;;
	esac


	# Look for any deprecated parameters
	local warningCount=0
	local deprecateds=()	
	if [[ ! -z ${CONFIG_DEPRECATED_VARIABLES} ]] ; then # if any deprecated variables were listed in *-functions.sh
		[[ $outputMode = "verbose" ]] && printf "Looking for deprecated config parameters... "
		for variableName in "${CONFIG_DEPRECATED_VARIABLES[@]}" ; do
			value=$(eval echo \$$variableName)
			if [[ ! -z ${value+x} ]] && [[ $value != "" ]] ; then  # if this deprecated variable was found
				deprecateds=( "${deprecateds[@]}" "$variableName") # save its name
				(( warningCount++ ))
				[[ $outputMode = "verbose" ]] && printf "\n%${DEFAULT_FORMAT_SPACING}s : %s\n" "${variableName}" " *** Warning: this parameter is now deprecated. Please remove from config."
			fi
		done
		[[ $outputMode = "verbose" ]] && [[ ${#deprecateds[@]} -eq 0 ]] && printf "none found (OK)\n"
	fi


	# Handle any interacting params and add any warnings to the tally
	[[ $outputMode = "verbose" ]] && printf "Sanitizing interactive config parameters... "
	PrepareParams $outputMode
	[[ $outputMode = "verbose" ]] && printf "(OK)\n"

	# Handle any other module-specific errors (only if the special validator function exists)
	if [[ $(type -t SpecialValidate) = "function" ]] ; then
		# get the lines of printed errors, and convert them to an array -- one line per array element
		# https://stackoverflow.com/a/8768435/5011245
		IFS=$'\n'; specialErrors=($( SpecialValidate )) 
	fi	
	

	# Prepare for a final printout of the errors
	local configErrCount=0 totalErrCount=0
	let "warningCount = warningCount + $?"
	let "configErrCount = configErrCount + ${#missingRequired[@]} + ${#otherErrors[@]} + ${#envErrors[@]}"
	let "totalErrCount = configErrCount + $nChildErrors + ${#specialErrors[@]}"

	case $outputMode in
	"count")
		printf "${totalErrCount}\n"
		;; 
	"oneliner")
		[[ $totalErrCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		local message="Module contains ${totalErrCount} error${s}"
		[[ $warningCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		message="${message}, ${warningCount} warning${s}."
		[[ $totalErrCount -gt 0 ]] && message="*** ${message} For details, try '${MODULE_NAME}/val verbose'"
		printf "${message}\n"
		;; 
	"verbose" )
		printf "\nSUMMARY:\n"
		if [[ $totalErrCount -gt 0 ]] ; then
			[[ $totalErrCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
			local message="Module contains ${totalErrCount} error${s}:"
			[[ $totalErrCount -gt 0 ]] && message="*** ${message}"
			printf "${message}\n"

			if [[ $configErrCount -gt 0 ]] ; then
				[[ $configErrCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
				printf "*** There ${isare} ${configErrCount} error${s} in the ${MODULE_TYPE} config:\n"
				if [[ ${#envErrors[@]} -gt 0 ]] ; then
					local e
					for e in "${envErrors[@]}" ; do
							printf "%${DEFAULT_FORMAT_SPACING}s\n" "*** ${e}" | sed -e "s/^/    /"
					done
				fi
			fi
			if [[ ${#missingRequired[@]} -gt 0 ]] ; then
				[[ ${#missingRequired[@]} -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
				printf "*** ${#missingRequired[@]} config parameter${s} ${isare} required but not defined:\n"
				for arrayName in "${missingRequired[@]}" ; do
						printf "%${DEFAULT_FORMAT_SPACING}s\n" "${arrayName}" | sed -e "s/^/    /"
				done
			fi
			if [[ ${#deprecateds[@]} -gt 0 ]] ; then
				local d
				for d in "${deprecateds[@]}" ; do
						printf "%${DEFAULT_FORMAT_SPACING}s\n" "*** Found deprecated parameter '${d}'. Please remove from config." | sed -e "s/^/    /"
				done
			fi

			if [[ ${#otherErrors[@]} -gt 0 ]] ; then
				local e
				for e in "${otherErrors[@]}" ; do
						printf "%${DEFAULT_FORMAT_SPACING}s\n" "*** ${e}" | sed -e "s/^/    /"
				done
			fi
			
			if [[ ${#specialErrors[@]} -gt 0 ]] ; then
				local e
				for e in "${specialErrors[@]}" ; do
						printf "%${DEFAULT_FORMAT_SPACING}s\n" "*** ${e}" | sed -e "s/^/    /"
				done
			fi

			if [[ $nChildErrors -gt 0 ]] ; then
				printf "*** ${childErrMsg}\n" | sed -e "s/^/    /"
			fi			
			
		else 
			printf "No config errors found in module '${MODULE_NAME}' (OK)\n"	
		fi
		
		if [[ $warningCount -gt 0 ]] ; then
			[[ $warningCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
			printf "%d config warning${s} in module '${MODULE_NAME}' (OK)\n" $warningCount
		else
			printf "No config warnings in module '${MODULE_NAME}' (OK)\n"
		fi
		
		;; 
	esac
}


#############
# AttachModule() -- re-attach a virtual screen to the Terminal
# Usage:  AttachModule 
#############
function AttachModule ()  {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	ConfigExistsOrDie $callingFrom
	
	# look up the screen socket name
	socket="$(screen -list | grep "\.${MODULE_TYPE}" | awk '{print $1}')"
	if [[ "$socket" = "" ]] ; then
		printf "${callingFrom}: No virtual terminal found.\n"
		exit
	fi
	
	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Attaching screen (type \"CTRL-a CTRL-d\" to exit)..."
	sleep 2 # pause a moment to give the user a chance to read the previous message...

	screen -r "$socket" # reattach the screen

	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Done."
}

#
# UpdateModule () -- installs or refreshes a module
#	with optional arg "nogit" arg, suppress a "git pull..." command
#
function UpdateModule () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	# set some flags according to the arguments to UpdateModule ()

	# By default, UpdateModule pulls the latest codebase from the git repo.
	# With the "nogit" argument, git will NOT be invoked.
	if ArrayContainsElement nogit $@ ; then
		local doGit=false
	else 
		local doGit=true
	fi
		
	# By default, UpdateModule will NOT try to create a module that doesn't yet exist
	# With the "create" arg, UpdateModule WILL create a new module from scratch.
	# The "create" arg also prevents trying to update an existing module.
	if ArrayContainsElement create $@ ; then
		local doCreate=true
	else
		local doCreate=false
	fi


	# Can't do git without git-lfs also being installed
	if $doGit ; then
		local gitlfs=$(which ${BIN_GIT_LFS})
		if [[ $gitlfs  = "" ]] ; then 
			printf "${callingFrom}: *** git-lfs is MISSING! Can't update.\n"
			printf "${callingFrom}: *** Before continuing, please install git-lfs from https://git-lfs.github.com\n"
			exit
		else
			printf "${callingFrom}: Found git-lfs at ${gitlfs} (OK)\n"
		fi
	fi

	if ! ElementIn "$MODULE_TYPE" "${DEFAULT_RESERVED_MODULE_TYPES[@]}" ; then 	# skip over "protected" files
		printf "*** ${callingFrom}: Unknown module type '${MODULE_TYPE}'.\n"
		exit
	fi
	
	# are the environment variables correct?
	local envErrCount="$(CheckEnvironment count)"
	if [[ $envErrCount -ne 0 ]] ; then
		printf "*** ${callingFrom}: "
		CheckEnvironment verbose
		exit
	fi
	# (There's no need to print out a message if env is OK)

	# if the functions for this module aren't available, then abort
	if [[ ! -f "${SCRIPTS_DIR}/${MODULE_TYPE}-functions.sh" ]] ; then
		printf "*** ${callingFrom}: No functions defined for module type '${MODULE_TYPE}'. Can't continue.\n"
		exit
	fi
		
	# if the module dir wasn't there (e.g., in a fresh install), then create it
	if [[ ! -e "${MODULE_DIR}" ]] ; then # if dir doesn't exist
		if $doCreate ; then
			mkdir "${MODULE_DIR}"
		else
			printf "*** ${callingFrom}: Module '${MODULE_NAME}' doesn't exist. Can't update.\n"
			exit
		fi
	else	# module directory was already present
		if [[ -f "${MODULE_CONFIG_FILE}" ]] ; then # if there already is a config file
			if $doCreate ; then # in "create" mode, don't touch an existing module 
				printf "*** ${callingFrom}: Module '${MODULE_NAME}' already installed. Can't create it. Try '${MODULE_NAME}/update' instead.\n"
				exit
			fi
		else  # config is missing, which forces re-creation of the module
			: # noop. No need to print a message here. It happens below. 
			# printf "${thisFunction}: Config file is missing. Installing a new one.\n"
		fi 
	fi
	# set the module-specific variables and get the module-specific functions
	source "${SCRIPTS_DIR}/${MODULE_TYPE}-functions.sh"
	# if the module is currently running, then STOP!
	if ModuleIsRunning ${MODULE_NAME}; then
		printf "*** ${callingFrom}: Module '${MODULE_NAME}' is running. Can't update. Try '${MODULE_NAME}/stop' and try again.\n"
		exit
	fi
	
	# If the config file is missing or is of zero size, then this module is either a fresh install or horribly corrupt,
	# and we need to install a fresh config file before proceeding.
	# (NB: LogMe is smart enough to avoid writing to a log file that hasn't yet been created)
	if [[ ! -f "${MODULE_CONFIG_FILE}" ]] ; then
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: installing clean ${MODULE_TYPE} config file"
		FilterTemplate "${MODULE_TYPE}.conf"
	else 
		local configSize="$(wc -c < ${MODULE_CONFIG_FILE} | sed -e 's/ //g')" # the size, in bytes of the config file
		if [[ $configSize = "0" ]] ; then  # if this doesn't look like a functioning config file
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: installing clean ${MODULE_TYPE} config file"
			FilterTemplate "${MODULE_TYPE}.conf"
		fi
	fi

	
	# pull from the git repo, if that's what was requested
	if [[ "${doGit}" = true ]] ; then
		# update the scripts from the git repo
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Updating local codebase from the remote git repository..."
		export PATH=$BIN_DIR:$USR_LOCAL_BIN:$PATH # add BIN_DIR to the search path (for git-lfs)
		# FIXME v171: ??? Is BIN_DIR in the previous line necessary ??? because git-lfs isn't in BIN_DIR ???
		# FIXME v171: So I'm adding USR_LOCAL_BIN to the path, since that's where git-lfs resides.
		pwd=$(pwd); cd "${PSM_DIR}" # (we could do pushd/popd, but those generate un-redirectable output)
		printf "@@@@ start git\n"
		git pull origin master 2>&1 | sed -e "s/^/@@@@   /"
		printf "@@@@ end git\n"
		cd $pwd # (see note above re: pushd/popd
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Local script codebase updated. (OK)"
	fi
	
	RecreateFiles # rebuild the module's files, if necessary
	
}



#############
# 	RecreateFiles() -- recreates a module's directory structure, bash scripts, and other files.
#   Does not touch the config file.
# 	This function expects the following arrays to have been defined:
#		MODULE_COMMANDS	The names of the shell scripts (commands) that need to be created
#############
function RecreateFiles () {

	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	if [[ -z ${MODULE_COMMANDS+x} ]] ; then 
		printf "*** ${thisFunction}: Fatal error. Module '${MODULE_TYPE}' is missing \$MODULE_COMMANDS.\n"
		exit
	fi
		
	printf "${callingFrom}: Installing the essential module directories: "

	# install the essential subdirectories
	local dir
	for dir in "${MODULE_ESSENTIAL_DIRS[@]}" ; do
		local d="${MODULE_DIR}/${dir}"
		if [[ ! -d "${d}" ]] ; then	
			printf "${dir} "
			mkdir $d				
		else 
			printf "[${dir}] "
		fi
	done
	printf "(OK)\n"

#	RotateLogFiles "${MODULE_LOGFILE}"

	# create the module-specific directories if they don't already exist
	if [[ ${#MODULE_SPECIAL_DIRS[@]} -gt 0 ]] ; then
		printf "${callingFrom}: Installing the ${MODULE_TYPE}-specific directories: "
		for dir in "${MODULE_SPECIAL_DIRS[@]}" ; do
			local d="${MODULE_DIR}/${dir}"
			if [[ ! -d "${d}" ]] ; then
				printf "${dir} "
				mkdir "${d}"
			else 
				printf "[${dir}] "
			fi
		done
		printf "(OK)\n"
	fi
		
	printf "${callingFrom}: Installing the latest ${MODULE_TYPE} commands: "
	# install the scripts as edited versions of their templates
	if [[ ${#MODULE_COMMANDS[@]} -gt 0 ]] ; then
		local cmd
		for cmd in "${MODULE_COMMANDS[@]}" ; do
			printf "${cmd} "
			InstallCommand $cmd
		done
		printf "(OK)\n"
	fi
		
	# install non-executable files
	if [[ ${#MODULE_SPECIAL_FILES[@]} -gt 0 ]] ; then
		printf "${callingFrom}: Installing the latest ${MODULE_TYPE} special files: "
		local f
		for f in "${MODULE_SPECIAL_FILES[@]}" ; do 
			if [[ -e "${MODULE_DIR}/${f}" ]] && ElementIn "${f}" "${MODULE_USER_PREFERENCES[@]}" ; then 	# skip over user prefs
				printf "[${f}] "
			else
				printf "${f} "
				FilterTemplate "${f}"
				if [[ "${f##*.}" = "sh" ]] ; then # if the file extension suggests that it's a shell script
					chmod +x "${MODULE_DIR}/${f}"	# then make it executable
				fi
			fi
		done
		printf "(OK)\n"
	fi

	# always install a pristine demo config
	printf "${callingFrom}: Installing clean demo config: "
	local demoConfig="demo-${MODULE_TYPE}.conf"
	FilterTemplate "${MODULE_TYPE}.conf" "${demoConfig}"
	printf "(OK)\n"
		
	# install program asset files, if any
	if [[ ${#MODULE_ASSETS[@]} -gt 0 ]] ; then
		printf "${callingFrom}: Installing $MODULE_TYPE assets: "
		local assetsDir="${MODULE_DIR}/assets"
		for a in "${MODULE_ASSETS[@]}" ; do
			printf "${a} "
			/bin/cp "${ASSETS_DIR}/${a}" "${assetsDir}/."
		done
		printf "(OK)\n"
	fi

	# if it's a program, then install a vanilla eventlog file
	local eventlog="${DEFAULT_PROGRAM_EVENTLOG}"
	if [[ "${MODULE_TYPE}" = "program" ]] ; then
		printf "${callingFrom}: Looking for ${eventlog}: "
		if [[ -e "${MODULE_DIR}/${eventlog}" ]] ; then
			printf "found "
		else
			printf "installing..."
			FilterTemplate "${eventlog}"
		fi
		printf "(OK)\n"
	fi
	

}


# Source the contents of the config file and tidy up the params.
# If no config file is present, print error and exit.
# First arg is the name of the calling program
function SourceConfig() {
	SourceRawConfig $@
	PrepareParams  || : # " xxx || : " means "if xxx returns a non-zero errval, do nothing)"; this prevents an exit here, due to a prior "set -e" in program-eLoop.sh
}

# Source the contents of the config file without tidying up the params.
# If no config file is present, print error and exit.
# First arg is the name of the calling program
function SourceRawConfig() {
	ConfigExistsOrDie $1
	UnsetConfigVariables # wipe the slate clean
	source "${MODULE_CONFIG_FILE}" # source the config file
	# Test for any unclosed quotes, stray brackets, etc. 
	if [[ $? -ne 0 ]] ; then
		printf "*** Oops! There are some serious problems with your config file!\n"
		exit
	fi	
}

# If no config file is present, print error and exit.
# First arg is the name of the calling program
function ConfigExistsOrDie() {
	# Is the config file present?
	if [[ ! -e "${MODULE_CONFIG_FILE}" ]] ; then
		printf "${1}: *** Missing ${MODULE_NAME} config file. Nothing to do.\n"
		exit 1
	fi
}


function GetWebhostInboundDir() {
	source "${PSM_PROJECT}/pod/pod.conf"
	[[ ! -n "${POD_WEBHOST_ROOT_DIR}" ]] && POD_WEBHOST_ROOT_DIR="${DEFAULT_POD_WEBHOST_ROOT_DIR}" # CAUTION: this line also appears in pod-functions.sh
	inboundDir="${POD_WEBHOST_ROOT_DIR}/${DEFAULT_WEBHOST_INBOUND_DIR}"
	printf "${inboundDir}"
}
function GetWebhostArchiveDir() {
	source "${PSM_PROJECT}/pod/pod.conf"
	[[ ! -n "${POD_WEBHOST_ROOT_DIR}" ]] && POD_WEBHOST_ROOT_DIR="${DEFAULT_POD_WEBHOST_ROOT_DIR}" # CAUTION: this line also appears in pod-functions.sh
	archiveDir="${POD_WEBHOST_ROOT_DIR}/${DEFAULT_WEBHOST_ARCHIVE_DIR}"
	printf "${archiveDir}"
}
function GetWebhostPodsDir() {
	source "${PSM_PROJECT}/pod/pod.conf"
	[[ ! -n "${POD_WEBHOST_ROOT_DIR}" ]] && POD_WEBHOST_ROOT_DIR="${DEFAULT_POD_WEBHOST_ROOT_DIR}" # CAUTION: this line also appears in pod-functions.sh
	podsDir="${POD_WEBHOST_ROOT_DIR}/${DEFAULT_WEBHOST_PODS_DIR}"
	printf "${podsDir}"
}
function GetWebhostDispatchersDir() {
	source "${PSM_PROJECT}/pod/pod.conf"
	[[ ! -n "${POD_WEBHOST_ROOT_DIR}" ]] && POD_WEBHOST_ROOT_DIR="${DEFAULT_POD_WEBHOST_ROOT_DIR}" # CAUTION: this line also appears in pod-functions.sh
	podsDir="${POD_WEBHOST_ROOT_DIR}/${DEFAULT_WEBHOST_DISPATCHERS_DIR}"
	printf "${podsDir}"
}


#
# ArrayContainsElement () -- returns true (0) if array contains an element; false (1) otherwise
# Usage:
# 	haystack=(foo bar octo)
# 	needle="foo"
#  	if ArrayContainsElement "${needle}" "${haystack[@]}" ; then
# 		echo "haystack contains ${needle}"
# 	else
# 		echo "haystack does not contain ${needle}"
# 	fi
function ArrayContainsElement() {
	needle="$1"
	shift
	if [[ $(printf "%s\n" "$@" | grep  -x "$needle" | wc -l) -eq 1 ]] ; then
		return 0 	# contains element (0=success)
	else
		return 1	# does not contain element (1=failure)
	fi
}


#################################################
# DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
# DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
# DeleteAllExcept -- a handy, yet unacceptably dangerous, function that deletes all files in the 
# MODULE_DIR directory EXCEPT those named in the argument list.
# A tiny safeguard: if the first argument is NOT equal to the $key, then the function simply lists 
# the files that would have been deleted if the key were correct.
# DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
# DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER DANGER
function DeleteAllExcept () {
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	local key="8675309"
	local firstArg=$1 ; shift
	
	# Construct a "find" string containing a list of files to skip over.
	# The string will look something like: "-not -name Foo.txt -not -name Bar.doc ..."
	local fileName ignoreString=""
	for fileName in "$@"; do
		ignoreString=${ignoreString}"-not -name ${fileName} "
	done
	ignoreString=${ignoreString}"-not -name ${MODULE_TYPE}.conf "  # NEVER delete the module's config file
	ignoreString=${ignoreString}"-not -name *.txt "  # NEVER delete a txt file
	
	
	if [[ ! -z ${DEBUG_TRACE+x} ]] ; then 
		local traceString="${thisFunction}: "
	fi
	# if the wrong key was supplied, then list the files that WOULD have been deleted, then exit.
	if [[ "${firstArg}" != "${key}" ]] ; then
		ignoreString=${ignoreString}"-not -name ${firstArg}"  # since the first arg wasn't they key, it may be a file, so ignore that file, too
		printf "${traceString}DeleteAllExcept: Incorrect key.\n"
		printf "${traceString}Here's what would have been deleted if the key were correct:\n"
		find "${MODULE_DIR}" -depth 1 -type f $ignoreString -print
		printf "${traceString}To actually delete those files, the first argument must be the correct key."
	else
		local deleteList=($(find "${MODULE_DIR}" -depth 1 -type f $ignoreString -print))
		local nDelete=${#deleteList[@]}
		if [[ $nDelete -lt 1 ]] ; then
			printf "${traceString}No extraneous module files to delete."
		else 
			[[ $nDelete -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
			printf "${traceString}Deleting $nDelete extraneous module file${s}: "
			for f in "${deleteList[@]}" ; do
				printf "$(basename ${f}) "
			done
			/bin/rm ${deleteList[*]}
		fi
	fi
	printf " (OK)\n"
}
#################################################


#############
# TracePath() -- print a (sorta) stack trace
# Example output: x1/update::Program_update()::UpdateModule()::RecreateFiles() 
function TracePath() {

	# get the module name from the directory in which the command was invoked
	# (We can't rely on MODULE_NAME, since it may be reset during pod/newserver command)
	local moduleName=$MODULE_NAME #(basename $(cd $(dirname "${BASH_SOURCE[${#BASH_SOURCE[@]}-1]}") && pwd) )

	if [[ -z ${DEBUG_TRACE+x} ]] ; then # if DEBUG_TRACE not set
		printf "${moduleName}/$(basename ${BASH_SOURCE[${#BASH_SOURCE[@]}-1]})" # e.g., "pod/update"
		return
	else 	
		local funcpath=""
		# get array elements of FUNCNAME in reverse order
		# NB: idx>=1 instead of idx>=0 . This ensures that we don't include TracePath itself in the stack
		for (( idx=${#FUNCNAME[@]}-2 ; idx>=1 ; idx-- )) ; do
			funcpath="${funcpath}::$(basename ${FUNCNAME[$idx]})()"
		done
	
		local callingFrom="${moduleName}/$(basename ${BASH_SOURCE[${#BASH_SOURCE[@]}-1]})"  # e.g., "pod/update"
		funcpath="${callingFrom}::${funcpath#::}"  # bash parameter expansion: capture everything after the first "::"
		printf $funcpath
	fi

}

#######
# ElementIn() -- returns 0 (true) if an element is in array, 1 (false) otherwise
# https://stackoverflow.com/a/8574392
function ElementIn () {
	local e match="$1"
	shift
	for e ; do # nifty bash shorthand for 'for e in "$@"'
		[[ "$e" = "$match" ]] && return 0
	done
	return 1
}

# Version () -- print the (pseudo-)version number of this instance of psm (in the local repository)
# By default, print the major version number
# With the optional second arg, it prints the full version (including time stamp of last git commit)
# see https://stackoverflow.com/a/25564443/5011245 and https://git-scm.com/docs/git-log
function Version () {
	if [[ $# -eq  1 ]] && [[ $1 = "full" ]] ; then
		local commitTime=$(cd $PSM_DIR ; date -u -j -f "%a %b %d %T %Y %z" "`git log -1 --format=%cd`" "+%Y%m%d.%H%M%S")
		local v="${CURRENT_SCRIPT_VERSION}.${commitTime}"
		printf "${v}\n"
	else
		printf "${CURRENT_SCRIPT_VERSION}\n"
	fi
}

# MasterVersion () -- print the (pseudo-)version number of the latest commit to the remote master git repo
# By default, print the major version number
# With the optional second arg, it prints the full version (including time stamp of last git commit)
# see https://stackoverflow.com/a/25564443/5011245 and https://git-scm.com/docs/git-log
function MasterVersion () {
	pushd $PSM_DIR > /dev/null
	git remote update >/dev/null # update tracking of remote branches (but don't merge any changes -- that's what 'git pull' does)
	if [[ $# -eq  1 ]] && [[ $1 = "full" ]] ; then
		local commitTime=$(date -u -j -f "%a %b %d %T %Y %z" "`git log -1 --format=%cd origin/master`" "+%Y%m%d.%H%M%S")
		local v="${CURRENT_SCRIPT_VERSION}.${commitTime}"
		printf "${v}\n"
	else
		printf "${CURRENT_SCRIPT_VERSION}\n"
	fi
	popd > /dev/null
}

# Given two version numbers (A, B), prints an integer of their relative age:
#	0	if they're equal
#	1	if A is newer than B
#	-1	if A is older than B
# Version numbers contain three dot-separated fields of the form NNN.YYYYMMDD.HHMMSS,
# where NNN is the Major version number and the other two fields correspond to the git commit time.
# E.g. "151.20190328.192143"
# NB -- the HHMMSS digits may contain leading zeroes (hour zero). Bash arithmetic may interpret those as octal 
# digits; mayhem and weirdness ensues. To strip off those leading zeroes, convert them to base 10 before doing any arithmetic 
function CompareVersions () {
	[[ $# != 2 ]] && printf "***CompareVersions requires exactly two args (got $#).\n" && exit
	local verA=(${1//\./ }) # split the string (multi-part version number) on the dots into an array
	local verB=(${2//\./ }) # split the string (multi-part version number) on the dots into an array
	local diffMajor=$(( ${verA[0]} - ${verB[0]}))
	local diffYYYYMMDD=$(( ${verA[1]} - ${verB[1]}))
	local diffHHMMSS=$(( $((10#${verA[2]})) - $((10#${verB[2]}))  )) # 

	# compare each part of the version numbers	
	[[ $diffMajor -gt 0 ]] || [[ $diffYYYYMMDD -gt 0 ]] || [[ $diffHHMMSS -gt 0 ]] && printf "%d\n" 1 && return # A > B
	[[ $diffMajor -lt 0 ]] || [[ $diffYYYYMMDD -lt 0 ]] || [[ $diffHHMMSS -lt 0 ]] && printf "%d\n" -1 && return # A < B
	printf "%d\n" 0 # they're equal
}

#######
# ModuleIsPresent -- returns 0 (true) if the named module (i.e., directory) is present, 1 (false) otherwise
# Caveat: a module is regarded as "present" simply if a directory by that name exists.
function ModuleIsPresent() {
	if [[ $# -ne 1 ]] ; then # if wrong arg count
		return 1	# return false
	fi
	local module="$1"
	[[ -d "${PSM_PROJECT}/$module" ]] && return 0
	return 1
}

#######
# ModuleType -- prints the name of the type of the given module
function ModuleType() {
	local mName="$1"
	if [[ $# -ne 1 ]] ; then # if wrong arg count
		printf "NA"
	fi
	# if the name of the module is not one of the DEFAULT_RESERVED_MODULE_NAMES, then it must be a program module
	local mType="program"
	if ArrayContainsElement "${mName}" ${DEFAULT_RESERVED_MODULE_NAMES[@]} ; then
		mType=$mName
	fi
	printf "$mType"
}



#############
# ModuleState() - prints a string with the "state" of the module:
#	MISSING		not installed 
#	INACTIVE	installed && not activated in pod.conf
#	UP			installed && activated && running a process
#	DOWN		installed && activated && not running a process
#	IDLE		installed && activated && awaiting a command  
#	UNKNOWN
#############
function ModuleState () {
	if [[ $# -ne 1 ]] ; then  # if no arg...
		printf "${DEFAULT_MODULE_STATE_UNKNOWN}" 
		return
	fi

	local module=$1		
	
	# is the module directory present?
	local moduleDir="${PSM_PROJECT}/${module}"
	if [[ ! -d $moduleDir ]] ; then  
		printf "${DEFAULT_MODULE_STATE_MISSING}"
		return
	fi

	# is the module activated in the pod config file?
	source "${PSM_PROJECT}/pod/pod.conf"
	if ! ArrayContainsElement "${module}" ${MODULES[@]} ; then
		printf "${DEFAULT_MODULE_STATE_INACTIVE}"
		return
	fi
		
	# the module dir is present, and it's activated in the config, so proceed...
	case "$module" in		
	dispatcher | radio | pod | slink | program )
		ModuleIsRunning $module && printf "${DEFAULT_MODULE_STATE_UP}" || printf "${DEFAULT_MODULE_STATE_DOWN}"
		;;
	rtmon )
		printf "${DEFAULT_MODULE_STATE_IDLE}"  # it's ready and waiting for a command
		;;
	*)
		printf "${DEFAULT_MODULE_STATE_UNKNOWN}"
		;;
	esac
	
}

#############
# ModuleIsRunning() - returns 0 (true) if the named module is running, 1 (false) if it is not.
#############
function ModuleIsRunning () {
	[[ $# -ne 1 ]] && return 1  # if no arg, then declare it "not running"

	local module=$1	
		
	# the module dir is present, so proceed...
	case "$module" in		
	dispatcher)
		local ppid=($(ps -O ppid | egrep "${DISPATCHER_PROCESS_SIGNATURE}|${DISPMON_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $2}'))
		if [[ "${#ppid[@]}" -eq 0 ]] ; then 
			return 1;	# module is not running
		else
			return 0;	# module is running
		fi
		;;
	
	slink)
		# Slink is "running" if any one of its associated procs (slarchive or slinksweeper) are running
		ModuleIsRunning slinksweeper && return 0
		ModuleIsRunning slarchive && return 0
		return 1 # it's not running
		;;
		
	# Strictly speaking, slarchive and slinksweeper aren't "modules"; they are really sub-modules to slink.
	# Nevertheless, it's handy to treat them as modules so we can test their running status handily.
	slarchive)
		local ppid=( $(ps -O ppid  | grep "${SLARCHIVE_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $2}' ) )
		if [[ "${#ppid[@]}" -eq 0 ]] ; then 
			return 1;	# module is not running
		else
			return 0;	# module is running
		fi
		;;
	
	slinksweeper)	
		local ppid=( $(ps -O ppid  | grep "${SLINKSWEEPER_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $2}' ) )
		if [[ "${#ppid[@]}" -eq 0 ]] ; then 
			return 1;	# module is not running
		else
			return 0;	# module is running
		fi
		;;		

	radio)
		return 1
		;;
		
	pod) # a pod is running if any of its control or program modules are running
		ModuleIsRunning dispatcher && return 0
		ModuleIsRunning slink && return 0
		ModuleIsRunning radio && return 0
		ModuleIsRunning program && return 0
		return 1 # none of the modules were running (or no config file present after 'installpod'), so declare the pod as down
		;;
		
	program) # if ANY program is running
		# get the active MODULES from the pod.conf (except, if pod hasn't yet been installed, skip that part!)
		[[ -e "${PSM_PROJECT}/pod/pod.conf" ]] && source "${PSM_PROJECT}/pod/pod.conf"
		local m
		for m in ${MODULES[@]} ; do  # sift through the MODULES array...
			if ! ArrayContainsElement "${m}" ${DEFAULT_RESERVED_MODULE_NAMES[@]} ; then  # if this one is NOT one of the reserved names (slink, dispatcher, etc.)
				ModuleIsRunning $m && return 0 # then it must a program, so test if it's running
			fi  # if it's not running, continue on to the next module...
		done
		return 1  # couldn't find a running program, so declare "program" (i.e., all programs) as down
		;;
	
	*) # it's none of the above, so assume that it's a program module
		local ppid=($(ps -O ppid | grep "${module}/${DEFAULT_PROGRAM_ELOOP_LAUNCHER} ${DEFAULT_PROGRAM_RUN_KEY}" | grep -v "grep" | awk '{print $2}') )
		if [[ -z "${ppid+x}" ]] || [[ "${ppid}" = "" ]] || [[ "${#ppid[@]}" -eq 0 ]] ; then 
			return 1 # not running
		else
			return 0 # running
		fi
		;;
	esac
	
}

# SittingAtConsole() - Is the current shell running as a regular login shell at the pod's console?
# Returns 0 if the shell is running as a regular log-in shell; returns 1 if it's an ssh session
function SittingAtConsole() {
  	# It appears that $SSH_CLIENT is **always** set by bash, so we test for its emptiness, rather than its set-ness
	# return 0 (true) if SSH_CLIENT is empty
	[[ "x$SSH_CLIENT" = "x" ]] && return 0 || return 1
}


#############
# MachineName() -- Attempt to extract a machine name from a POD address of the form USER@MACHINE.DOMAIN
#############
function MachineName () {
	
	# A pod is generally of the form USER@HOST:PORT, so parse it out...
	local u="$(echo $1 | cut -f1 -d@)"  # USER
	local h="$(echo $1 | cut -f2 -d@ | cut -f1 -d:)"	# HOST
	local p="$(echo $1 | cut -f2 -d@ | cut -f2 -d:)" 	# PORT

	local tidyName=$(echo $h | sed -e "s|.local||")   # foobar.local --> foobar
	printf "${tidyName}"
}
	
# Get the Pod name from pod.conf (or from hostname)
# The POD_NAME in the config file overrides the machine name
function PodName() {
	[[ $# -ge 1 ]] && [[ "$1" = "f" ]] || [[ "$1" = "full" ]] && local doFull=true || doFull=false
	local name=""
	if [[ -e "${PSM_PROJECT}/pod/pod.conf" ]] && \
		source "${PSM_PROJECT}/pod/pod.conf" && \
		[[ ! -z "${POD_NAME}" ]] && \
		[[ "${POD_NAME}" != "" ]] ; then 
			 name="$POD_NAME"
	else
		name=$(hostname -s) # the machine name (e.g., "iris"; 'hostname' with no options gives yields "iris.local"
	fi

	# the "full" version includes the IP number (e.g., iris@23.45.67.89)
	if [[ $doFull = true ]] ; then
		name="${name}@$(PodIP)"
	fi
	printf "$name\n"
}

# print the public IP address of this pod
function PodWANIP() {
	curl ifconfig.me 2>/dev/null  # the public IP address of this server
	printf "\n"
}

# print the local network IP address of this pod
function PodLANIP() {
	# next line is from https://stackoverflow.com/a/13322549/5011245
	local lanip=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
	printf "$lanip\n"
# DEPRECATED	ipconfig getifaddr en0
}

# print the pod's IP address: if it's on a LAN, then print that address, otherwise print the WAN IP address
function PodIP() {
	local wan=$(PodWANIP)
	local lan=$(PodLANIP)
	[[ "${lan}" = "${wan}" ]] && printf "$wan" || printf "$lan"
	printf "\n"
}

# "Sanitize" a string by censoring any IP addresses in it
# e.g., "222.333.444.123" ==> "x.x.444.123"
function SanitizeIP() {
	echo "$1" | sed -E "s/[0-9]+\.[0-9]+\.([0-9]+\.[0-9])/x.x.\\1/" 
}

# The home part of a directory full path gives away the user name (e.g., "/Users/jtb/earthsound/foo/bar").
# This replaces the home part with the string "PSM_PROJECT" (e.g., "PSM_PROJECT/foo/bar")
function SanitizeDirs() {
	echo "$1" | sed -E "s#.*${PSM_PROJECT}#PSM_PROJECT#g"
	# 20230331 I think we need the "/g", in case it's $SOURCEDIRS, which may have several space-delimited dirs.
	# 20230331 FIXME: Can $SOURCEDIRS atually contain space-delimited dirs???
}


# Module_peek() -- print part or all of one of a module's log files
PEEK_DEFAULT_NLINES=20
PEEK_DEFAULT_LOGTYPE="screen"
function Module_peek () {

	# get an array of the available log types for this module
	local logtypes=($(find ${MODULE_LOG_DIR} -depth 1  -name \*.log -exec basename {} \; | sed -e "s/.log//" ))
	local defaultLogType
	[[ $MODULE_TYPE = "pod" ]] && defaultLogType="pod" || defaultLogType="${PEEK_DEFAULT_LOGTYPE}"

	local nlines=${PEEK_DEFAULT_NLINES} logtype=${defaultLogType}
	case $# in
	0)
		;;
	
	1)
		[[ $1 = "help" ]] && Module_peek_usage && return
		[[ $1 = "list" ]] && printf "$MODULE_NAME has these types of log files: ${logtypes[*]}\n" && return
		[[ $1 = "all" ]] && nlines=0 || [[ $1 =~ ^[0-9]+$ ]] && nlines=$1 || logtype=$1 # if arg is a number, then it's nlines; else its a logtype
		;; 
		
	2)	
		if [[ $1 =~ ^[0-9]+$ ]] && [[ $2 =~ ^[0-9]+$ ]] ; then # both are numbers
			printf "*** Both args can't be numbers.\n"
			Module_peek_usage && return
		elif [[ ! $1 =~ ^[0-9]+$ ]] && [[ ! $2 =~ ^[0-9]+$ ]] ; then # both are non-numbers
			if [[ $1 = "all" ]] && [[ $2 != "all" ]] ; then   # "peek all screen"
				nlines=0 && logtype=$2	
			elif [[ $1 != "all" ]] && [[ $2 = "all" ]] ; then   # "peek screen all"
				nlines=0 && logtype=$1	
			else				
				Module_peek_usage && return  # "peak all all"
			fi
		else 
			[[ $1 =~ ^[0-9]+$ ]] && [[ ! $2 =~ ^[0-9]+$ ]] && nlines=$1 && logtype=$2	# peak 20 screen
			[[ ! $1 =~ ^[0-9]+$ ]] && [[ $2 =~ ^[0-9]+$ ]] && nlines=$2 && logtype=$1	# peak screen 20
			[[ $logtype = "all" ]] && printf "*** Invalid log type '${logtype}'.\n" && Module_peek_usage && return
			[[ $logtype = "help" ]] && Module_peek_usage && return
		fi
		;;
	*)	
		printf "*** Too many arguments ($#).\n"
		Module_peek_usage && return
		;;
	esac
	

	# if nlines=0, then cat the entire file, otherwise use tail
	local cmd
	[[ $nlines -eq 0 ]] && cmd="cat" || cmd="tail -${nlines}"
	
	local logfile="${MODULE_LOG_DIR}/${logtype}.log"

	[[ ! -f $logfile ]] && printf "${MODULE_NAME} doesn't have a logfile named '$(basename ${logfile})'.\n" && return

	[[ $nlines -gt 0 ]] && printf "Last $nlines lines of ${MODULE_NAME}'s $(basename ${logfile}):\n"
	$cmd $logfile | sed -e "s/^/==>   /"
}

function Module_peek_usage() {
	local logtypes=($(find ${MODULE_LOG_DIR} -depth 1  -name \*.log -exec basename {} \; | sed -e "s/.log//" ))
	local defaultLogType
	[[ $MODULE_TYPE = "pod" ]] && defaultLogType="pod" || defaultLogType="${PEEK_DEFAULT_LOGTYPE}"
	cat << EOT
Usage: peek [NNN] [LOGTYPE]
Prints part or all of one of ${MODULE_NAME}'s logfiles.
If the word "help" appears in the arguments, then print this usage message.
By default, prints the last ${PEEK_DEFAULT_NLINES} lines of the module's ${PEEK_DEFAULT_LOGTYPE} file.
Options may appear in either order. They are:
     NNN      print the last NNN lines of the log file (default: ${PEEK_DEFAULT_NLINES})
     LOGTYPE  print the named logfile, which may be one of: ${logtypes[*]} (default: ${defaultLogType})

EOT
}


#
# SystemLoad() -- prints the 5-minute system load average appended with a symbol to indicate whether it's rising or falling
# Load avg is obtained from 'uptime'
#
function SystemLoad () {
	local uptime=($(uptime 2>/dev/null | sed -e "s/^.*: //" )) # extract just the times from 'uptime's output
	local load1=${uptime[0]}	# 1-minute load average
	local load5=${uptime[1]}	# 5-minute load average
	local load15=${uptime[2]}	# 15-minute load average
	local slop="0.1" # if the time difference (secs) exceeds this, then we'll declare that the time is either "rising" or "falling"
					# "slop" or "deadband" is an interesting and important concept in control system theory (e.g., thermostats). See https://en.wikipedia.org/wiki/Deadband
	local diff=$( echo "scale=2; $load5 - $load15" | bc -ql )
	local adiff=$(echo $diff | sed -e "s/-//") # absolute value
	local isRising=$( echo "$load5 > $load15" | bc -ql )
	local isFalling=$( echo "$load5 < $load15" | bc -ql )

	# create a descriptive summary of what's going on (rising, falling, stable, high, etc...)
	local summary=""
	[[ $(echo "$load5 <= ${DEFAULT_SAFE_LOAD_AVERAGE}" | bc -lq) -eq 1 ]] && summary="${summary}normal" || summary="${summary}***HIGH***"

	summary="${summary} "
	if [[ $(echo "$adiff > $slop" | bc -ql) -eq 1 ]] ; then # if the change exceeds the "slop", then we declare that the load time has changed
		[[ $isRising -eq 1 ]] && summary="${summary}rising"		# increasing 
		[[ $isFalling -eq 1 ]] && summary="${summary}falling"	# decreasing 
	else
		summary="${summary}stable"
	fi
	printf "${load5} ${summary}"
}

# returns 0 if system load average is tolerable, 1 if it's excessive.
function SystemLoadIsTolerable () {
	local loadval=$(SystemLoad | sed -e "s/ .*//") # get the numerical part of the system load report
	return $(echo "$loadval > ${DEFAULT_SAFE_LOAD_AVERAGE}" | bc -lq)  # bc returns '1' if the relational test is true (i.e., load is NOT tolerable)
}

# Unset all the variables that were set in the config file
function UnsetConfigVariables () {
	unset -v "${CONFIG_REQUIRED_VARIABLES[@]}"
	unset -v "${CONFIG_OPTIONAL_VARIABLES[@]}"
	unset -v "${CONFIG_REQUIRED_ARRAYS[@]}"
	unset -v "${CONFIG_OPTIONAL_ARRAYS[@]}"
	unset -v "${CONFIG_DERIVED_VARIABLES[@]}"
}

# Insert spaces between each character
function SpaceOut() {
	local spaced=$(echo $1 | sed -e "s/\(.\)/\1 /g")
	echo $spaced
}
