#!/bin/bash -f
CURRENT_SCRIPT_VERSION=180	# The major version number of all these scripts (see changelog for details)
#DEBUG_TRACE=1 # Set this (to anything), to force the script output to include pseudo stack-traces (for debugging)

DEFAULT_TIME_FORMAT="%Y.%m.%d %H:%M:%S"	# time format used in logfile timestamps, etc.
DEFAULT_FILE_TIME_FORMAT="%Y%m%d%H%M%S" # time format used for filenames (backups, etc.)
DEFAULT_SAFE_LOAD_AVERAGE=10 # the maximum tolerable 5-minute load avg (from 'uptime'), in seconds (used by pod/housecall and pod/start)
DEFAULT_MODULE_NAME_REGEX='[A-Za-z0-9-]' # the names of new modules may only contain chars from this regex set
DEFAULT_RESERVED_POD_CONTROL_MODULES=(dispatcher slink radio rtmon) # the modules under pod's control
DEFAULT_RESERVED_MODULE_TYPES=(pod program "${DEFAULT_RESERVED_POD_CONTROL_MODULES[@]}") # all the known module types
DEFAULT_RESERVED_MODULE_NAMES=(dispatcher pod psm slink radio rtmon) # audio programs may not have any of these names
DEFAULT_FORMAT_SPACING=25 # format param for printf's in statu reports -- e.g., printf "${DEFAULT_FORMAT_SPACING}%s : foo bar doo dah"
DEFAULT_AUDIO_DEVICE_CHANNELS=16 # default max number of audio channels available
DEFAULT_AUDIO_DEVICE_PREFIX="psmdev"	# prefix for the name of audio devices created by LoopBack (e.g., the "psmdev" in "psmdev13")
# NB: Due to an apparent bug in SoX, names of virtual audio devices must be at least 7 characters long. So this prefix must be at least 6 chars.
DEFAULT_MODULE_STATE_UP="UP"		# a module's process is up and running
DEFAULT_MODULE_STATE_DOWN="DOWN"	# a module's process is not running, but should be
DEFAULT_MODULE_STATE_IDLE="IDLE"	# a module is ready to respond to commands, but is not currently running a process 
DEFAULT_MODULE_STATE_EMPTY="EMPTY"	# a directory is empty
DEFAULT_MODULE_STATE_INACTIVE="-"	# module has not been activated in pod.conf
DEFAULT_MODULE_STATE_HUNG="HUNG"	# a process (or module) is running, but has entered the Twilight Zone
DEFAULT_MODULE_STATE_MISSING="MISSING"	# something (e.g., a directory or an asset) is missing
DEFAULT_MODULE_STATE_UNKNOWN="UNKNOWN"	# wtf	
DEFAULT_MODULE_STATE_OK="OK"		#  "Yeah, OK, I guess. Whatevs."


########### PARAMETERS COMMON TO ALL (OR MOST) MODULES ########
DEFAULT_MODULE_ARCHIVE_DIR="archive" # Path (relative to the module dir) to the dir containing any archive files
DEFAULT_MODULE_ASSETS_DIR="assets" # Path (relative to the module dir) of the assets dir
DEFAULT_MODULE_LOG_DIR="log" # Path (relative to the module dir) of the log dir
DEFAULT_MODULE_WORKSPACE_DIR="workspace" # Path (relative to the module dir) of the workspace dir
DEFAULT_MODULE_SCREEN_LOGFILE="screen.log" # Path (relative to the log dir) of the screen(1) log file
DEFAULT_MODULE_SCREENRC_FILE=".screenrc" # Path (relative to the module dir) of the screen(1) config file
DEFAULT_MODULE_BACKUP_DIR="bak" # Path (relative to the module dir) of the "backup" dir

########### PROGRAM PARAMETERS ##########
DEFAULT_PROGRAM_SOX_PLAY_ALLOWANCE=20 # if sox audio playback duration exceeds the audio file duration by this many secs, then declare that sox is hung 
DEFAULT_PROGRAM_BUTTCONFIG_PATH="${DEFAULT_MODULE_WORKSPACE_DIR}/buttConfig.txt" # Location (relative to the program dir) of the auto-generated butt config file
DEFAULT_PROGRAM_EVENTLOG="eventlog.txt"  # Path (relative to the module dir) of the eventlog file
DEFAULT_PROGRAM_RUN_KEY="lhcfirefly"	# pseudo-passkey to prevent accidental command-line launches of esound loop script, _update scripts, etc.
DEFAULT_PROGRAM_ELOOP_LAUNCHER="_launch"	# name of command (script in program dir) that launches the loop
DEFAULT_PROGRAM_BUTT_SONGFILE_ERR="buttError.songfile.txt" # Path (relative to the workspace dir) of a butt error file
DEFAULT_PROGRAM_BUTT_SOCKET_ERR="buttError.socket.txt" # Path (relative to the workspace dir) of a butt error file
DEFAULT_PROGRAM_ARCHIVE_KEEPDAYS=15 # how long to keep files lying around in a program's /archive directory before deleting them

########### POD PARAMETERS ##########
DEFAULT_POD_DIR="pod"	# Path (relative to PSM_PROJECT) of the pod dir
DEFAULT_POD_SENDBATCH_LOGFILE="sendbatch.log" # Path (relative to the module log dir) of the log file
DEFAULT_POD_MAX_PROGRAM_COUNT=15 # Max number of programs that can run in a single pod.
DEFAULT_POD_CONTROL_MODULES=(dispatcher radio rtmon slink) #
DEFAULT_POD_QUEUE_DIR="queue" # Path (relative to the pod dir) of the pod's queue dir
POD_QUEUE_DIR="${PSM_PROJECT}/${DEFAULT_POD_DIR}/${DEFAULT_POD_QUEUE_DIR}" # the directory where files are queued for upload to the webserver
DEFAULT_POD_WEBHOST_ROOT_DIR="earthsound.earth" # path to the remote website directory, relative to web server's ssh/sftp login dir; can be overriden in pod.conf
DEFAULT_POD_SYNC_DIR="sync" # Path (relative to the pod dir) of the pod's sync dir
POD_SYNC_DIR="${PSM_PROJECT}/${DEFAULT_POD_DIR}/${DEFAULT_POD_SYNC_DIR}" # abs path to the pod's sync dir

########### DISPATCHER PARAMETERS ##########
DEFAULT_DISPATCHER_SENDBATCH_LOGFILE="sendbatch.log"		# Path (relative to the module log dir) of the log file
DEFAULT_DISPATCHER_WAIT=10	# wait time (seconds) before issuing 'sendbatch' command to the next pod
# Tradeoff: If DEFAULT_DISPATCHER_WAIT is too high, the website won't receive timely data 
# (especially if the dispatcher is serving many, many pods); if too low, the LAN will be hammered by 
# relentless ssh activity (is this really an issue??) and maybe the web host will complain about too many successive connections??
DEFAULT_DISPATCHER_LOG_LOOPCOUNT=1	# number of passes through the Dispatcher loop between logging events
# Tradeoff: If DEFAULT_DISPATCHER_LOG_LOOPCOUNT is low, the logfile will be cluttered with unnecessary info; 
# if high, it won't contain much useful info about when something bad happened.

DEFAULT_DISPATCHER_TIMEOUT=120	# seconds the dispatcher should wait before trying to reestablish a timed-out sftp connection
DEFAULT_DISPATCHER_MAX_TIMEOUTS=10	# max number of consecutive timeouts before the dispatcher should simply give up and quit
DEFAULT_SSH_CONNECTTIMEOUT=10 # seconds that ssh should try opening a connection before giving up (ssh option 'ConnectTimeout')
DEFAULT_DISPMON_INTERVAL=60 # seconds between dispmon's checkins

########### SLINK PARAMETERS ##########
DEFAULT_SLINK_LOGFILE="slink.log" # Path (relative to the module log dir) of the log file
DEFAULT_SLINK_SWEEP_INTERVAL=3600	# seconds between Sweeper's file checks
DEFAULT_SLINK_INBOUND_DIR="inbound" # Location (relative to the module dir) of the inbound dir

########### RADIO PARAMETERS ##########
DEFAULT_RADIO_LOGFILE="radio.log"
DEFAULT_RADIO_SOURCE_DIR="sources" # Location (relative to the radio dir) of the radio source dir

# the number of logfile lines to display in the 'status' command
DEFAULT_LOGFILE_TAIL=10

########### REMOTE WEBSITE PARAMETERS ##########
# Website directories for uploaded files 
# NB: The full paths will be built from the POD_WEBHOST_ROOT_DIR as specified in the pod config 
# (or the DEFAULT_POD_WEBHOST_ROOT_DIR)
DEFAULT_WEBHOST_INBOUND_DIR="unver/inbound" # path, relative to POD_WEBHOST_ROOT_DIR, of inbound directory
DEFAULT_WEBHOST_PODS_DIR="${DEFAULT_WEBHOST_INBOUND_DIR}/pods" # path, relative to POD_WEBHOST_ROOT_DIR, of pods directory
DEFAULT_WEBHOST_DISPATCHERS_DIR="${DEFAULT_WEBHOST_INBOUND_DIR}/dispatchers" # path, relative to POD_WEBHOST_ROOT_DIR, of dispatchers directory
DEFAULT_WEBHOST_ARCHIVE_DIR="archives/data" # path, relative to POD_WEBHOST_ROOT_DIR, of archive directory
