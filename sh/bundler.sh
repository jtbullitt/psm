#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)

# This script helps you bundle up the apps for a psm distro. 
# It doesn't create a "real" OSX bundle, but it at least gathers up the XCode binaries and libraries
# into sort of a self-contained package (psm/bin and psm/lib)

# Use homebrew install the required ancillary apps and libraries:
# 	boost
# 	gnuplot
# 	id3v2
# 	imagemagick
# 	jsoncpp
# 	libsamplerate
# 	portaudio
# 	sox 

# DEPRECATED sudo port install gnuplot -aquaterm -luaterm +pangocairo -wxwidgets -x11

# a little safeguard against accidentally running this script...
[[ ! $1 = 8675309 ]] &&	printf "You appear to not know what you're doing. Please don't run this script.\n" && exit
shift

ME="$(basename $0 )"	# the name of this script
CALLER="bundleize"

# Get the constants that concern the apps and libs
APPS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/APPS.sh" 
source "${APPS}"

ALLOWED_MACHINES=(iris.local) # machines on which this script is allowed to run
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # the containing dir of this script (http://stackoverflow.com/a/246128/5011245)
MODULE_TYPE="bundler" 
MODULE_NAME="bundler"
source "${SCRIPTS_DIR}/shared.sh" # get the common functions and set some crucial variables


function UsageExit () {
	appList="all" # list of all available TARGETS (i.e., apps)
	for app in ${ALL_APPS[@]} ; do
		appList="$appList $(basename ${app})"
	done
	cat << EOT
${CALLER}: a tool to gather up the PSM apps and localize their associated dylibs for a self-contained PSM installation.
Usage: ${CALLER} ACTION [APPNAME]
where 'ACTION' is one of:
	list              list the apps that ${CALLER} knows about
	val [APPNAME]     basic validity check: prints number of errors in apps and dylibs 
	                  (prints '0' if everything OK)
	check [APPNAME]   prints more detailed results of validity check
	clean             deletes all the XCode apps and their dylibs
	install APPNAME   install a fresh copy of APPNAME into the bin and collects its dylibs
	                  (APPNAME 'all' does all of them)

and where 'APPNAME' may be any of the following:
	$appList 
EOT
	local brewApps=""
	for a in ${HOMEBREW_APPS[@]} ; do
		brewApps="$brewApps $(basename $a)"
	done
	printf "Some apps (e.g.,${brewApps}) must be installed using homebrew. For example, to install 'gnuplot':\n"
	BrewHowTo gnuplot

	SpecialHowTo
	exit
}

############################################
# define some functions...
function Main () {

	if [[ -z ${PSM_PROJECT+x} ]] ; then 
		printf "${CALLER}: *** Environment variable PSM_PROJECT not set.\n"
		exit
	fi

	cd "${PSM_PROJECT}/psm"
	if [[ ! -d "bin" ]] || [[ ! -d "lib" ]] ; then
		printf "${CALLER}: Missing directory (lib or bin). I think you're lost.\n"
		exit
	fi
	
	local action="" target=""
	
	# parse the command line
	case "$#" in
	1)
		action="$1"
		;; 
	2)
		action="$1"
		target="$2"
		;;
	*)
		UsageExit
		;;
	esac

	case "$action" in
	
	# print a list of the apps
	list)
		doList
		;;

	# check the validity of the app and its libraries
	check)
		shift
		doCheck $*
		;;
		
	# print 0 if app is valid, non-zero otherwise
	val)
		shift
		doCheck count $*
		;;
		
	# remove the XCode apps and their dylibs
	clean) 
		ExitIfNotDevMachine
		doClean
		;;
		
	# install a fresh copy of the target XCode app and collect its libraries
	install)
		ExitIfNotDevMachine
		[[ "${target}" = "" ]] && printf "${CALLER}: *** missing TARGET for action '${action}'.\n" && UsageExit
		doInstall $target
		;;
		
	*)
		printf "${CALLER}: Unknown action '${action}'.\n"
		UsageExit
		;;
	esac
}

function doList () {
	local appPath flag
	printf "Here are the apps that ${CALLER} knows about.\n"
	printf "Those marked * are missing:\n"

	printf "   Homebrew builds:\n"
	for appPath in "${HOMEBREW_APPS[@]}" ; do
		[[ -f "${appPath}" ]] && flag=" " || flag="*"
		printf "	${flag} $(basename ${appPath})\n"
	done
	
	printf "\n   Local XCode builds:\n"
	for appPath in "${XCODE_APPS[@]}" ; do
		[[ -f "${appPath}" ]] && flag=" " || flag="*"
		printf "	${flag} $(basename ${appPath})\n"
	done
	
	printf "\n   Special apps (those not actually installable via this script):\n"
	for appPath in "${SPECIAL_APPS[@]}" ; do
		[[ -f "${appPath}" ]] && flag=" " || flag="*"
		printf "	${flag} $(basename ${appPath})\n"
	done
}

# Do a fresh install of an XCode app -- i.e., copy it from its staging dir and collect its libraries
function doInstall () {
	ExitIfNotDevMachine
	[[ $# -ne 1 ]] && printf "*** Missing TARGET for 'do'\n" && UsageExit
	local appName="$1"
	local appPath
	
	if [[ "${appName}" = "all" ]] ; then
		# get confirmation from the user (https://stackoverflow.com/a/226724/5011245)
		while true; do
			read -p "Do you really want to re-install all ${#ALL_APPS[*]} apps? " yn
			case $yn in
				[Yy]* ) printf "OK. Installing...\n"; break;; # OK -- continue on
				[Nn]* ) printf "OK. Can't say I blame you.\n"; return ;;  # we're done
				* ) echo "Please answer y or n.";;
			esac
		done
		for appPath in "${ALL_APPS[@]}" ; do
			doInstall $(basename $appPath)
		done
		return
	fi
	
	# Non-XCode apps require special handling
	# Is it a homebrew app?
	for appPath in "${HOMEBREW_APPS[@]}" ; do
		if [[ "${appName}" = $(basename $appPath) ]] ; then
			if [[ -e "${appPath}" ]] ; then
				printf "${appName} already installed. (OK)\n"
			else
				BrewHowTo $appName
			fi
			return
		fi
	done
	
	# Is it a 'special' app?
	for appPath in "${SPECIAL_APPS[@]}" ; do
		if [[ "${appName}" = $(basename $appPath) ]] ; then
			if [[ -e "${appPath}" ]] ; then
				printf "${appName} already installed. (OK)\n"
			else
				SpecialHowTo $appName
			fi
			return
		fi
	done
	
	# Is it an XCode app?
	for appPath in "${XCODE_APPS[@]}" ; do
		if [[ "${appName}" = $(basename $appPath) ]] ; then
			if [[ -e "${appPath}" ]] ; then # app already present
				printf "${appName} already installed. (OK)\n"
				while true; do
					read -p "Do you want to re-install ${appName}? " yn
					case $yn in
						[Yy]* ) printf "OK. Installing...\n"; break;; # OK -- continue on
						[Nn]* ) printf "OK. Skipping re-installation.\n"; return ;;  # we're done
						* ) echo "Please answer y or n.";;
					esac
				done
				InstallApp $appName
			else # app not present, so install it
				InstallApp $appName
			fi
			return
		fi
	done
	
	# If we've gotten this far, then the app must be unknown
	printf "Don't know how to install '${appName}'.\n"
}

#
# InstallApp
# Forcefully installs the named app (not the full pathname) and collects its dylibs
function InstallApp() {
	[[ $# -ne 1 ]] && printf "*** InstallApp requires exactly one argument. Found $# \n" && exit
	appName="$1"
	devPath="${DEV_BINDIR}/$appName" # the path to the app in the development bin dir
	psmPath="${BINDIR}/$appName" # the path to the app in the psm bin dir
	if [[ ! -e $devPath ]] ; then
		printf "App '${devPath}' not found in development directory. Can't install.\n"
		exit
	fi
	printf "Installing $appName (${devPath})...\n"

	cp $devPath $psmPath
	[[ $debug ]] && printf " copied\n"
	
	# get a list of dylibs referenced within the app
	dylibPaths=($(otool -L "${psmPath}" | egrep "[ 	]" | sed -e "s|(compatibility.*||" | grep "${DEV_LIBDIR}" ))
	
	# for each dylib
	local relativeDir="@executable_path/../lib" # for the path that will be inserted into the dylibs (with install_name_tool)
	for devDylibPath in "${dylibPaths[@]}" ; do  		# Array of paths like "/Users/jtb/dev/lib/libjtbutils.dylib"
		dylibName=$(basename $devDylibPath) 			# e.g., "libjtbutils.dylib"
		relativePath="${relativeDir}/${dylibName}"	# e.g., "@executable_path/../lib/libjtbutils.dylib"
		psmDylibPath="${LIBDIR}/${dylibName}"
		install_name_tool -change ${devDylibPath} ${relativePath} ${psmPath} 2> /dev/null # edit the app
		InstallDylib $devDylibPath
	done	

	# codesigning flummox frobulator to get the app to not be instantly be "killed 9"
	# from https://stackoverflow.com/a/77971839 
	xattr -c "${psmPath}" && sudo codesign -f -s - "${psmPath}" 2> /dev/null 
}

recursionDepth=0 #depth of InstallDylib recursion
#debug=1

# Usage:
# InstallDylib PATH_TO_DEV_DYLIB
# Copies the named dylib from the dev directory to the psm directory and 
# examines PATH_TO_DEV_DEYLIB (a dylib) with otool to find the dylibs it contains.
# For each one it finds, it uses install_name_tool to reassign the path to LIBDIR
# It's recursive: it processes each dylib (reference) it finds within a dylib.
function InstallDylib() {
	[[ $# -ne 1 ]] && printf "*** InstallDylib requires exactly one argument. Found $# \n" && exit
		
	(( recursionDepth++ )) # for debugging

	# this padding business is for debugging and making pretty output
	local pad=""
	local n=0
	while [[ $n -le $recursionDepth ]] ; do
		pad="${pad}\t"
		(( n++ ))
	done

	local relativeDir="@executable_path/../lib" # for the path that will be inserted into the dylibs (with install_name_tool)
	local fromPath=$1						# e.g., "/Users/jtb/dev/lib/librt130.dylib"
	local fromName=$(basename $fromPath)	# e.g. "librt130.dylib"
	local toPath="${LIBDIR}/${fromName}"	# e.g., "/Users/jtb/earthsound/psm/lib/librt130.dylib"
	
	[[ $debug ]] && printf "${pad}${recursionDepth}: Working on ${fromName} (${fromPath})  \n"

	if [[ -e $toPath ]] ; then 	# if this dylib has already been copied to the psm lib dir 
								# (and presumably fixed) then skip it
		[[ $debug ]] && printf "${pad}${recursionDepth}: Skipping $fromName (${toPath}). It already exists (OK)\n"
		[[ $debug ]] && printf "${pad}${recursionDepth}: Finished with $1 \n"
		(( recursionDepth-- ))
		return
	fi

	# Now copy and fix the dylib
	[[ $debug ]] && printf "${pad}${recursionDepth}: Copying $fromPath to $toPath...\n"
	cp "$fromPath" "$toPath"				# then copy it to to the psm lib dir
	
	# N.B. After the above 'cp', toPath is the dylib that we're editing
	
	# collect a list of the dev dylibs in this dylib
	local dylibPaths=($(otool -L "${toPath}" | egrep "[ 	]" | sed -e "s|(compatibility.*||" | grep "${DEV_LIBDIR}" ))
	[[ $debug ]] && printf "${pad}${recursionDepth}: $(basename ${toPath}) contains ${#dylibPaths[@]} dylibs\n"
	local thisDylibNewPath="$toPath"					# e.g., /Users/jtb/earthsound/psm/lib/librt130.dylib
	install_name_tool -id ${thisDylibNewPath} ${thisDylibNewPath} 2> /dev/null # change the id (the first name that appears in 'otool -L')	
echo DEBUG	install_name_tool -id ${thisDylibNewPath} ${thisDylibNewPath} 
	local nLib=1
	for devDylibPath in "${dylibPaths[@]}" ; do	# Array of dev paths like "/Users/jtb/dev/lib/libjtbutils.dylib"
		local oldPath="${devDylibPath}"				# e.g., "/Users/jtb/dev/lib/libjtbutils.dylib"	
		local oldName=$(basename $oldPath) 			# e.g., "libjtbutils.dylib"
		local newPath="${LIBDIR}/${oldName}"			# e.g., "/Users/jtb/earthsound/psm/lib/libjtbutils.dylib"
		
#		printf "${pad}${recursionDepth}.${nLib}: install_name_tool $fromName : $devDylibPath ==> $newPath \n"
#		echo install_name_tool -change ${oldPath} ${newPath} ${thisDylibNewPath}
		[[ $debug ]] && printf "${pad}${recursionDepth}.${nLib}: install_name_tool -change ${oldPath} to ${newPath} in ${thisDylibNewPath} \n"
		install_name_tool -change ${oldPath} ${newPath} ${thisDylibNewPath} 2> /dev/null # edit the app
		xattr -c "${thisDylibNewPath}" && sudo codesign -f -s - "${thisDylibNewPath}" 2> /dev/null # blurp frobulator to get the app to not be instantly "killed 9" -- see https://stackoverflow.com/a/77971839

		InstallDylib $oldPath	# Recurse through the dylibs...
		(( nLib++ ))
	done
	
	[[ $debug ]] && printf "${pad}${recursionDepth}: Finished with ${thisDylibNewPath} \n"
	(( recursionDepth-- ))
}





# Check that all the required libs are present and that the apps are more or less usable

function InitCheckErrors() {
	checkResultErrors=0;
	checkResultMissingApps=0;
	checkResultBrokenApps=0;
	checkResultMissingLibraries=0;
}

function doCheck () {
	InitCheckErrors
	local target
	local mode="verbose" # by default, be verbose; otherwise just print the number of errors

	if [[ $# -eq 0 ]] ; then
		target="all"
	fi
	if [[ $# -ge 1 ]] && [[ $1 == "count" ]] ; then
		mode="count"
		shift
	fi
	if [[ $# -ge 1 ]] ; then
		target=$1
	fi

	if [[ $target = "all" ]] ; then
		for appPath in "${ALL_APPS[@]}" ; do
			if [[ $mode != "count" ]] ; then 
				printf "\n"
			fi
			app=$(basename $appPath)
			doCheckOne $app
		done
	else
		doCheckOne $target
	fi
	
	if [[ $mode = "count" ]] ; then 
		printf "${checkResultErrors}\n"
	else # (verbose)
		if [[ $checkResultErrors -gt 0 ]] ; then
			printf "\n*** Ouch! Errors found:\n"
			printf "         missing apps : ${checkResultMissingApps}\n"
			printf "          broken apps : ${checkResultBrokenApps}\n"
			printf "    missing libraries : ${checkResultMissingLibraries}\n"
			printf "*** Try reinstalling any bad apps with '${CALLER} install APP_NAME'.\n"
		else 
			printf "No errors found.\n"
		fi
	fi
}


# doCheckOne
# Check a single app by its name (not its path)
# e.g., "doCheckOne esound2"
function doCheckOne () {	
	if [[ $# -ne 1 ]] ; then
		printf "*** doCheckOne is missing an argument.\n"
		exit; # bug in the code, so exit
	fi
	if [[ $1 = "all" ]] ; then
		printf "*** doCheckOne can't check all apps. Call 'doCheck all' instead.\n"
		exit; # bug in the code, so exit
	fi
	
	targetApp="$1"

	# determine which kind of app it is
	local appType="null"
	local appPath="null"
	local appName="null"
	local name
	local path
	
	# is it an XCode app?
	for path in "${XCODE_APPS[@]}" ; do
		name=$( basename "${path}" )
		if [[ "${targetApp}" = "${name}" ]] ; then
			appType="xcode"
			appPath=$path
			appName=$name
			break
		fi
	done
		
	# is it a Brew app?
	for path in "${HOMEBREW_APPS[@]}" ; do
		name=$( basename "${path}" )
		if [[ "${targetApp}" = "${name}" ]] ; then
			appType="brew"
			appPath=$path
			appName=$name
			break
		fi
	done
	
	# is it a 'special' app?
	for path in "${SPECIAL_APPS[@]}" ; do
		name=$( basename "${path}" )
		if [[ "${targetApp}" = "${name}" ]] ; then
			appType="special"
			appPath=$path
			appName=$name
			break
		fi
	done


	local is_broken=0 # true if app doens't run
	local nLibsMissing=0 # how many libs are missing from this app
	local nErrors=0


	# if app not found, then return
	if [[ $appType = "null" ]]; then
	  printf "Don't know about ${targetApp}.\n" && checkResultErrors=1 && return
	fi
	
	[[ $mode = "verbose" ]] && printf "Validating ${appName}... "


	# is the app even present ? If not, there's not much more we can do
	if [[ ! -e "${appPath}" ]] ; then
		[[ $mode = "verbose" ]] && printf "*** MISSING APP ***\n"
		(( checkResultErrors++ ))
		(( checkResultMissingApps++ ))
		return  # no point in continuing; skip to next app
	else
		[[ $mode = "verbose" ]] && printf "Found the app. "
	fi
	
	
	
	# Does the app even run? 
	# Check this by invoking it with the appropriate --version flag
	local versionFlag="--version"
	[[ ${appName} = "slarchive" ]] && versionFlag="-V"
	[[ ${appName} = "audiodev" ]] && versionFlag="-v"
	[[ ${appName} = "buttj" ]] && versionFlag="-v"
	[[ ${appName} = "magick" ]] && versionFlag="-version"
	
	# execute the -version option as a test for the apps operability (throw out both stdout and stderr)
	if [[ $( ("${appPath}" "${versionFlag}" 2>&1) > /dev/null ; echo $?) -ne 0 ]] ; then
		if [[ $mode = "verbose" ]] ; then
			printf "*** BROKEN ***\n"
			printf "*** Here's what happens when you run '${appPath} ${versionFlag}':\n"
			"${appPath}" "${versionFlag}" | fold -w 100 -s | sed -e "s/^/   @@  /" 
		fi
		is_broken=1
		(( nErrors++ ))
	fi	

	# Check that apps and libraries were bundled correctly
	# No need to check if it's a homebrew or special app; we already know that it was installed properly
	if [[ $appType = "special" ]] || [[ $appType = "brew" ]]; then
		if [[ $mode = "verbose" ]] && [[ $nErrors -eq 0 ]] ; then 
			printf "OK\n"  # printf "Skipping dylib check of ${appName}. (OK)\n"
		fi
		if [ $is_broken -eq 1 ] ; then
			(( checkResultBrokenApps++ ))
		fi
		checkResultBrokenApps=$(( $nAppsBroken + $checkResultBrokenApps ))
		checkResultErrors=$(( $checkResultErrors + $nErrors ))
		checkResultMissingLibraries=$(( $checkResultMissingLibraries + $nLibsMissing ))
		return;
	fi
	
	
	# Collect the paths to this app's dependent libs (cleaning them up a little, first)

	# Use 'otool' to collect the paths to the dylibs required by this app.
	# Use 'sed' to convert  "@executable_path" to a bona fide filesystem path.)
	# NB we ignore /usr/lib/* because those should be present in every OS
	# we also ignore /System/Library/* because they're part of the OS (?) and they don't actually exist as regular files (?)
	local dylibPaths=($(otool -L "${appPath}" | egrep "[ 	]" | \
		sed -e "s|@executable_path|$(dirname ${appPath})|"\
		-e "s|(compatibility.*||" \
		-e "/\/usr\/lib\//d"\
		-e "/\/opt\/homebrew\//d"\
		-e "/\/System\/Library\//d"\
		))
	# Now step through each dylib path, 
	for dylib in "${dylibPaths[@]}" ; do  			# Ex: dylib=/Users/jtb/earthsound/psm/bin/../lib/libportaudio.2.dylib
		local libname=$(basename "${dylib}")		# Ex: libportaudio.2.dylib 
		local libdirname=$(dirname "$dylib")		# Ex: /Users/jtb/earthsound/psm/bin/../lib
		local lib
		if [[ -e $libdirname ]] ; then				# if this dir exists, then resolve it into a full absolute path name
			local libdir=$(cd $(dirname "$dylib") ; pwd) # Ex: /Users/jtb/earthsound/psm/lib
			lib="${libdir}/${libname}"			# Ex: /Users/jtb/earthsound/psm/lib/libportaudio.2.dylib
		else # if the dir doesn't exist
			lib=$dylib	#then call the lib by its original full path name
		fi
		if [[ ! -e "${lib}" ]] || [[ "${lib}" =~ ^/opt/ ]] || [[ "${lib}" =~ ^/usr/local ]]; then # if it doesn't exist, or it's found in the wrong directory...
			if [[ $nLibsMissing -eq 0 ]] && [[ $mode = "verbose" ]] ; then
				printf "*** MISSING LIBRARIES:\n"
			fi
			if [[ $mode = "verbose" ]] ; then 
				printf "	${lib}"
				if [[ "${lib}" =~ ^/opt/ ]] ; then
					printf "   (refers to a library in non-portable /opt . Can't deal with it.)"
				fi
				printf "\n"
			fi
			(( nLibsMissing++ ))
			(( nErrors++ ))
		else
			:
		fi
	done
		
	if [[ $mode = "verbose" ]] && [[ $nErrors -eq 0 ]] ; then 
		printf "OK\n"
	fi
	
	# update the error counters
	if [ $is_broken -eq 1 ] ; then
		(( checkResultBrokenApps++ ))
	fi
	checkResultBrokenApps=$(( $nAppsBroken + $checkResultBrokenApps ))
	checkResultErrors=$(( $checkResultErrors + $nErrors ))
	checkResultMissingLibraries=$(( $checkResultMissingLibraries + $nLibsMissing ))
	return
}


# Delete the XCode apps (and all their dylibs). 
function doClean () {
	ExitIfNotDevMachine
	
	# get confirmation from the user (https://stackoverflow.com/a/226724/5011245)
	while true; do
		read -p "Do you really want to delete all the apps and all their dylibs? " yn
		case $yn in
			[Yy]* ) printf "OK.\n"; break;; # OK -- continue on
			[Nn]* ) printf "OK. Can't say I blame you.\n"; return ;;  # we're done
			* ) echo "Please answer y or n.";;
		esac
	done
	printf "Deleting XCode dylibs... "
	dylibs=($(find ${LIB_DIR} -depth 1 -name '*.dylib')) # find them all
	if [[ ${#dylibs[@]} -eq 0 ]] ; then
		printf "none found. (OK)\n"
	else
		printf "\n"
		for dylib in "${dylibs[@]}" ; do
			printf "	${dylib}..."
			/bin/rm "${dylib}" && printf "removed (OK)\n"
		done
	fi

	printf "Deleting XCode apps...\n"
	for appPath in "${XCODE_APPS[@]}" ; do
		appName=$(basename $appPath)
		printf "\t${appName}..."
		if [[ ! -e "${appPath}" ]] ; then
			printf "not found (OK)\n"
		else
			/bin/rm "${appPath}" && printf "removed (OK)\n"
		fi
	done
}

# Exit this script if you're not running this on one of the development machines
function ExitIfNotDevMachine () {
	host=$(hostname -f)
	if ! ArrayContainsElement "${host}" "${ALLOWED_MACHINES[@]}" ; then
		printf "This script should only be used on a PSM development machine. Can't build on '${host}'. Ta.\n"
		exit 1
	fi
}

function BrewHowTo() {
	cat << EOT
App '${1}' must be installed with homebrew:
	$ brew install ${1}
If you haven't yet installed brew, do this:
	/bin/bash -c "\$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
EOT
}

function SpecialHowTo() {
	local specialApps=""
	for a in ${SPECIAL_APPS[@]} ; do
		specialApps="$specialApps $(basename $a)"
	done
	cat << EOT
Installation of some apps (e.g.,${specialApps}) requires special handling.
	Loopback: Download the macOS app from https://rogueamoeba.com/loopback/
		and run the installer. Launch Loopback. Follow instructions to allow permissions.
		Purchase a license. Enter license key in Loopback > License...
EOT
}


############################################

Main "$@"
