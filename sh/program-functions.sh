#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
# Various functions used by the program scripts

MODULE_TYPE="program"
# MODULE_NAME may have previously been set in UpdateModule, in the pod's "install" command.
# If it wasn't, then deduce the MODULE_NAME from the directory name
if [[ -z ${MODULE_NAME} ]] ; then 
	let "lastarg = ${#BASH_SOURCE[@]} - 1"
	MODULE_NAME="$(basename $(cd $(dirname ${BASH_SOURCE[${lastarg}]}) ; pwd) )" # the directory that contained the shell command that brought us here
fi
MODULE_FUNCTION_PATH="${BASH_SOURCE[0]}" # path to this module function file
MODULE_FUNCTION_FILENAME="$( basename ${BASH_SOURCE[0]} )" # handy name of this module function file

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # the containing dir of this script (http://stackoverflow.com/a/246128/5011245)
source "${SCRIPTS_DIR}/DEFAULTS.sh" # get the default params
source "${SCRIPTS_DIR}/shared.sh" # get the common functions and set some crucial variables
MODULE_COMMANDS=(attach buttctl clean help peek restart start status stop _update val ver "${DEFAULT_PROGRAM_ELOOP_LAUNCHER}") # commands to be installed from templates
MODULE_SPECIAL_DIRS=(${DEFAULT_MODULE_ASSETS_DIR} ${DEFAULT_MODULE_ARCHIVE_DIR} ${DEFAULT_MODULE_BAK_DIR}) # special dirs (unique to this module) that must be present 
MODULE_SPECIAL_FILES=(${DEFAULT_PROGRAM_EVENTLOG} "demo-${DEFAULT_PROGRAM_EVENTLOG}" ${DEFAULT_MODULE_SCREENRC_FILE})	# special files to be installed from templates
MODULE_ASSETS=(intervalSignal.wav) # assets to install 
MODULE_REQUIRED_BINARIES=($BIN_ESOUND $BIN_AUDIODEV $BIN_EVENTOVERLAY $BIN_SOXOVERLAY $BIN_SOX $BIN_GNUPLOT $BIN_MAGICK $BIN_ID3V2)
BUTT_HUNG_ERR_PATHS=(${MODULE_WORKSPACE_DIR}/${DEFAULT_PROGRAM_BUTT_SONGFILE_ERR} ${MODULE_WORKSPACE_DIR}/${DEFAULT_PROGRAM_BUTT_SOCKET_ERR})
MODULE_USER_PREFERENCES=("${MODULE_TYPE}.conf" ${DEFAULT_PROGRAM_EVENTLOG}) # user-configurable files (to be saved with the "backup" command)
MODULE_PROTECTED_FILES=("${MODULE_USER_PREFERENCES[@]}" "${MODULE_COMMANDS[@]}" "${MODULE_SPECIAL_FILES[@]}" "demo-${MODULE_TYPE}.conf") # files that will never be clean'ed
ELOOP_LAUNCHER_PATH="${MODULE_DIR}/${DEFAULT_PROGRAM_ELOOP_LAUNCHER}"	# the command to launch the esound loop script

PROGRAM_SOX_SIGNATURE="${BIN_SOX} --buffer" # search pattern for ps

# jtb 20230423 debugging sox hanging while playing hello.aiff
PROGRAM_SOX_SIGNATURE="${BIN_SOX} -" # search pattern for ps

# List of parameters to expect in the config file
CONFIG_REQUIRED_VARIABLES=(\
	SOURCEDIRS \
	CHANNELS \
	DATATYPE \
	FIXRATE \
	GAMMA \
	HOURS \
	INSTRUMENT \
	INSTRUMENT_TYPE \
	LOCATION \
	MAXFREQUENCY \
	NETWORK \
	RECORDER \
	SAMPLE_RATE \
	SIGNAL_SOURCE \
	SIGNAL_TYPE \
	STATION \
	STATION_LAT \
	STATION_LON \
	STATION_URL \
)
CONFIG_REQUIRED_ARRAYS=()
CONFIG_OPTIONAL_VARIABLES=(\
	DBGAIN \
	AUDIO_DEVICE \
	ENDTIME \
	LOOPMAX \
	NETWORK_URL \
	SOXCOMPAND \
	SOXFILTER \
	SPECTROPOST \
	COMMENT \
	SHOUTCAST \
	CC_PORT \
	CC_ACCOUNT \
	CC_STREAMURL \
)
CONFIG_DEPRECATED_VARIABLES=( \
	CONFIGVERSION \
	REMIX DEVICE \
	CRASH_EMAIL \
	ARCHIVEDIR \
	SOURCEDIR \
	STREAM \
)

# Params that are derived from the config params and are used widely
CONFIG_DERIVED_VARIABLES=(SHOUTCAST_HOST_IP SHOUTCAST_PORT SHOUTCAST_PASSWORD)


# Params that only the developer needs to know about
# VERBOSITY: 
# By default, only minimal diagnostic info appears on the terminal (VERBOSITY=0).
# For debugging, uncomment the next line and set it to a positive integer.
# The higher the number, the more annoying it gets.
CONFIG_HIDDEN_VARIABLES=(VERBOSITY)




#############
# Program_help() -- print a help message
#############
function Program_help () { 
	cat << EOT
A PSM ${MODULE_TYPE} module may be controlled from the command line.
Commands are bash scripts; to invoke a command, type its path. 
For example, to generate this help message from the PSM project directory (${PSM_PROJECT}), 
type '${MODULE_TYPE}/help'. Some commands take optional arguments.

   Command     What it does
   =======     ============
   attach      Attach this ${MODULE_TYPE} to the current terminal. 
               (To re-detach from the ${MODULE_TYPE} and return to the shell, type "CTRL-a CTRL-d".)
   buttctl     Control this program's instance of BUTT (Broadcast Using This Tool).  
               Usage: ${MODULE_NAME}/buttcl [start | stop | restart | status]
                   start   : start BUTT
                   stop    : stop BUTT (if it's running)
                   restart : stop, then start BUTT
                   status  : print BUTT's status (running, not-running, etc.)
   clean       Delete any temporary or stray files from the ${MODULE_TYPE} directory and roll over the logfiles.
               (Reminder: be careful adding your own files to this directory, as 'clean' will
               delete them, unless their names end in ".txt".)
   help        Print this help info.
   start       Launch this ${MODULE_TYPE} module, which starts in a virtual terminal (screen(1)), and then
               returns control to the shell.
   status      Print the run status of this ${MODULE_TYPE} module
               Usage: ${MODULE_NAME}/status [verbose | code]
                  verbose  : also print the last few lines of the log file
                     code  : print only a coded summary of the status
   stop        Stop this ${MODULE_TYPE} module (if it is running)
   val         Validate the ${MODULE_TYPE} module's config file and print the result.
               Usage: ${MODULE_NAME}/val [count | verbose | oneliner]
                  count    :  print the number of errors
                  verbose  :  print a detailed report
                  oneliner :  print a one-line summary [default]
   ver         Show the codebase version number
               Usage: ${MODULE_NAME}/ver [full] 
                      full : print the full (major and minor) version numbers
               (By default, just print the major version number.)

EOT

}


#############
# Program_restart() -- restart a single program
# Optional argument(s) are words describing the reason (usually to address an exception detected by the pod).
#############
function Program_restart () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	ConfigExistsOrDie $callingFrom
	
	local logtext="${callingFrom}: restarting program ${MODULE_NAME}"
	if [[ $# -ge 1 ]] ; then # if there are more args
		local reason="$@"
		LogException "${callingFrom}: ${reason}"
		logtext="${logtext} (Exception: ${reason})" # append the exception description to the logtext
	fi
	LogMe "${MODULE_LOGFILE}" "${logtext}"

	# (NB: Don't pass the args (the reason) on to the other programs)
	if ModuleIsRunning "${MODULE_NAME}" ; then
		Program_stop
	fi
	Program_start
}

#############
# Program_start() -- launch a single program
#############
function Program_start () { 	
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	ConfigExistsOrDie $callingFrom 
		
	local errCount=$(Program_val count) # this does "source xyz.config"
	if [[ $errCount -gt 0 ]] ; then
		printf "%s\n" "${callingFrom}: *** ${MODULE_NAME} config has ${errCount} error${s}. Can't start."
		Program_val verbose
		printf "%s\n" "${callingFrom}: *** ${MODULE_NAME} config has ${errCount} error${s}. Can't start."
		exit
	fi
	
	ModuleIsRunning $MODULE_NAME && { 
		printf "%s\n" "${callingFrom}: *** An instance of this program is already running. Can't start another one." ; 
		exit ;
	}
	
	# Check that there's a .screenrc in the program dir
	if [[ ! -e "${MODULE_SCREENRC_FILE}" ]] ; then
		printf "%s\n" "${callingFrom}: *** Missing ${DEFAULT_MODULE_SCREENRC_FILE} file. Can't start ${MODULE_NAME}."
		exit
	fi

	RotateLogFiles "${MODULE_SCREEN_LOGFILE}" # N.B. only the screen log file gets rolled over with every restart
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: Launching eLoop ..."

	# start up a virtual terminal, detached (-d -m), with logging (-L)
	# also pass the arguments ($@) to the _launch script (to allow for overriding config file parameters)
	screen -c "${MODULE_SCREENRC_FILE}" -L -d -m -S "${MODULE_NAME}" "${ELOOP_LAUNCHER_PATH}" "${DEFAULT_PROGRAM_RUN_KEY}" $@
	exit
	
}


#############
# Program__launch() -- launch the esound loop script
# This function is not generally to be invoked from the command line. 
# It's meant to be launched via the 'screen' command in Program_start.
# It's set apart in order to keep all its own complicated functions quarantined from
# scripts that are separate from the general program functions. (I.e., this is a tangle!) 
#############
function Program__launch () {
	source "${SCRIPTS_DIR}/program-eLoop.sh"
	eLoopLaunch $@
}

#############
# Program_status() -- Report the status of a single program
# If no argument equal to "verbose" is present , then print a minimal one-line status report
# If an argument is present and is equal to "verbose", then print out detailed info
#############
function Program_status () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	ConfigExistsOrDie $callingFrom

	if [[ $# -gt 1 ]] ; then
		printf "Usage: ${callingFrom} [ default | verbose | code | program | dir | sox | butt ] \n"
		exit
	fi
		
	# first check the arg count (and save the first arg)
	case $# in
	0)
		local mode="default"
		;;
	1)
		local mode="$1"
		;;
	*)
		Program_status help
		;;
	esac

	# check that the arg is one we know about (if not, call this function with a bunch of bad args. Clever, eh? :)
	if  [[ $mode != "help" ]] && [[ $mode != "h" ]] && \
		[[ $mode != "verbose" ]] && [[ $mode != "v" ]] && \
		[[ $mode != "default" ]] && [[ $mode != "code" ]] && \
		[[ $mode != "program" ]] && [[ $mode != "dir" ]] && \
		[[ $mode != "sox" ]] && [[ $mode != "butt" ]] ; then
		"${PSM_PROJECT}/${callingFrom}" help
		exit
	fi
	
#	local s isare errCount=$(Program_val count)
#
#	if [[ $errCount -gt 0 ]] ; then
#		[[ $errCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
#		printf "${callingFrom}: *** ${MODULE_NAME} has ${errCount} error${s} that must be fixed before program will start.\n"
#		printf "${callingFrom}: *** To troubleshoot the error, try '${MODULE_NAME}/val verbose'.\n"
#	fi	
	
	# Now collect the status of the various pieces of the program
	local programStatus=$(ModuleIsRunning ${MODULE_NAME} && printf "${DEFAULT_MODULE_STATE_UP}" || printf "${DEFAULT_MODULE_STATE_DOWN}")
	local buttStatus=$(StatButt)
	local soxStatus=$(StatSoXPlayer)
	local dirStatus=$(StatSourceDirs)
	
	

	case "$mode" in
	
	"help" | "h")
		printf "%s\n" "Usage: ${callingFrom} [ verbose | code | program | dir | sox | butt ]"
		exit
		;;
	
	"default") # a simple one-liner
		printf "%${DEFAULT_FORMAT_SPACING}s : program %4s | dir %4s | SoX %s | butt %4s %s\n" "${MODULE_NAME}" "${programStatus}" "${dirStatus}" "${soxStatus}" "${buttStatus}" "${valError}" 
		return
		;;
		
	"program")
		printf "%s\n" "$programStatus"
		return
		;;
		
	"dir")
		printf "%s\n" "$dirStatus"
		return
		;;
		
	"sox")
		printf "%s\n" "$soxStatus"
		return
		;;
		
	"butt")
		printf "%s\n" "${buttStatus}"
		return
		;;

	"code")
		local code=""
		# BYTE 1:  programStatus
		if [[ $programStatus = "${DEFAULT_MODULE_STATE_UP}" ]] ; then
			code="${code}0"
		elif [[ $programStatus = "${DEFAULT_MODULE_STATE_DOWN}" ]] ; then
			code="${code}1"
		else 
			code="${code}X" # (unrecognized)
		fi
		
		# BYTE 2:  dirStatus
		if [[ $dirStatus = "${DEFAULT_MODULE_STATE_OK}" ]] ; then
			code="${code}0"
		elif [[ $dirStatus = "${DEFAULT_MODULE_STATE_EMPTY}" ]] ; then
			code="${code}1"
		elif [[ $dirStatus = "${DEFAULT_MODULE_STATE_MISSING}" ]] ; then
			code="${code}2"
		else 
			code="${code}X" # (unrecognized)
		fi

		# BYTE 3:  soxStatus
		if [[ $soxStatus = "${DEFAULT_MODULE_STATE_UP}" ]] ; then
			code="${code}0"
		elif [[ $soxStatus = "${DEFAULT_MODULE_STATE_DOWN}" ]] ; then
			code="${code}1"
		elif [[ $soxStatus = "${DEFAULT_MODULE_STATE_HUNG}" ]] ; then
			code="${code}2"
		else 
			code="${code}X" # (unrecognized)
		fi

		# BYTE 4:  buttStatus
		if [[ $buttStatus = "${DEFAULT_MODULE_STATE_UP}" ]] ; then
			code="${code}0"
		elif [[ $buttStatus = "${DEFAULT_MODULE_STATE_DOWN}" ]] ; then
			code="${code}1"
		elif [[ $buttStatus = "${DEFAULT_MODULE_STATE_HUNG}" ]] ; then
			code="${code}3"
		else 
			code="${code}X" # (unrecognized)
		fi
		
		printf "%s\n" "$code"
		return
		;;
		
	"verbose" | "v") # do the default, plus print a chunk from the logfile
		Program_status default
		local pgid=( $(ps -O ppid,pgid | grep "${ELOOP_LAUNCHER_PATH} ${DEFAULT_PROGRAM_RUN_KEY}" | grep -v "grep" | awk '{print $3}') )
		local nInstances=${#pgid[@]}
		if [[ $nInstances -gt 1 ]] ; then
			printf "${callingFrom}: ***WARNING: running multiple (${nInstances}) instances of esound: "
			for p in "${pgid[@]}" ; do
				printf "${p} "
			done
			printf "\n"
		fi
		if [[ -e "${MODULE_SCREEN_LOGFILE}" ]] ; then
			printf "\n"
			printf "@@@ Last ${DEFAULT_LOGFILE_TAIL} lines of logfile '${MODULE_SCREEN_LOGFILE}':\n"  | sed -e "s/^/   || /"
			tail -"${DEFAULT_LOGFILE_TAIL}" "${MODULE_SCREEN_LOGFILE}" | fold -w 100 -s | sed -e "s/^/   || /" 
			printf "@@@ end of logfile @@@\n" | sed -e "s/^/   || /"
			printf "\n"
		else 
			printf "%s\n" "${callingFrom}: *** No logfile."
		fi
		return
		;;

	*)	# unexpected error! unrecognized mode! should never get here!
		;; 
	esac
}



#############
# Program_stop() -- Stop (kill) this program
#############
function Program_stop () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	ConfigExistsOrDie $callingFrom

	# Get the group ID of the parent process...
	# (We save the id's into an array, in case there are multiple instances running.)
	local pgid=( $(ps -O ppid,pgid | grep "${ELOOP_LAUNCHER_PATH} ${DEFAULT_PROGRAM_RUN_KEY}" | grep -v "grep" | awk '{print $3}') )
	
	if [[ ${#pgid[@]} -eq 0 ]] ; then
		printf "%s\n" "${callingFrom}: Program not running. Nothing to stop." 
	else
	for p in "${pgid[@]}" ; do
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Killing process group ${p}..."
		kill -TERM -"$p"  # (the '-' before $p kills all descendants)
	done
	fi

	# terminate any stray BUTT procs
	if BUTTisRunning ; then
		BUTTStop
	fi
}


#############
# Program_attach() -- re-attach a virtual screen to the Terminal
#############
function Program_attach ()  {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	ConfigExistsOrDie $callingFrom
	
	socket="$(screen -list | grep "\.${MODULE_NAME}" | awk '{print $1}')"
	if [[ "$socket" = "" ]] ; then
		printf "%s\n" "${callingFrom}: No virtual terminal found."
		exit
	fi
	
	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Attaching screen (type \"CTRL-a CTRL-d\" to exit)..."
	sleep 2 # pause a moment to give the user a chance to read the previous message...

	# reattach the screen
	screen -r "$socket"

	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Done."
}

#############
# Program_ver() -- print the version number of the installed scripts
#############
function Program_ver () { 
	Version $@
}

#############
# Program_golan() -- log in to another pod on the LAN
#############
function Program_golan ()  {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if [[ $# -eq 1 ]] ; then
			ssh "${1}.local"
	else
			printf "Usage: ${callingFrom} PODNAME\n"
			return
	fi
}


#############
# Program_clean() -- clean out the tmp files and other cruft from this program
#############
function Program_clean ()  {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	SourceConfig $callingFrom

	if ModuleIsRunning "${MODULE_NAME}"  ; then
		printf "%s\n" "${callingFrom}: Can't clean this module while it's running. Run '${MODULE_NAME}/stop', then try again."
		exit
	fi
	
	cd "${MODULE_DIR}" &&  LogBreak "${MODULE_LOGFILE}" "${callingFrom}"
	local mBytesBefore="$(du -cm | tail -1 | awk '{print $1}')" # get the current disk space usage
	
	printf "${callingFrom}: "
	DeleteAllExcept 8675309 ${MODULE_PROTECTED_FILES[*]} 	# delete all but the protected files
	find "${MODULE_WORKSPACE_DIR}" -depth 1 -type f -delete  # clean out the workspace
	/bin/rm -f ${BUTT_HUNG_ERR_PATHS[@]} 	# remove the butt error files, if they exist

	local mBytesAfter="$(du -cm | tail -1 | awk '{print $1}')" # get the current disk space usage
	local mBytesCleaned=`expr $mBytesBefore - $mBytesAfter`
	printf "%s\n" "${callingFrom}: Deleted ${mBytesCleaned} MB. (OK)"
	RotateLogFiles "${MODULE_SCREEN_LOGFILE}"  "${MODULE_EXCEPTION_LOGFILE}"
	# (don't rotate "${MODULE_LOGFILE}" here; UpdateModule's RecreateModule will take care of that)

	UpdateModule nogit && LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_TYPE} module cleaned. (OK)"
}


#############
# Program_buttctl() -- stopping and starting BUTT (Broadcast Using This Tool)
# First arg: 	the command
# Optional second arg(s):	words describing the reason (usually to address an exception detected by the pod).
#############
function Program_buttctl ()  {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	ConfigExistsOrDie $callingFrom

	if [[ $# -lt 1 ]] ; then
		cat << EOT
Usage: ${callingFrom} COMMAND
COMMAND may be one of:
	start     starts BUTT
	stop      stops BUTT if it's running
	restart   stops BUTT if it's running, then start it
	status    prints BUTT's status (running, not-running, etc.)
EOT
		exit
	fi
	local cmd="$1"
	shift
	
	if [[ $# -ge 1 ]] ; then # if there are more args
		local reason="$@"
		LogException "${callingFrom} ${cmd}: ${reason}"
		logtext="${logtext} (Exception: ${reason})" # append the exception description to the logtext
	fi

	
	case "$cmd" in
	"stop")
		BUTTStop
		;; 
	"start")
		BUTTStart
		;; 
	"restart")
		BUTTRestart 
		;; 
	"status")
		BUTTStatus
		;; 
	*)
		printf "%s\n" "${callingFrom}: *** Unknown BUTT command '$cmd'"
		;;
	esac
}


# Generate a report: is BUTT running or not?
function BUTTStatus () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	local buttStatus=$(StatButt)
	local s isare nProcs=$( BUTTnProcesses )
	[[ $nProcs -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	printf "%s\n" "${callingFrom}: $nProcs instance${s} of BUTT ${isare} ${buttStatus}"
}


#############
# BUTTStop() - Stop BUTT if it's running
#############
function BUTTStop () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	/bin/rm -f ${BUTT_HUNG_ERR_PATHS[@]} 	# remove the butt error files, if they exist

	if ! BUTTisRunning ; then
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: BUTT isn't running. Nothing to stop."
		return
	fi
	
	local s isare nProcs=$( BUTTnProcesses )
	[[ $nProcs -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Killing ${nProcs} instance${s} of butt..."
	kill $( BUTTgetProcesses )

}

#############
# BUTTStart() - Start up BUTT (if it's not already running)
# "open" flags:
#	-g	Do not bring the application to the foreground.
#	-n	Open a new instance of the application(s) even if one is already running.
#	-a	launch the named app
#############
function BUTTStart () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	SourceConfig $callingFrom
	# if SHOUTCAST parameter wasn't set in the config, then there's nothing to do
	if [[ ! -n ${SHOUTCAST} ]]; then 
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: SHOUTCAST not set in config. BUTT not launched."
		return
	fi	

	if BUTTisRunning ; then
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: Another instance of BUTT is running. Can't start a new one."
		return
	fi

	CreateButtConfig

	LogMe "${MODULE_LOGFILE}"  "${callingFrom}: starting BUTT..."
	local buttConfigPath="${MODULE_DIR}/${DEFAULT_PROGRAM_BUTTCONFIG_PATH}"
	/bin/rm -f ${BUTT_HUNG_ERR_PATHS[@]} 	# remove the butt error files, if they exist

	if [[ -f "${buttConfigPath}" ]] ; then 
		"${BIN_BUTT}" -c "${buttConfigPath}" &
	else
		LogMe "${MODULE_LOGFILE}"  "${callingFrom}: *** Can't start BUTT. No config file (${buttConfigPath})."
		exit
	fi

}

# Create a new BUTT config file tailored to the current program
# Assumes that SourceConfig has already been called
# Does 
function CreateButtConfig () {
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	local templatePath="${TEMPLATE_DIR}/butt_config_template.txt"
	local buttConfigPath="${MODULE_DIR}/${DEFAULT_PROGRAM_BUTTCONFIG_PATH}"
	local buttSongNameFile="${MODULE_WORKSPACE_DIR}/buttSongName.txt"
	local channelCount=$( wc -w <<< "${CHANNELS}" | sed -e 's/^[[:space:]]*//' )	# http://stackoverflow.com/a/15108286/5011245

	# check that the input audio device exists
	local buttDeviceNum="$($BIN_AUDIODEV $AUDIO_DEVICE -I)"
	if [[ $buttDeviceNum -lt 0 ]] ; then
		printf "%s\n" "${thisFunction}: Input device '${AUDIO_DEVICE}' not found. Maybe a typo in your config file? Devices available to BUTT are:"
		${BIN_AUDIODEV} -l
		exit
	fi

	# save the song name 
	printf "%s\n" "${LOCATION} [${SIGNAL_TYPE} x${GAMMA}]" > "${buttSongNameFile}"	# e.g., "Mbarara, Uganda [seismic x3600]"

	#############
	# Edit the config template.
	# (Note the use below of an alternate regexp delimiter in place of "/" -- makes editing file paths MUCH easier!)
	# CAVEAT: But this means you must careful not to use that special char in any of the config param values!
	printf "${thisFunction}: preparing the BUTT config file..."
	ed -s << EOT $templatePath 
%g+^#%#%#%+d
1a
# Edited $(date "+${DEFAULT_TIME_FORMAT}") by ${thisFunction}()
.
%s+__BUTT_SERVER__+${MODULE_NAME}+g
%s+__BUTT_SRV_ENT__+${MODULE_NAME}+g
%s+__BUTT_ICY__+PSM channel ${MODULE_NAME}+g
%s+__BUTT_ICY_ENT__+PSM channel ${MODULE_NAME}+g
%s+__BUTT_SONG_PATH__+${buttSongNameFile}+g
%s+__BUTT_DEVICE__+${buttDeviceNum}+g
%s+__BUTT_DEVICE_NAME__+${AUDIO_DEVICE}+g
%s+__BUTT_CHANNEL__+${channelCount}+g
%s+__BUTT_SRV_HOST__+${SHOUTCAST_HOST_IP}+g
%s+__BUTT_SRV_PORT__+${SHOUTCAST_PORT}+g
%s+__BUTT_SRV_PASSWORD__+${SHOUTCAST_PASSWORD}+g
w $buttConfigPath
q
EOT
	#############
	printf "OK.\n"
}

# ParseShoutcastParameter() 
# Extracts from the SHOUTCAST parameter (a string of the form PASSWORD@HOST:PORT) the 
# three global variables: SHOUTCAST_HOST_IP, SHOUTCAST_PASSWORD, SHOUTCAST_PORT
# If parsing fails, it prints the parsing errors. It returns an error code equal to the number of parsing errors found.
function ParseShoutcastParameter () {	
	if [[ ! -n ${SHOUTCAST} ]]; then # if SHOUTCAST not set, then ignore it
		unset -v SHOUTCAST_HOST_IP SHOUTCAST_PORT SHOUTCAST_PASSWORD
		return 0
	fi
	local creds="${SHOUTCAST}"
	SHOUTCAST_PASSWORD="$(echo $creds | cut -f1 -d@)"
	SHOUTCAST_HOST_IP="$(echo $creds | cut -f2 -d@ | cut -f1 -d:)"
	SHOUTCAST_PORT="$(echo $creds | cut -f2 -d@ | cut -f2 -d:)"
	local errCount=0
	local atCount=$(grep -o "@" <<< "$creds" | wc -l)
	local colonCount=$(grep -o ":" <<< "$creds" | wc -l)
	local failReason=""
	[[ $colonCount -eq 0 ]] && failReason="${failReason} Missing \":\"."
	[[ $colonCount -gt 1 ]] && failReason="${failReason} Too many \":\"s."
	[[ $atCount -eq 0 ]] && failReason="${failReason} Missing \"@\"."
	[[ $atCount -gt 1 ]] && failReason="${failReason} Too many \"@\"s."
	if [[ "${SHOUTCAST_PASSWORD}" = "${creds}" ]] || [[ "${SHOUTCAST_PASSWORD}" = "" ]] ; then
		failReason="${failReason} Can't deduce password."
		(( errCount++ ))
	fi
	if [[ "${SHOUTCAST_HOST_IP}" = "${creds}" ]] || [[ "${SHOUTCAST_HOST_IP}" = "" ]] ; then
		failReason="${failReason} Can't deduce hostname."
		(( errCount++ ))
	fi
	if [[ "${SHOUTCAST_PORT}" = "${creds}" ]] || [[ "${SHOUTCAST_PORT}" = "" ]]; then
		failReason="${failReason} Can't deduce port."
		(( errCount++ ))
	fi
	if [[ "${SHOUTCAST_HOST_IP}" = "${SHOUTCAST_PORT}" ]] ; then
		failReason="${failReason} Can't deduce either hostname or port."
		(( errCount++ ))
	fi
	
	if [[ $errCount -gt 0 ]] ; then
		printf "%s\n" "${failReason}"
		unset -v SHOUTCAST_HOST_IP SHOUTCAST_PORT SHOUTCAST_PASSWORD # derived params are useless, so dump 'em
	fi

	return $errCount
}

#############
# BUTTRestart() - Stop BUTT if it's running, then restart it
#############
function BUTTRestart () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: restart"
	BUTTisRunning && BUTTStop
	BUTTStart
}

#############
# BUTTgetProcesses() - Get the number of BUTT processes running under this program
#############
function BUTTgetProcesses () {
	# Look for any instances of BUTT that are associated with this program.
	# When BUTT is launched, it's associated with its config file in the program directory. 
	# The path to the config file shows up in the output of "ps", which allows us to grep for 
	# that particular instance of BUTT. For example, if it's launched in the directory 
	# "/Users/jtb/project/ffc1a", then ps shows this:
	#	82494 ??         0:00.59 .../butt -c /Users/jtb/project/ffc1a/buttConfig.txt
	# Grepping for the string "butt -c /Users/jtb/project/ffc1a/buttConfig.txt"
	# will find all the instances of BUTT that need to` be killed.
	# N.B. #1: Since BUTT is launched on the command line via the "open" command, 
	# it's not associated with any terminal; we therefore can't search for it by terminal.
	# N.B. #2: In the unlikely case of more than one instance of BUTT running in the program directory, 
	# ps catches them all, and this script kills 'em. Tidy. Sweet.
	local seekString="buttj -c ${MODULE_DIR}/${DEFAULT_PROGRAM_BUTTCONFIG_PATH}"
	local pid=$(ps -x | grep "${seekString}" | grep -v "grep" | awk '{print $1}')
	printf "$pid"
}

#############
# BUTTnProcesses() - get the number of BUTT processes running under this program
#############
function BUTTnProcesses () {
	local nProcs=$( wc -w <<< "$( BUTTgetProcesses )" | sed -e 's/^[[:space:]]*//')	# tally up the pids (for <<< see http://stackoverflow.com/a/15108286/5011245)
	printf "$nProcs"
}


#############
# BUTTisRunning() - returns 1 if BUTT is running, 0 if it is not.
# !!! THIS WORKS ONLY WHEN THE CURRENT WORKING DIRECTORY IS A PROGRAM DIRECTORY !!!
#############
function BUTTisRunning () {
	local nProcs=$( BUTTnProcesses)
	if [[ $nProcs -eq 0 ]] ; then # no processes
		return 1	# "FALSE" (like an exit code of 1)
	else
		return 0	# "TRUE" (like an exit code of 0)
	fi
}

#############
# Program__update() -- update the module scripts
#############
function Program__update () { 
	local callingFrom=$(TracePath)
	if [[ $1 != $DEFAULT_PROGRAM_RUN_KEY ]] ; then
		printf "%s\n" "${callingFrom}: Please run pod/update instead."
		exit
	fi
	UpdateModule nogit $@
}

function Program_val () {
	ValidateModule $@ # i.e., shared.sh::ValidateModule
}

#
# PrepareParams() -- tidy up config params and construct any needed ones from those in the config
# If $1="verbose" then printf any (non-fatal) warnings
# Returns the number of (non-fatal) warnings
function PrepareParams () {
	local warningCount=0

	ParseShoutcastParameter	# set the params that depend on SHOUTCAST
			
	[[ ! -n "${VERBOSITY}" ]] && VERBOSITY=0
	[[ ! -n "${DBGAIN}" ]] && DBGAIN=0
	[[ ! -n "${SOXFILTER}" ]] && SOXFILTER=""
	[[ ! -n "${SOXCOMPAND}" ]] && SOXCOMPAND=""

	# Construct the DESCRIPTION string
	DESCRIPTION="${LOCATION} (${SIGNAL_TYPE}) | ${STATION}"
	[[ "${NETWORK}" != "" ]]  && DESCRIPTION+=" (${NETWORK})"

	# sanitize the AUDIO_DEVICE by removing a leading zero, if present, from the digit portion
	# E.g.: 	AUDIO_DEVICE=psm008   sanitizes to  AUDIO_DEVICE=psm8
	if [[ $AUDIO_DEVICE =~ ^${DEFAULT_AUDIO_DEVICE_PREFIX}0+([0-9])$ ]] ; then   
		AUDIO_DEVICE="${DEFAULT_AUDIO_DEVICE_PREFIX}${BASH_REMATCH[1]}"
	fi
	# if AUDIO_DEVICE isn't set, then make it the default device
	[[ ! -n "${AUDIO_DEVICE}" ]] && AUDIO_DEVICE="default" # default audio output device
	
	if [[ -n "${SHOUTCAST}" ]] && [[ $AUDIO_DEVICE = "default" ]] ; then 
		(( warningCount++ ))
		[[ "$1" = "verbose" ]] && printf "\n*** Warning: AUDIO_DEVICE='default'. SHOUTCAST settings will be ignored. Streaming disabled.\n"
		unset -v SHOUTCAST SHOUTCAST_HOST_IP SHOUTCAST_PASSWORD SHOUTCAST_PORT
	else
		:
	fi
	return $warningCount
}

#
# ValidateParamCallback() -- Validate a single parameter
# This is called by shared.sh::ValidateModule.
# It examines a single variable (name supplied in $1) to see if its value(s) ($@) is/are appropriate.
# If there is an error, print a single line that describes the error.
# If no error, print nothing. 
# shared.sh::ValidateModule takes care of tallying up errors and printing the result.
# For cleaner output formatting, please do not include colons in the error message.
function ValidateParamCallback () {
	local variableName="$1"
	shift ; local args=($@)  # $args is everything after $1
	local reply=""
	local nChannels=$(local arr=($CHANNELS); echo ${#arr[@]}) # (convert string to array, then count elements)
	local nSourceDirs=$(local arr=($SOURCEDIRS); echo ${#arr[@]}) # (convert string to array, then count elements)

	case "$variableName" in
	CHANNELS)
		# We only know how to process 1 or 2 audio channels; any other channel count is an error.
		if [[ $nChannels -lt 1 ]]  || [[ $nChannels -gt 2 ]] ; then 
			reply="Bad CHANNEL count (${nChannels}) ***"
		fi
		;; 
		
	SOURCEDIRS)
		# We only know how to process 1 or 2 audio channels; any other channel count is an error.
		if [[ $nSourceDirs -lt 1 ]]  || [[ $nSourceDirs -gt 2 ]] ; then 
			reply="*** Bad SOURCEDIRS count (${nSourceDirs}). Must be either 1 or 2. ***"
		elif [[ $nSourceDirs -gt $nChannels ]] ; then
			reply="*** More SOURCEDIRS (${nSourceDirs}) than CHANNELS (${nChannels})"
		else 
			# verify that the directories actually exist
			for d in "${sourceDirs[@]}" ; do
				if [[ ! -d "${d}" ]] ; then
					reply="*** Can't find source directory ${d}"
					break
				fi
			done
		fi
		;;

	AUDIO_DEVICE)
		# non-default audio device name must be of the form psmNN where NN is a (leading zero formatted) integer
		if [[ ${AUDIO_DEVICE} != "default" ]] ; then
			# (nifty regex test -- see https://stackoverflow.com/a/21112809/5011245)
			if [[ ! $AUDIO_DEVICE =~ ^${DEFAULT_AUDIO_DEVICE_PREFIX}([0-9]+)$ ]] ; then   
				reply="Bad AUDIO_DEVICE format (${AUDIO_DEVICE}). Must be of the form '${DEFAULT_AUDIO_DEVICE_PREFIX}##' ***"
			else
				local devnum=${BASH_REMATCH[1]} # (the captured digits from the above regex test)
				if [[ $devnum -lt 1 ]] || [[ $devnum -gt $DEFAULT_AUDIO_DEVICE_CHANNELS ]] ; then
					reply="Bad audio device number (${devnum}) in AUDIO_DEVICE (${AUDIO_DEVICE}). Must be in the range [1,${DEFAULT_AUDIO_DEVICE_CHANNELS}].  ***"
				else 
					# check that the requested input audio device exists
					# DEBUG FIXME: maybe "audiodev default" should be allowed?
					local buttDeviceNum="$($BIN_AUDIODEV $AUDIO_DEVICE -I)"
					if [[ $buttDeviceNum -lt 0 ]] ; then
						reply="Input device '${AUDIO_DEVICE}' not found. Maybe a typo in your config file? Devices available to BUTT are:\n"
						reply="${reply}\nFIXME COMING SOON... 'audiodev -l' should show a simple list; 'audiodev -t' should show the table"
					#	${BIN_AUDIODEV} -l
					#	exit
					fi				
				fi
			fi
		fi
		;;

	SHOUTCAST)
		reply="$(ParseShoutcastParameter)"
		;;
		
	*)
		;;
	esac
	
	printf "${reply}"
}

#####
# StatSourceDirs -- prints the status of the program's data directories
# results: 
#	MISSING	directory can't be found (likely problem: remote dir wasn't mounted)
#	EMPTY 	directory exists but contains no files
#	OK		directory exists and is non-empty
#####
function StatSourceDirs ()  {
	SourceConfig "StatSourceDirs"
	
	# convert SOURCEDIRS to an array
	local sourceDirs=($SOURCEDIRS)

	# verify that all the directories actually exist
	# if any one is missing, print DEFAULT_MODULE_STATE_MISSING
	for d in "${sourceDirs[@]}" ; do
		if [[ ! -d "${d}" ]] ; then
			printf "${DEFAULT_MODULE_STATE_MISSING}"
			return
		fi
	done
		
	# Count up the number of files in each dir
	# If any dir is empty, print DEFAULT_MODULE_STATE_EMPTY
	for d in "${sourceDirs[@]}" ; do
		local nFiles=$(/bin/ls "${d}" | wc -l | sed -e "s| ||g")
		if [[ $nFiles -eq 0 ]] ; then # (NB: /bin/ls by default doesn't show "." and ".." )
			printf "${DEFAULT_MODULE_STATE_EMPTY}"
			return
		fi
	done
	
	# if we survived this far, all must be well
	printf "${DEFAULT_MODULE_STATE_OK}"
	return
}

#####
# StatSoXPlayer -- prints the status of the SoX 'play' operation. 
# If SoX is not playing then it is "DOWN".
# If SoX is playing and its elapsed time is <= the duration of the audio file (plus a few seconds' allowance) then it is "UP"
# If SoX is playing but its elapsed time is longer than the duration of the audio file (plus some allowance), then it is "HUNG"
# Example: Audio file is 100 secs long. Allow 5 secs for buffers to fill.
# If it's been playing for 99 secs, it's UP. 100 ==> UP. 101 ==> UP . ... 105 => UP. 106 ==> HUNG
# NB In normal operation, SoX goes UP and DOWN all the time (it may be DOWN for a sec or two between program plays). 
# What is abnormal is if it's been running unreasonably longer than the audio file it's supposedly playing.
#####
function StatSoXPlayer ()  {
	local search=$(ps -xo lstart,command | grep "${PROGRAM_SOX_SIGNATURE}".*${MODULE_NAME} | grep -v "grep")
	# e.g.:  Fri Sep 21 14:29:42 2018     /Users/jtb/project/psm/bin/sox --buffer 100000 -q 00test.7.post.aif -t coreaudio default
	if [[ "${search}" = "" ]] ; then  # if sox is not running, assign it a negative run duration
		printf "${DEFAULT_MODULE_STATE_DOWN}"
		return
	fi
	local startTime=$(echo $search | sed -e "s| *${PROGRAM_SOX_SIGNATURE}.*||")
	local startSecs=$(date -jf "%c" "$startTime" +"%s")
	local ageSeconds=$(( $(date +"%s") - $startSecs))
	SourceConfig "StatSoXPlayer" #  get HOURS and GAMMA
	audioDuration=$(( $HOURS * 3600 / $GAMMA )) # the duration in secs of the audio file
	local acceptableAge=$(( $audioDuration + $DEFAULT_PROGRAM_SOX_PLAY_ALLOWANCE ))	# allow some slop for audio buffers to fill
	if [[ $ageSeconds -le $acceptableAge ]] ; then
		printf "${DEFAULT_MODULE_STATE_UP}"
	else 
		printf "${DEFAULT_MODULE_STATE_HUNG}"
	fi
	return
}


#####
# StatButt -- prints the status of butt
# If butt is running without errors then it is "UP".
# If butt is not running then it is "DOWN".
# If butt is running but logged any errors then it is "HUNG".
#####
function StatButt ()  {

	SourceConfig "StatButt"

	# if the SHOUTCAST parameter wasn't set in the config file, then declare BUTT as "OFF"
	if [[ -z ${SHOUTCAST+x} ]] ; then 
		printf "${DEFAULT_MODULE_STATE_INACTIVE}"
		return
	fi

	# if it's not running at all...
	if ! BUTTisRunning $MODULE_DIR ; then 
		printf "${DEFAULT_MODULE_STATE_DOWN}"
		return
	fi
	
	# if butt logged any "hung" errors...
	local hungError
	for hungError in "${BUTT_HUNG_ERR_PATHS[@]}" ; do
		if [[ -f "${hungError}" ]] ; then  # if the file exists
			printf "${DEFAULT_MODULE_STATE_HUNG}"
			return
		fi
	done

	# no errors found, so it must be up
	printf "${DEFAULT_MODULE_STATE_UP}"
	
	return		
}

#############
# Program_peek() -- view  screen.log
#############
function Program_peek () { 
	Module_peek $@
}
