#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
# Various functions used by the radio/* scripts

MODULE_TYPE="radio"
MODULE_NAME="radio" # $( basename $( cd $(dirname ${BASH_SOURCE[1]}); pwd) )" # the directory that contained the shell command that brought us here
COMMANDS=(update clean status start stop restart val help attach)
MODULE_FUNCTION_PATH="${BASH_SOURCE[0]}" # path to this module function file
MODULE_FUNCTION_FILENAME="$( basename ${BASH_SOURCE[0]} )" # handy name of this module function file

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # the containing dir of this script (http://stackoverflow.com/a/246128/5011245)
source "${SCRIPTS_DIR}/DEFAULTS.sh" # get the default params
source "${SCRIPTS_DIR}/shared.sh" # get the common functions and set some crucial variables
MODULE_COMMANDS=( attach clean help peek restart start status stop val _update ) # commands to be installed from templates
MODULE_SPECIAL_DIRS=(${DEFAULT_RADIO_SOURCE_DIR} ) # special dirs (unique to this module) that must be present 
MODULE_SPECIAL_FILES=(${DEFAULT_MODULE_SCREENRC_FILE})	# special files to be installed from templates
MODULE_REQUIRED_BINARIES=(${BIN_RADIOMIX} ${BIN_SOX})

SSHFS_BIN=$(which sshfs) # for testing the presence an executable required by radiomix

DEFAULT_RADIOMIX_ARGS="-c ${MODULE_DIR}/radiomix.conf" # may be overridden in config by setting  RADIOMIX_ARGS

# List of parameters to expect in the config file
CONFIG_REQUIRED_VARIABLES=()
CONFIG_REQUIRED_ARRAYS=()
CONFIG_OPTIONAL_VARIABLES=(RADIOMIX_ARGS)

#############
# Test for the presence of sshfs, which is required by the radiomix app
function Test_sshfs () {
	if [[ ! -n ${SSHFS_BIN} ]] ; then
		printf "sshfs is missing. Please download and install FUSE and sshfs from https://osxfuse.github.io/"
	fi
}
function DieWithout_sshfs () {
	local err=$(Test_sshfs)
	[[ -n ${err} ]] && (printf "*** ${err}\n\n" ; exit 1 )
}
#############

#############
# Radio_help() -- print a help message
#############
function Radio_help () { 
	cat << EOT
A PSM ${MODULE_TYPE} module may be controlled from the command line.
Commands are bash scripts; to invoke a command, type its path. 
For example, to generate this help message from the PSM project directory (${PSM_PROJECT}), 
type '${MODULE_TYPE}/help'. Some commands take optional arguments.

Command     Action
=======     ============
   clean    Delete any temporary or stray files from the radio directory and roll over the logfiles.
            (Reminder: do not add your own files to the radio directory, as 'clean' will
            delete them.)
   help     Print this help info.
   restart  Restart the radio.
   start    Launch the radio (in a virtual terminal (screen(1)), and return to the shell.
   status   Print the status of the radio (stopped, running, etc.)
            Usage: radio/status [verbose]
               verbose -- also print the last few lines of the log file
   stop     Stop the radio.
   val      Validate the config file and print the result.
            Usage: radio/val [count | verbose | oneliner]
                  count    :  print the number of errors
                  verbose  :  print a detailed report
                  oneliner :  print a one-line summary [default]

EOT
	DieWithout_sshfs
}

#
# PrepareParams() -- tidy up config params and construct any needed ones from those in the config
# If $1="verbose" then printf any (non-fatal) warnings
# Returns the number of (non-fatal) warnings
function PrepareParams () {
	local warningCount=0

	return $warningCount
}

#############
# Radio_status() -- report the status of the instance(s?) of radiomix
# If no second argument equal to "verbose" is present , then print a minimal one-line status report
# If a second argument is present and is equal to "verbose", then print out detailed info
#############
function Radio_status () { 
	local caller="${MODULE_NAME}/$(basename "${BASH_SOURCE[1]}")"; # nice name of the script that called this function 

	SourceConfig $caller
	OverrideDefaults

	local s isare errCount=$(Radio_val count)
	if [[ $errCount -gt 0 ]] ; then
		[[ $errCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		printf "${caller}: *** ${MODULE_NAME} has ${errCount} error${s}. Results will be suspect!\n"
		printf "${caller}: *** To troubleshoot the error, try '${MODULE_NAME}/val verbose'.\n"
	fi	

	local radiomix=$(basename ${BIN_RADIOMIX})
	local pid=( $(ps |grep "${radiomix} ${RADIOMIX_ARGS}" | grep -v grep ) ) 
	printf "Module status: "
	if [[ $pid = "" ]] ; then
		printf "${DEFAULT_MODULE_STATE_DOWN}\n"
	else
		printf "${DEFAULT_MODULE_STATE_UP} (pid ${pid}). "
		# get the child processes
		local children=( $(pgrep -P $pid) )
		local nChildren=${#children[@]}
		local s
		[[ $nChildren -eq 1 ]] && s="" || s="s" # pluralizer
		if [[ $nChildren -eq 0 ]] ; then
			printf "No child processes.\n"
		else
			printf "There are $nChildren child processes running:\n"
			ps ${children[@]} | sed -e "s/^/     /"
		fi
	fi
	DieWithout_sshfs
}

# isModuleRunning() -- returns true (0) if this module running; false (1) otherwise
# (Required by shared.sh)
function isModuleRunning () {
	local radiomix=$(basename ${BIN_RADIOMIX})
	local pid=( $(ps |grep "${radiomix} ${RADIOMIX_ARGS}" | grep -v grep ) ) 
	if [[ $pid = "" ]] ; then
		return 1 # i.e., false or error
	else
		return 0 # i.e., true, or no error
	fi
}



#############
# Radio_restart() -- restart the module if it's running; start it if not
#############
function Radio_restart () {
	DieWithout_sshfs
	local caller="${MODULE_NAME}/$(basename "${BASH_SOURCE[1]}")"; # useful name of the script that called this function 

	ConfigExistsOrDie $caller

	if isModuleRunning ; then
		Radio_stop
	fi
	Radio_start
}

#############
# Radio_stop() -- kill the module if it's running 
#############
function Radio_stop () { 
	DieWithout_sshfs
	local caller="${MODULE_TYPE}/$(basename "${BASH_SOURCE[1]}")"; # nice name of the script that called this function 

	SourceConfig $caller  # get RADIOMIX_ARGS
	OverrideDefaults

	local errCount=$(Radio_val count)
	if [[ $errCount -gt 0 ]] ; then
		[[ $errCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		printf "${caller}: *** ${MODULE_NAME} has ${errCount} error${s}. Results will be suspect!\n"
		printf "${caller}: *** To troubleshoot the error, try '${MODULE_NAME}/val verbose'.\n"
	fi	

	local radiomix=$(basename ${BIN_RADIOMIX})
	local pid=( $(ps |grep "${radiomix} ${RADIOMIX_ARGS}" | grep -v grep ) ) 
	if [[ $pid = "" ]] ; then
		printf "Module not running. Nothing to stop.\n"
		return
	else 
		# get the child processes
		local children=( $(pgrep -P $pid) )
		local nChildren=${#children[@]}
		local es
		[[ $nChildren -eq 1 ]] && es="" || es="es" # pluralizer

		printf "Module is running with ${nChildren} child process${es}. Now killing...\n"
		for proc in "${children[@]}" ; do
			kill -TERM $proc
		done
		kill $pid
		Radio_status
		LogMe "${MODULE_LOGFILE}" "${caller}: stopped module and ${nChildren} child process${es}."
		printf "Module is stopped. (OK)\n"
	fi

}




#############
# Radio_start() -- 
#############
function Radio_start () { 
	DieWithout_sshfs
	local caller="${MODULE_TYPE}/$(basename "${BASH_SOURCE[1]}")"; # nice name of the script that called this function 

	SourceConfig $caller  # get RADIOMIX_ARGS
	OverrideDefaults

	local errCount=$(Radio_val count)
	if [[ $errCount -gt 0 ]] ; then
		[[ $errCount -eq 1 ]] && local s="" || local s="s" # pluralizer
		printf "${caller}: *** The module has ${errCount} error${s}. Can't start!\n"
		printf "${caller}: *** To troubleshoot the error, try '${MODULE_TYPE}/val verbose'.\n"
		exit
	fi

	if isModuleRunning ; then
		printf "*** ${caller}: Module '${MODULE_NAME}' is already running. Can't start.\n"
		exit
	fi

	# Check that there's a .screenrc file
	if [[ ! -e "${MODULE_SCREENRC_FILE}" ]] ; then
		printf "${caller}: *** Missing .screenrc file. Can't start ${MODULE_NAME}.\n"
		exit
	fi

	RotateLogFiles "${MODULE_SCREEN_LOGFILE}" # N.B. only the screen log file gets rolled over with every restart
	LogMe "${MODULE_LOGFILE}" "${caller}: Launching radiomix ..."

	# start up a virtual terminal, detached (-d -m), with logging (-L)
	screen -c "${MODULE_SCREENRC_FILE}" -L -d -m -S "${MODULE_TYPE}" "${BIN_RADIOMIX}" ${RADIOMIX_ARGS} # (N.B. Do NOT put quotes around RADIOMIX_ARGS!)

	# "${BIN_RADIOMIX}" ${RADIOMIX_ARGS} # (N.B. Do not put quotes around RADIOMIX_ARGS!)

	printf "How to do some some common tasks:\n"
	printf "%30s: %s\n" "Get ${MODULE_TYPE} status" "${MODULE_TYPE}/status"
	printf "%30s: %s\n" "Stop the ${MODULE_TYPE} " "${MODULE_TYPE}/stop"
	printf "%30s: %s\n" "Reattach process to the terminal" "${MODULE_TYPE}/attach"
}




#############
# Radio_attach() -- re-attach a virtual screen to the Terminal
#############
function Radio_attach ()  {
	AttachModule $@
}

#############
# Radio__update() -- update the module scripts
#############
function Radio__update () { 
	local callingFrom=$(TracePath)
	if [[ $1 != $DEFAULT_PROGRAM_RUN_KEY ]] ; then
		printf "${callingFrom}: Please run pod/update instead.\n"
		exit
	fi
	UpdateModule nogit $@
}



#############
# Clean() -- clean up log files,  queued batchfiles, and the workspace in each program
#############
function Radio_clean () { 
	local caller="${MODULE_TYPE}/$(basename "${BASH_SOURCE[1]}")"; # nice name of the script that called this function 

	ConfigExistsOrDie $caller

	local errCount=$(Radio_val count)
	if [[ $errCount -gt 0 ]] ; then
		[[ $errCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		printf "${caller}: *** ${MODULE_NAME} has ${errCount} error${s}. Results will be suspect!\n"
		printf "${caller}: *** To troubleshoot the error, try '${MODULE_NAME}/val verbose'.\n"
	fi	

	if isModuleRunning ; then
		printf "${caller}: Can't clean this module while it's running. Run '${MODULE_NAME}/stop', then try again.\n"
		exit
	fi

	# get the current disk space usage
	local mBytesBefore="$(du -cm | tail -1 | awk '{print $1}')"
	
	find ./workspace -depth 1 -type f -delete
	
	RotateLogFiles "${MODULE_LOGFILE}" "${MODULE_SCREEN_LOGFILE}" "${MODULE_EXCEPTION_LOGFILE}"

	mBytesAfter="$(du -cm | tail -1 | awk '{print $1}')"
	mBytesCleaned=`expr $mBytesBefore - $mBytesAfter`

	printf "${caller}: Deleted ${mBytesCleaned} MB. (OK)\n"
}


function Radio_val () {
		ValidateModule $@ # i.e., shared.sh::ValidateModule
}

# a special callback that prints any additional errors unique to this module
function SpecialValidate {
	Test_sshfs
}



#
# ValidateParamCallback() -- Validate a single parameter
# This is called by shared.sh::ValidateModule.
# It examines a single variable (name supplied in $1) to see if its value ($@) is appropriate
# If there is an error, print a single line that describes the error.
# If no error, print nothing. 
# shared.sh::ValidateModule takes care of tallying up errors and printing the result.
# For cleaner output formatting, do not include colons in the error message.
function ValidateParamCallback () {
	local variableName="$1"
	shift ; local args=($@)  # $args is everything after $1
	local reply=""
	case "$variableName" in
	BLOOP)
		if [[ "${#args[@]}" -gt 0 ]] ; then
			reply="*** too many arguments (${args[@]})"
		fi
		;; 
	RADIOMIX_ARGS)
		reply="sdf"
		;; 
	*)
		;;
	esac
	
	printf "${reply}"
}



#############
# Radio_peek() -- view  screen.log
#############
function Radio_peek () { 
	Module_peek $@
}


# OverrideDefaults() -- some optional config params can override default values
# If these params weren't set in the config, set them now to their default values
function OverrideDefaults () {
 	[[ ! -n ${RADIOMIX_ARGS} ]] && RADIOMIX_ARGS=${DEFAULT_RADIOMIX_ARGS} 
}

