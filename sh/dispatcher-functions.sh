#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
# Various functions used by the dispatcher/* scripts

MODULE_TYPE="dispatcher"
MODULE_NAME="dispatcher"
MODULE_FUNCTION_PATH="${BASH_SOURCE[0]}" # path to this module function file
MODULE_FUNCTION_FILENAME="$( basename ${BASH_SOURCE[0]} )" # handy name of this module function file
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # the containing dir of this script (http://stackoverflow.com/a/246128/5011245)
source "${SCRIPTS_DIR}/DEFAULTS.sh" # get the default params
source "${SCRIPTS_DIR}/shared.sh" # get the common functions and set some crucial variables
MODULE_COMMANDS=(attach clean help housecall login logtail peek pod restart sendbatch start status stop val ver _pushstart _pushstop _pushup _update)
MODULE_SPECIAL_DIRS=() # special dirs (unique to this module) that must be present 
MODULE_SPECIAL_FILES=(${DEFAULT_MODULE_SCREENRC_FILE})	# special files to be installed from templates
MODULE_USER_PREFERENCES=("${MODULE_TYPE}.conf") # user-configurable files (to be saved with the "backup" command)
MODULE_PROTECTED_FILES=("${MODULE_USER_PREFERENCES[@]}" "${MODULE_COMMANDS[@]}" "${MODULE_SPECIAL_FILES[@]}" "demo-${MODULE_TYPE}.conf") # files that will never be clean'ed

SENDBATCH_LOGFILE="${MODULE_LOG_DIR}/${DEFAULT_DISPATCHER_SENDBATCH_LOGFILE}" 
DISPATCHER_SCREEN_ID="Dispatcher" # id for the virtual terminal
DISPMON_SCREEN_ID="Dispmon" # id for the virtual terminal
DISPMON_HOUSECALL_MAX_DURATION=180 # maximum allowed duration (secs) of a 'housecall' connection to a pod; anything longer implies a hung connection

# List of parameters to expect in the config file
CONFIG_REQUIRED_VARIABLES=()
CONFIG_REQUIRED_ARRAYS=(PODS)
CONFIG_OPTIONAL_VARIABLES=(DISPATCHER_WAIT)
CONFIG_DEPRECATED_VARIABLES=(DISPATCHER_SFTP)

# The return string from ExecPodSilently contains fields separated by this character
EPS_FIELDSEPARATOR='¶' 

SSH_COMMAND="ssh -o ConnectTimeout=${DEFAULT_SSH_CONNECTTIMEOUT} -o NumberOfPasswordPrompts=0"


#############
# Dispatcher_help() -- print a help message
#############
function Dispatcher_help () { 

	cat << EOT
A PSM ${MODULE_TYPE} module may be controlled from the command line.
Commands are bash scripts; to invoke a command, type its path. 
For example, to generate this help message from the PSM project directory (${PSM_PROJECT}), 
type '${MODULE_TYPE}/help'. Some commands take optional arguments.

   Command     What it does
   =======     ============
   attach      Attach this ${MODULE_TYPE} to the current terminal. 
               (To re-detach from the ${MODULE_TYPE} and return to the shell, type "CTRL-a CTRL-d".)
   clean       Delete any temporary or stray files from the ${MODULE_TYPE} directory and roll over the logfiles.
               (Reminder: be careful adding your own files to this directory, as 'clean' will
               delete them, unless their names end in ".txt".)
   help        Print this help info.
   housecall   Send a single 'housecall' command to each of the pods
   login       Open an ssh login session with a pod.
               Usage: ${MODULE_NAME}/login PODNAME
   logtail     Print just the tail end of the ${MODULE_TYPE} logfile.
   pod         Send a command to a pod.
               Usage: ${MODULE_NAME}/pod PODNAME COMMAND
   restart     Stop, then start the ${MODULE_TYPE} module
   sendbatch   Send a single 'sendbatch' command to each of the pods
   start       Start the ${MODULE_TYPE} module
   status      Print the run status of this ${MODULE_TYPE} module
               Usage: ${MODULE_NAME}/status [verbose | code]
                   verbose : also print the last few lines of the log file
                      code : print only a coded summary of the status
   stop        Stop this ${MODULE_TYPE} module (if it is running)
   val         Validate the ${MODULE_TYPE} module's config file and print the result.
               Usage: ${MODULE_NAME}/val [count | verbose | oneliner]
                  count    :  print the number of errors
                  verbose  :  print a detailed report
                  oneliner :  print a one-line summary [default]
   ver         Show the codebase version number
               Usage: ${MODULE_NAME}/ver [full]
                      full : print the full (major and minor) version numbers
               (By default, just print the major version number.)

Note: There are no commands to start and stop all the pods at once.
      To start or stop an individual pod, you must command it directly -- e.g., "dispatcher/pod FOOPOD restart".
EOT
}

#############
# Dispatcher__update() -- update the module scripts
#############
function Dispatcher__update () { 
	local callingFrom=$(TracePath)
	if [[ $1 != $DEFAULT_PROGRAM_RUN_KEY ]] ; then
		printf "${callingFrom}: Please run pod/update instead.\n"
		exit
	fi
	UpdateModule nogit $@
}


#############
# Dispatcher__pushstop() -- Stop all the downstream pods
#############
function Dispatcher__pushstop () { 
	local callingFrom=$(TracePath)	
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	# To discourage invoking this function directly, require a "key" to run it. 
	# When pod/pushup invokes this function, it sends the correct key.
	if [[ $1 != $DEFAULT_PROGRAM_RUN_KEY ]] ; then
		printf "${callingFrom}: Please run pod/pushstop instead.\n"
		exit
	fi
	
	SourceConfig $callingFrom #get the PODS array
			
	local n s isare
	n=${#PODS[@]}
	if [[ $n -lt 1 ]] ; then 
		printf "${callingFrom}: No pods defined for this dispatcher. Nothing more to do.\n"
		exit
	fi
	
	local execReturnValue remotePodName 
	local pod  # NB $pod might be "USER@HOST:PORT"; $remotePodName is `hostname -s` (e.g., "hera")
	for pod in "${PODS[@]}" ; do
		execReturnValue="$( ExecPodSilently $pod _name )" # get the name of the remote pod
		if ! ExtractReturnCode "${execReturnValue}" ; then
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** Unable to connect to remote pod at ${pod}."
		else 
			remotePodName="$(ExtractOutputString "$execReturnValue" )"
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Stopping ${remotePodName} (${pod})..."
			execReturnValue="$( ExecPodSilently $pod stop )" 
			local remoteResult="$(ExtractOutputString "$execReturnValue" )"
			if ! ExtractReturnCode "${execReturnValue}" ; then
				LogMe "${MODULE_LOGFILE}" "${callingFrom}: Unable to stop ${remotePodName} (${pod})."
			else
				LogMe "${MODULE_LOGFILE}" "${callingFrom}: Stopped ${remotePodName} (${pod})."
			fi
		fi
	done

}

#############
# Dispatcher__pushstart() -- Start all the downstream pods
#############
function Dispatcher__pushstart () { 
	local callingFrom=$(TracePath)	
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	# To discourage invoking this function directly, require a "key" to run it. 
	# When pod/pushup invokes this function, it sends the correct key.
	if [[ $1 != $DEFAULT_PROGRAM_RUN_KEY ]] ; then
		printf "${callingFrom}: Please run pod/pushstart instead.\n"
		exit
	fi
	
	SourceConfig $callingFrom #get the PODS array
			
	local n s isare
	n=${#PODS[@]}
	if [[ $n -lt 1 ]] ; then 
		printf "${callingFrom}: No pods defined for this dispatcher. Nothing more to do.\n"
		exit
	fi
	
	local execReturnValue remotePodName 
	local pod  # NB $pod might be "USER@HOST:PORT"; $remotePodName is `hostname -s` (e.g., "hera")
	for pod in "${PODS[@]}" ; do
		execReturnValue="$( ExecPodSilently $pod _name )" # get the name of the remote pod
		if ! ExtractReturnCode "${execReturnValue}" ; then
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** Unable to connect to remote pod at ${pod}."
		else 
			remotePodName="$(ExtractOutputString "$execReturnValue" )"
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Resuming  ${remotePodName} (${pod})..."
			execReturnValue="$( ExecPodSilently $pod start )" 
			local remoteResult="$(ExtractOutputString "$execReturnValue" )"
			if ! ExtractReturnCode "${execReturnValue}" ; then
				LogMe "${MODULE_LOGFILE}" "${callingFrom}: Unable to start ${remotePodName} (${pod})."
			else
				LogMe "${MODULE_LOGFILE}" "${callingFrom}: Started ${remotePodName} (${pod})."
			fi
		fi
	done

}


#############
# Dispatcher__pushup() -- Push updates to downstream pods (must be invoked as a pod command)
#############
function Dispatcher__pushup () { 
	local callingFrom=$(TracePath)	
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	# To discourage invoking this function directly, require a "key" to run it. 
	# When pod/pushup invokes this function, it sends the correct key.
	if [[ $1 != $DEFAULT_PROGRAM_RUN_KEY ]] ; then
		printf "${callingFrom}: Please run pod/pushup instead.\n"
		exit
	fi
	
	SourceConfig $callingFrom #get the PODS array
			
	local n s isare
	n=${#PODS[@]}
	if [[ $n -lt 1 ]] ; then 
		printf "${callingFrom}: No pods defined for this dispatcher. Nothing more to do.\n"
		exit
	fi
	
	local localVer=$(Version full) # get the full version no. of this pod (should be the current master version, since pod/pushup already did "git pull...")

	local execReturnValue remotePodName remotePodVer  
	local pod  # NB $pod might be "USER@HOST:PORT"; $remotePodName is `hostname -s` (e.g., "hera")
	for pod in "${PODS[@]}" ; do
		execReturnValue="$( ExecPodSilently $pod _name )" # get the name of the remote pod
		if ! ExtractReturnCode "${execReturnValue}" ; then
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** Unable to connect to remote pod at ${pod}."
		else 
			# get the version number of the remote pod to see if it needs updating	
			# (NB: version numbers consist of three dot-separated integer fields)
			remotePodName="$(ExtractOutputString "$execReturnValue" )"
			execReturnValue="$( ExecPodSilently $pod 'ver full' )" # get the version of the remote pod (e.g., "151.20190328.192143")
			remotePodVer="$(ExtractOutputString "$execReturnValue" )"
			if [[ $(CompareVersions ${localVer} ${remotePodVer}) -eq 0 ]] ; then
				LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${remotePodName} (${pod}) is up to date (version ${localVer})."
			elif [[ $(CompareVersions ${localVer} ${remotePodVer}) -lt 0 ]] ; then
				LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** SYNC ERROR: ${remotePodName} (${pod}) version (${remotePodVer}) is ahead of dispatcher version (${localVer})."
			else
				LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${remotePodName} (${pod}) is out of date (version ${remotePodVer}). Pushing update..."
				execReturnValue="$( ExecPodSilently $pod pushup )" # issue pod/update on the remote pod
				if ExtractReturnCode "${execReturnValue}" ; then
					local newVersion="$(ExtractOutputString "$( ExecPodSilently ${pod} 'ver full')" )"
					LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${remotePodName} (${pod}) is up to date (version ${localVer})."
					printf "\n"
				else
					LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** Failed remote update to ${remotePodName} (${pod})."
					LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** Please log in to remote pod and do a manual update."
					printf "\n"
				fi
			fi
		fi
	done

	LogMe "${MODULE_LOGFILE}" "${callingFrom}: Done."
}


#############
# Dispatcher_sendbatch() -- Send a "sendbatch" message to every pod under the dispatcher's control
#############
function Dispatcher_sendbatch () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	SourceConfig $callingFrom # get the PODS array

	# first invoke sendbatch on the dispatcher's own pod
	printf "%${DEFAULT_FORMAT_SPACING}s : " "localhost (pod '$(PodName)')"
	"${PSM_PROJECT}/${DEFAULT_POD_DIR}/sendbatch" 
	
	# now contact each of the dispatcher's downstream pods
	local pod logString=""
	for pod in "${PODS[@]}" ; do
		printf "%${DEFAULT_FORMAT_SPACING}s : " $pod
		if [[ "${pod}" != "localhost" ]] ; then		# skip "localhost", since we already did it (above)
			local returnValue="$( ExecPodSilently ${pod} sendbatch )"
			local returnString=$(ExtractOutputString "${returnValue}")
			if ExtractReturnCode "${returnValue}" ; then
				printf "Sendbatch OK"
			else
				printf " %s " "${returnString}"
			fi
		fi
		printf "\n"

	done
}


#############
# Dispatcher_housecall() -- Send a "housecall" message to every pod under the dispatcher's control
# With the optional "send" argument, also invokes sendbatch to deliver factoids, etc to a remote web server
#############
function Dispatcher_housecall () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	local doSend="NO"
	if [[ $# -eq 1 ]] ; then
		if [[ $1 = "send" ]] ; then 
			doSend="YES" 
		else
			printf "${callingFrom}: *** Unrecognized argument '$1'. Goodbye. \n"
			return
		fi
	fi

	SourceConfig $callingFrom # get the PODS array

	MakeFactoids  # generate the dispatcher factoids 

	# first invoke housecall on the dispatcher's own pod
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: Requesting housecall from myself."
	printf "%${DEFAULT_FORMAT_SPACING}s : " "localhost (pod '$(PodName)')"
	"${PSM_PROJECT}/${DEFAULT_POD_DIR}/housecall" $@ > /dev/null 
	[[ $? = 0 ]] && printf "housecall OK\n" || printf "*** (housecall error)\n"

	# with the optional "send" argument, deliver factoids, etc. to the webserver
	if [[ $doSend = "YES" ]] ; then
		Dispatcher_sendbatch
		local errCode=$?
		if [[ $errCode -ne 0 ]] ; then  # initial sendbatch failed
			printf "${callingFrom}: *** Unable to send dispatcher's own files (err=${errCode}). Will continue polling the other pods anyway.\n"
		fi
	fi


	
	# now contact each of the dispatcher's downstream pods
	local pod logString=""
	for pod in "${PODS[@]}" ; do
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Requesting housecall from \'${pod}\'."
		printf "%${DEFAULT_FORMAT_SPACING}s : " $pod
		if [[ "${pod}" != "localhost" ]] ; then		# skip "localhost", since we already did it (above)
			local returnValue="$( ExecPodSilently ${pod} "housecall $@" )"
			local returnCode=$(ExtractReturnCode "${returnValue}")
			local returnString=$(ExtractOutputString "${returnValue}")
			if ExtractReturnCode "${returnValue}" ; then
				printf " housecall OK"
			else
				printf " %s " "${returnString}"
			fi
		fi
		printf "\n"

	done
}


#############
# Dispatcher_clean() -- clean up log files (does not affect pods)
#############
function Dispatcher_clean () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	if ModuleIsRunning dispatcher ; then
		printf "${callingFrom}: ${MODULE_NAME} is running. Can't clean. Run '${MODULE_NAME}/stop', then try again.\n"
		exit
	fi

	cd "${MODULE_DIR}" &&  LogBreak "${MODULE_LOGFILE}" "${callingFrom}"
	local mBytesBefore="$(du -cm | tail -1 | awk '{print $1}')" # get the current disk space usage

	printf "${callingFrom}: "
	DeleteAllExcept 8675309 ${MODULE_PROTECTED_FILES[*]} 	# delete all but the protected files
	
	local mBytesAfter="$(du -cm | tail -1 | awk '{print $1}')" # get the current disk space usage
	local mBytesCleaned=`expr $mBytesBefore - $mBytesAfter`
	printf "${callingFrom}: Deleted ${mBytesCleaned} MB. (OK)\n"
	RotateLogFiles "${MODULE_SCREEN_LOGFILE}" "${MODULE_EXCEPTION_LOGFILE}" "${SENDBATCH_LOGFILE}"
	# (don't rotate "${MODULE_LOGFILE}" here; UpdateModule's RecreateModule will take care of that)
	UpdateModule nogit && LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_TYPE} module cleaned. (OK)"

}




#############
# Dispatcher_status() -- dispatcher status report
#############
function Dispatcher_status () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	# dispatcher report	
	printf "Status of Dispatcher: $(ModuleIsRunning dispatcher && printf ${DEFAULT_MODULE_STATE_UP} ||  printf ${DEFAULT_MODULE_STATE_DOWN})\n"


	# get an array of pids for instances of dispmon
	pids=( $(ps  | grep "${DISPMON_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $1}' ) )
	if [[ -z "${pids+x}" ]] ; then 
		printf "${callingFrom}: dispmon is ${DEFAULT_MODULE_STATE_DOWN}.\n" 
	else 
		((nPids=${#pids[@]}, maxIndex=nPids - 1)) 	# get the number of procesess
		[[ $nPids -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		printf "${callingFrom}: ${nPids} instance${s} of dispmon (${pids[*]})\n"
	fi

	SourceConfig $callingFrom # get the PODS array
		
	local s isare nPods=${#PODS[@]}
	if [[ $nPods -lt 1 ]] ; then 
		printf "${callingFrom}: No pods defined for this dispatcher. Nothing to do.\n"
		exit
	fi
	
	[[ $nPods -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer

	printf "${callingFrom}: This dispatcher controls $nPods pod${s}:\n"
	local pod
	for pod in  ${PODS[@]} ; do
		printf "%${DEFAULT_FORMAT_SPACING}s%s\n" $pod
	done
	
	local myhost=$(hostname)
	local indentSpace="   "
	for pod in "${PODS[@]}" ; do
		printf "\n======== status of pod '${pod}' ========\n"
		local tStart=$(date +%s);
		if [[ "${pod}" = "localhost" ]] ; then		# if it's "localhost" then just start up a shell...
			if [[ ! -z ${PSM_PROJECT+x} ]] ; then 	# Check that environment variable PSM_PROJECT is set
				/bin/bash -lc "${PSM_PROJECT}/pod/status $2" # invoke bash as login shell
			else
				printf "*** Environment variable PSM_PROJECT is not set on '${pod}'. Can't stat." | sed -e "s/^/${indentSpace}/"
			fi
		else 	# ...if it's not "localhost" , then use ssh 
			# POD is generally of the form USER@HOST:PORT, so parse it to get USER@HOST and PORT:
			local uhp=(${pod//:/ })  # break it at the ":"
			local userAtHost="${uhp[0]}" # USER@HOST
			[[ ${#uhp[@]} -eq 2 ]] && local sshPort="-p ${uhp[1]}" || local sshPort=""
		
			local remoteEnvCheck=$($SSH_COMMAND "${userAtHost}" "${sshPort}" "source ~/.profile; if [[ -z \${PSM_PROJECT+x} ]] ; then echo 'N' ;  else echo 'YES' ; fi")
			if [[ "${remoteEnvCheck}" = "YES" ]] ; then
				$SSH_COMMAND "${userAtHost}" "${sshPort}" "source ~/.profile; \${PSM_PROJECT}/pod/status" | sed -e "s/^/${indentSpace}/"
			else
				printf "*** Pod '${pod}' failed environment test (can't find PSM_PROJECT). Can't stat." | sed -e "s/^/${indentSpace}/"
			fi
		fi
		printf "===============\n"
		local tEnd=$(date +%s);
		local tDelta=$(echo $tEnd - $tStart | bc )
		printf "Pod response time: ${tDelta} sec\n"

		printf "\n"

	done


}


#############
# Dispatcher_ver() -- print the version number of the installed scripts
#############
function Dispatcher_ver () { 
	Version $@
}


#############
# Dispatcher_val() -- Validate the config file (and the pods) and produce a report 
#############
function Dispatcher_val () {
	ValidateModule $@ # i.e., shared.sh::ValidateModule
}

#
# PrepareParams() -- tidy up config params and construct any needed ones from those in the config
# If $1="verbose" then printf any (non-fatal) warnings
# Returns the number of (non-fatal) warnings
function PrepareParams () {
	local warningCount=0

	[[ ! -n "${DISPATCHER_WAIT}" ]] && DISPATCHER_WAIT=${DEFAULT_DISPATCHER_WAIT}


	return $warningCount
}


#############
# ValidateChildrenCallback() -- add a list of invalid child modules to the global $_OFFENSIVE_DESCENDANTS
#############
function ValidateChildrenCallback () {
	local pods=(${PODS[*]})
	if [[ ${#pods[@]} -gt 0 ]] ; then
		for pod in "${pods[@]}" ; do
			local errCount=0
			if [[ "${pod}" = "localhost" ]] ; then		# if it's "localhost" then just start up a shell...
				errCount=$(/bin/bash -lc "${PSM_PROJECT}/pod/val count") 	# invoke bash as login shell
				if [[ $errCount -gt 0 ]] ; then		# if there was an error
					_OFFENSIVE_DESCENDANTS=("${_OFFENSIVE_DESCENDANTS[@]}" "$pod")  #save the name of the offending pod
				fi
			else 	# ...if it's not "localhost" , then use ssh 
				# POD is generally of the form USER@HOST:PORT, so parse it to get USER@HOST and PORT:
				local uhp=(${pod//:/ })  # break it at the ":"
				local userAtHost="${uhp[0]}" # USER@HOST
				[[ ${#uhp[@]} -eq 2 ]] && local sshPort="-p ${uhp[1]}" || local sshPort=""			
				local remoteEnvCheck=$($SSH_COMMAND "${userAtHost}" "${sshPort}" "if [[ -z \\${PSM_PROJECT+x} ]] ; then echo 'NO' ;  else echo 'YES' ; fi")
				if [[ "${remoteEnvCheck}" = "YES" ]] ; then
					errCount=$($SSH_COMMAND "${userAtHost}" "${sshPort}" "source ~/.profile ; \\${PSM_PROJECT}/pod/val count")
					if [[ $errCount -gt 0 ]] ; then		# if there was an error
						_OFFENSIVE_DESCENDANTS=("${_OFFENSIVE_DESCENDANTS[@]}" "$pod")  #save the name of the offending pod
					fi
				else 
					_OFFENSIVE_DESCENDANTS=("${_OFFENSIVE_DESCENDANTS[@]}" "$pod")  #save the name of the offending pod
				fi
			fi
		done
	fi
}
		
		
function ValidateParamCallback() {
	local reply=""
	printf "$reply"
}


#############
# ExecPodSilently() -- issue a pod command to a remote server (or localhost) and receive a result code and message
#	Usage: ExecPodSilently USER@HOST COMMAND
#
# When the remode command executes, it will typically print some text to stdout. This is captured by
# ssh. This output string and the ssh return code are packed together into a one-line string, 
# which contains one or more fields, which are separated by EPS_FIELDSEPARATOR:
# 	field 0	 : the return code (usually just 0 or 1)
# 	field 1  : first line of the text printed to stdout by the executing function
# 	field 2  : second line...
#	...
# EPS_FIELDSEPARATOR is thus both a stand-in for the newline character (which may appear in the command's output string), 
# AND it separates the return code from the output string itself. 
#############
function ExecPodSilently () {
	local thisFunction="${FUNCNAME[0]}";	# the name of this function 
	
	local cmdOutput="" # the string that the remote command printed to stdout
	local resultCode=0 # (NB declare this "local" here so as not to mess up $? later)

	if [[ "$#" -ne 2 ]] ; then
		resultCode=1
		cmdOutput="${thisFunction}: Got $# arguments; expected two: USER@HOST[:PORT] (or 'localhost') and a single command."
		printf "${resultCode}${EPS_FIELDSEPARATOR}${cmdOutput}"
		return
	fi
	local pod="$1" cmd=""
	
	if [[ "${pod}" = "localhost" ]] ; then
		cmd="source ~/.profile; ${PSM_PROJECT}/pod/logstring Received request \'pod/${2}\' from dispatcher on \'$(PodName)\' ; ${PSM_PROJECT}/pod/${2} 2>&1" 
		cmdOutput=$(eval $cmd)
		resultCode=$?
	else
		# exec the ssh command and save the results
		
		# POD is generally of the form USER@HOST:PORT, so parse it to get USER@HOST and PORT:
		local uhp=(${pod//:/ })  # break it at the ":"
		local userAtHost="${uhp[0]}" # USER@HOST
		[[ ${#uhp[@]} -eq 2 ]] && local sshPort="-p ${uhp[1]}" || local sshPort=""
		
		cmd="source ~/.profile; \$PSM_PROJECT/pod/logstring Received request \'pod/${2}\' from dispatcher on \'$(PodName)\' ; \$PSM_PROJECT/pod/${2}" # on a machine other than localhost, "\$PSM_PROJECT" gets expanded in the ssh call
		cmdOutput="$($SSH_COMMAND "${userAtHost}" "${sshPort}" "${cmd}" 2>&1 )"
		resultCode=$?
	fi
	
	# strip newlines from cmdOutput (necessary for the subsequent packing/unpacking of the string to work)
	cmdOutput="$( echo "${cmdOutput}" | tr '\n' $EPS_FIELDSEPARATOR)" # replace newlines with a special char
	
	# pack the cmdOutput and resultCode into a string, separated by EPS_FIELDSEPARATOR
	local encodedResult="${resultCode}${EPS_FIELDSEPARATOR}${cmdOutput}"
	
	printf "${encodedResult}"
}


# extract and **print** the return message from a call to ExecPodSilently
function ExtractOutputString() {
	# Split the string into an array of string fields, taking EPS_FIELDSEPARATOR as the field delimiter 
	# stringFields[0] : result code
	# stringFields[1] : LogMe stuff...
	# stringFields[2] : the command output...
	local stringFields=()
	IFS="${EPS_FIELDSEPARATOR}" read -ra stringFields <<< "$@"
	local lastField=$(( ${#stringFields[@]} - 1 ))   # the last field is always the interesting one
	printf "${stringFields[${lastField}]}"
}

# extract and **return** the result code from a call to ExecPodSilently
function ExtractReturnCode() {
	# unpack the parts of the encodedResult (include restoring a coded newline character)
	local resultCode=`echo "$@" | cut -d ${EPS_FIELDSEPARATOR} -f 1`
	return "$resultCode"
}



#############
# ExecPodInteractive() -- issue a pod command to a remote server (or localhost), and interact if necessary
# Usage: ExecPodInteractive USER@HOST [COMMAND]
# If COMMAND is not specified, then an interactive login shell (or ssh session) starts
#############
function ExecPodInteractive () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if [[ $# -lt 1 ]] ; then
		printf "Usage: ${callingFrom} POD [COMMAND]\n"
		exit
	fi
	local targetPod="$1"
	shift # pop the arg off the argument list
	local cmd="$1"
	shift
	local cmdArgs="$@"

	SourceConfig $callingFrom #get the PODS array
	
	# loop through the list of pods for the one requested
	local pod
	local myHost=$(hostname) 						# e.g., "helios.local"
	# The targetPod can be of two forms: either "USER@MACHINE.DOMAIN" or the shorthand "MACHINE"
	# E.g., "jtb@eos.local" or simply "eos"
	for pod in "${PODS[@]}" ; do
		local machine=$(MachineName "${pod}")  # if pod="jtb@inanna.local", machine="inanna"
		if [[  ( "${targetPod}" = "${pod}" || "${targetPod}" = "${machine}" )  # (e.g., if config says "jtb@eos.local" and target="jtb@eos.local" or "eos"
			|| ( "${targetPod}" = "$(hostname -s)" && "${pod}" = "localhost" ) # (e.g., if target="helios" and config says "localhost")
			]] ; then		# we found the requested pod
			printf "\n============== Opening connection to ${pod} =============\n"
			if [[ "${pod}" = "localhost" ]] ; then		# if it's "localhost" then just start up a shell...
				if [[ $cmd != "" ]] ; then # if a real command was provided...
					cmd="source ~/.profile; ${cmd} ${cmdArgs}";
					LogMe "${MODULE_LOGFILE}" "${callingFrom}: Sending request  \'pod/${cmd} ${cmdArgs}\' to '${pod}'..."
					/bin/bash ${PSM_PROJECT}/pod/logstring "Received request \'pod/${cmd} ${cmdArgs}\' from dispatcher on \'$(PodName)\'"
					/bin/bash ${PSM_PROJECT}/pod/${cmd} ${cmdArgs}  # then execute it in a shell (!! leave unquoted !!)
				else						# if no command provided...
					LogMe "${MODULE_LOGFILE}" "${callingFrom}: logging in to \'${machine}\'..."
					/bin/bash ${PSM_PROJECT}/pod/logstring "Remote login from dispatcher on '$(PodName)'"
					/bin/bash -l   			# ... then start a login shell
				fi
			else 	# ...if it's not "localhost" , then use ssh 
				# POD is generally of the form USER@HOST:PORT, so parse it to get USER@HOST and PORT:
				local uhp=(${pod//:/ })  # break it at the ":"
				local userAtHost="${uhp[0]}" # USER@HOST
				[[ ${#uhp[@]} -eq 2 ]] && local sshPort="-p ${uhp[1]}" || local sshPort=""

				if [[ $cmd != "" ]] ; then 		# if a command was provided...
					LogMe "${MODULE_LOGFILE}" "${callingFrom}: Sending request \'pod/${cmd} ${cmdArgs}\' to '${machine}'..." # log it to the dispatcher's logfile
					local remoteCmd="source ~/.profile; \$PSM_PROJECT/pod/logstring Received request \'${cmd} ${cmdArgs}\' from dispatcher on \'$(PodName)\' ; \$PSM_PROJECT/pod/${cmd} ${cmdArgs}" 
					$SSH_COMMAND "${userAtHost}" "${sshPort}" "${remoteCmd}"  #... then execute it via ssh
					# N.B. on a machine other than localhost, "\$PSM_PROJECT" gets expanded in the ssh call
				else
					LogMe "${MODULE_LOGFILE}" "${callingFrom}: logging in to '${machine}'..."
					local remoteCmd="source ~/.profile; \$PSM_PROJECT/pod/logstring Remote login from dispatcher on \'$(PodName)\'" 
					$SSH_COMMAND "${userAtHost}" "${sshPort}" "${remoteCmd}"  # log it to the pod's logfile
					$SSH_COMMAND "${userAtHost}" "${sshPort}"   # just log in
				fi				
			fi
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Exited '${pod}' and returned to '$(PodName)'."
			printf "=========== Closed connection to ${pod}. Back home at $(PodName) ================\n\n"
			return
		fi
	done

	# if we didn't find the targetPod in the loop...
	printf "${callingFrom}: Pod '${targetPod}' is not associated with this dispatcher. Nothing to do. Please check the dispatcher config file.\n"
}

#############
# Dispatcher_pod() -- Login to one of the pods and issue a pod command
# (A tidy wrapper for ExecPodInteractive) 
#############
function Dispatcher_pod () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if [[ $# -lt 1 ]] ; then
		printf "${callingFrom}: Usage: ${callingFrom} TARGETPOD [COMMAND...]\n"
		exit
	fi
	local pod="$1"
	shift
	ExecPodInteractive "${pod}" "$@"
}


#############
# Dispatcher_login() -- Login to one of the pods
# (A tidy wrapper for ExecPodInteractive) 
#############
function Dispatcher_login () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if [[ $# -ne 1 ]] ; then
		printf "${callingFrom}: Usage: Login TARGETPOD\n"
		exit
	fi
	local pod="$1"
	ExecPodInteractive "${pod}"
}





#############
# DispatcherStart() -- Launch the dispatcher to send messages to the pods  
# Periodically executes a "houseall" command to each pod,
# in order to check up on each pod and upload to the webserver any of their queued files 
#############
function Dispatcher_start () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	SourceConfig $callingFrom

	# Check that the module isn't already running
	ModuleIsRunning $MODULE_TYPE && { 
		printf "${callingFrom}: *** $MODULE_TYPE module is already running. Can't start.\n" ; 
		exit ;
	}	

	# Check that there's a .screenrc in the program dir
	if [[ ! -e "${MODULE_SCREENRC_FILE}" ]] ; then
		printf "${callingFrom}: *** Missing ${DEFAULT_MODULE_SCREENRC_FILE} file. Can't start ${MODULE_NAME}.\n"
		exit
	fi

	RotateLogFiles "${MODULE_LOGFILE}" "${MODULE_SCREEN_LOGFILE}" "${MODULE_EXCEPTION_LOGFILE}" "${SENDBATCH_LOGFILE}"
	LogBreak "${MODULE_LOGFILE}" "${callingFrom}"

	MakeFactoids  # generate the dispatcher factoids 
	"${PSM_PROJECT}/${DEFAULT_POD_DIR}/housecall" send # invoke pod's sendbatch to deliver the factoids
	local errCode=$?
	if [[ $errCode -ne 0 ]] ; then  # initial sendbatch failed
		printf "${callingFrom}: *** Unable to send files (err=${errCode}). Can't continue. Goodbye.\n"
		exit $errCode
	fi

	printf "Launching Dispatcher in a virtual terminal.\n"
	printf "How to do some some common tasks:\n"
	printf "%30s: %s\n" "Reattach to the terminal" "dispatcher/attach"
	printf "%30s: %s\n" "Detach from the terminal" "^a ^d"
	printf "%30s: %s\n" "Get Dispatcher status" "dispatcher/status"
	printf "%30s: %s\n" "Stop the Dispatcher" "dispatcher/stop"

	# We launch the dispatcher in a sub-shell. 
	# It's a roundabout way to facilitate identifying the process via 'ps' and 'grep'. 
	# For example, if we start the dispatcher by typing 'dispctl start' from 
	# within the dispatcher directory, and we execute Dispatcher() directly,  
	# the output of 'ps' looks something like this:
	#	/bin/bash -f ./dispctl start
	# If we type 'dispctl restart' instead, 'ps' shows this:
	#	/bin/bash -f ./dispctl restart
	# Whereas if we execute Dispatcher() in a sub-shell instead, it always looks like this, 
	# no matter how it was invoked:
	#	/bin/bash -fc source /Users/jtb/project/psm/sh/dispatcher-functions.sh; Dispatcher
	# it's good to have a unique string in the output of 'ps'. 
	# (That string is is defined in DISPATCHER_PROCESS_SIGNATURE).
		
	screen -c "${MODULE_SCREENRC_FILE}" -L -d -m -S "${DISPATCHER_SCREEN_ID}" /bin/bash -fc "${DISPATCHER_PROCESS_SIGNATURE}"


	# is there a dispmon already running?
	pids=( $(ps | grep "${DISPMON_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $1}' ) )
	if [[ -z "${pids+x}" ]] ; then 		# a dispmon is NOT running, so start one...
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Launching dispmon..."
		screen -c "${MODULE_SCREENRC_FILE}" -L -d -m -S "${DISPMON_SCREEN_ID}" \
						/bin/bash -fc "${DISPMON_PROCESS_SIGNATURE}"
	else
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: dispmon already running (${pids}). Skipping..."
	fi

}

# Dispatcher() -- Launch the dispatcher to send messages to the pods  
# Periodically executes a "sendbatch" command to each pod
# in order to upload each pod's queued files to the webserver.
function Dispatcher () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	SourceConfig $thisFunction #get the PODS array

	local s isare nPods=${#PODS[@]}
	[[ $nPods -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	if [[ $nPods -lt 1 ]] ; then 
		printf "${thisFunction}: No pods associated with this dispatcher. Nothing to do.\n"
		exit 1
	fi
	
	# log directly to the console; screen takes care of sending it to its logfile
	printf "$(date -u "+${DEFAULT_TIME_FORMAT}") ${thisFunction} starting.\n"
	printf "$(date -u "+${DEFAULT_TIME_FORMAT}") Found $nPods pod${s} (${PODS[*]}). Launching dispatcher (will wait ${DISPATCHER_WAIT} secs between calls; will report every ${DEFAULT_DISPATCHER_LOG_LOOPCOUNT}'th call).\n"		

	# The Dispatcher runs in an infinite loop. In order to keep an eye on it, 
	# we periodically ping the logfile by printing a message to it.
	# If we print every time, the logfile will rapidly fill up.
	# To print a message every n'th pass, set logTheNthPass accordingly:
	logTheNthPass=$DEFAULT_DISPATCHER_LOG_LOOPCOUNT  
	local nPass=0 nTimeout=0
	while : ; do  # continue forever...

		printf "%${DEFAULT_FORMAT_SPACING}s : " "localhost (pod '$(PodName)')"
		MakeFactoids # generate the dispatcher's own factoids and deposit them to the pod's upload queue
		${PSM_PROJECT}/${DEFAULT_POD_DIR}/housecall send # perform a housecall on the dispatcher's pod and deliver the queued files
		local errCode=$?
		if [[ $errCode -ne 0 ]] ; then  # housecall failed
			(( nTimeout++ ))
			if [[ $nTimeout -ge $DEFAULT_DISPATCHER_MAX_TIMEOUTS ]] ; then
				LogMe "${MODULE_LOGFILE}"  "${thisFunction}: *** Too many timeouts (${nTimeout}). Goodbye."
				exit 1
			fi
			LogMe "${MODULE_LOGFILE}"  "${thisFunction}: *** Timeout #${nTimeout}: waiting ${DEFAULT_DISPATCHER_TIMEOUT} secs before retrying sftp"
			sleep $DEFAULT_DISPATCHER_TIMEOUT
		else # housecall succeeded
			nTimeout=0 # reset the timeout counter
		fi
	
		# now poll the pods, one by one
		for pod in "${PODS[@]}" ; do
			# first generate and send the dispatcher's own factoids (to insure that website always has the latest info about dispatcher sendbatch)
			/bin/bash "${MODULE_DIR}/sendbatch"  # execute it in a shell, since Sendbatch() itself exits
			local errCode=$?
			if [[ $errCode -ne 0 ]] ; then  # sendbatch failed
				(( nTimeout++ ))
				if [[ $nTimeout -ge $DEFAULT_DISPATCHER_MAX_TIMEOUTS ]] ; then
					LogMe "${MODULE_LOGFILE}"  "${thisFunction}: *** Too many timeouts (${nTimeout}). Goodbye."
					exit 1
				fi
				LogMe "${MODULE_LOGFILE}"  "${thisFunction}: *** Timeout #${nTimeout}: waiting ${DEFAULT_DISPATCHER_TIMEOUT} secs before retrying sftp"
				sleep $DEFAULT_DISPATCHER_TIMEOUT
			else # sendbatch succeeded
				nTimeout=0 # reset the timeout counter
			fi


			local logString=""
			local errCount=0
			# logString="(${nPass}) Dispatcher ==> ${pod}..."
						
			local returnValue="$( ExecPodSilently $pod 'housecall send' )"
			# unpack the parts of the returnValue (include restoring a coded newline character)
			local resultCode=`echo "${returnValue}" | cut -d ${EPS_FIELDSEPARATOR} -f 1`
			# To insert a newline char in a sed replacement string, have to use special "$" quoting (see https://stackoverflow.com/a/18410122) 
			local resultMessage=`echo "${returnValue}" | cut -d ${EPS_FIELDSEPARATOR} -f 2 | sed -e '$s|${EPS_FIELDSEPARATOR}|\\\n|g' ` # convert the special char back into a newline
			if [[ "$resultCode" -eq 255 ]] ; then	# 255 means that ssh failed
				(( errCount++ ))
				logString="${logString}*** ERROR (${resultCode}): ${resultMessage}"
			elif [[ "$resultCode" -gt 0 ]] ; then	# ssh succeeded, but the command returned non-zero
				(( errCount++ ))
				logString="${logString}*** ERROR : *** housecall failed on pod '${pod}': ${resultMessage}"
			else	# ssh succeeded and command returned zero
#				logString="${logString} ${resultMessage}"
				logString="Housecall to '${pod}' succeeded."
				: # do-nothing
			fi
	
			# if there were any errors, or if this is just a periodic logfile checkin, then log it
			if [[ $(( $nPass % $logTheNthPass )) -eq 0 ]] || [[ $errCount -gt 0 ]]; then
				# LogMe "${MODULE_LOGFILE}" "${logString}"
				# To avoid also filling up the screen log file, just write directly to the dispatcher log file.
				# (don't use LogMe).
				printf "$(date -u "+${DEFAULT_TIME_FORMAT}") Sending housecall to '${pod}'\n" >> "${MODULE_LOGFILE}"
				printf "$(date -u "+${DEFAULT_TIME_FORMAT}") ${logString}\n" >> "${MODULE_LOGFILE}"
			fi

			sleep $DISPATCHER_WAIT
		done
	
		(( nPass++ ))
	done 
	
	LogMe "${MODULE_LOGFILE}"  "WTF? ${thisFunction} unexpectedly exited from its loop"
}


#############
# SanitizePods() -- convert the list of pod names into a website-ready list of names
############# 
function SanitizePods () {
	local callingFrom=$(TracePath)
	SourceConfig $callingFrom #get the PODS array	
	local cleanPods=""
	for pod in "${PODS[@]}" ; do
		cleanPods="${cleanPods} $(SanitizePod $pod)"
	done
	printf "${cleanPods}"
}
#############
# SanitizePod() -- convert a single pod (as listed in $POD) into a website-safe pod name
# Relies on each pod's _name command to return the pod's name
# Examples:	jtb@eos.local ==> eos
#			xyz@12.34.56.78 ==> mnemosyne
############# 
function SanitizePod () {
	local callingFrom=$(TracePath)
	[[ $# -ne 1 ]] && printf "NONAME"
	local podName pod="$1"
	local execReturnValue="$( ExecPodSilently $pod _name )" # get the name of the pod
	if ! ExtractReturnCode "${execReturnValue}" ; then
		podName="NONAME"
	else 	
		podName="$(ExtractOutputString "$execReturnValue" )"
	fi
	printf "${podName}"
}


#############
# MakeFactoids() -- deposit some dispatcher factoids to the sftp upload queue
############# 
function MakeFactoids () {
	if [[ "$#" -ne 0 ]] ; then
		printf "*** bad arg count in MakeFactoids\n"
		exit
	fi
	
	# step 1: Create a factoids file with info about this dispatcher
	local factoidFileName="dispatcher.$(PodName).factoids.txt" 
	local localPath="${POD_QUEUE_DIR}/${factoidFileName}" # path to the factoid file on this pod
	local remotePath="$(GetWebhostDispatchersDir)/${factoidFileName}" # path on the destination (remote) server


	# Create the factoids file and fill it with the basic info
	InitFactoids "${localPath}"
	
	# Append the factoids specific to this module
	cat << EOT >> $localPath
defaultwait: ${DISPATCHER_WAIT}
defaultloopcount: ${DEFAULT_DISPATCHER_LOG_LOOPCOUNT}
defaulttimeout: ${DEFAULT_DISPATCHER_TIMEOUT}
pods: $(SanitizePods)
EOT

	# step 2: create an sftp batch file for sending the factoids up to the web server
	local sftpBatchfile="${POD_QUEUE_DIR}/dispatcher.sftp"
	cat << EOT > $sftpBatchfile
put ${localPath} ${remotePath}
EOT

}

#############
# Dispatcher_stop() -- Stop the Dispatcher
############# 
function Dispatcher_stop () {
	local callingFrom=$(TracePath)
	if ! ModuleIsRunning dispatcher ; then
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_NAME} not running. Nothing to stop."
		return
	fi

	local p ppids
	
	# Kill the dispmon(s)
	ppids=( $(ps -O ppid | grep "${DISPMON_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $2}' ) )
	for p in "${ppids[@]}" ; do
		printf "${callingFrom}: stopping dispmon (${p})..."
		kill -SIGTERM $p 2>/dev/null  #redirect errors because sometimes it tries to kill child procs that have already disappeared
		printf "stopped. (OK)\n"
	done
	
	# kill the dispatcher(s)
	ppids=($(ps -O ppid | grep "${DISPATCHER_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $2}'))
	for p in "${ppids[@]}" ; do
		printf "${callingFrom}: stopping dispatcher (${p})..."
		kill -SIGTERM $p 2>/dev/null  #redirect errors because sometimes it tries to kill child procs that have already disappeared
		printf "stopped. (OK)\n"
	done
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_NAME} stopped. (OK)"
	return  # not exit, to allow for restart
}



#############
# DispatcherLog() -- view the log file
############# 
function DispatcherLog () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	if [[ -e "${MODULE_LOGFILE}" ]] ; then
		more "${MODULE_LOGFILE}"
	else 
		printf "${callingFrom}: No logfile (${MODULE_LOGFILE})\n"
	fi
}

#############
# DispatcherLogTail() -- view the tail end of the log file
############# 
function DispatcherLogTail () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if [[ -e "${MODULE_LOGFILE}" ]] ; then
		tail -50 "${MODULE_LOGFILE}"
	else 
		printf "${callingFrom}: No logfile (${MODULE_LOGFILE})\n"
	fi
}

#############
# Dispatcher_restart() -- restart Dispatcher
############# 
function Dispatcher_restart () {
#	LogMe ${MODULE_LOGFILE} "Attempting restart."
	# log directly to the console; screen takes care of sending it to its logfile
	printf "$(date -u "+${DEFAULT_TIME_FORMAT}") Attempting restart.\n"
	if ModuleIsRunning dispatcher ; then
		Dispatcher_stop
		sleep 1
	fi
	Dispatcher_start
}

#############
# Dispatcher_attach() -- attach the virtual terminal
############# 
function Dispatcher_attach () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if ! ModuleIsRunning dispatcher ; then
		printf "${callingFrom}: Dispatcher not running. Nothing to attach.\n"
		exit
	fi
	# LogMe ${MODULE_LOGFILE} "Attaching screen."
	# log directly to the console; screen takes care of sending it to its logfile
	printf "$(date -u "+${DEFAULT_TIME_FORMAT}") Attaching screen.\n"
	
	
	local socket="$(screen -list | grep "\.${DISPATCHER_SCREEN_ID}" | awk '{print $1}')"
	if [[ "$socket" = "" ]] ; then
		printf "${callingFrom}: No virtual terminal found.\n"
		exit
	fi
	
	printf "${callingFrom}: Attaching screen (type \"CTRL-a CTRL-d\" to exit) ..."
	sleep 2 # pause a moment to give the user a chance to read the previous message...

	# reattach the screen
	screen -r "$socket"

	printf "done.\n"
}

#############
# Dispatcher_peek() -- view  screen.log
#############
function Dispatcher_peek () { 
	Module_peek $@
}


#############
# DispMon() -- Monitor some of Dispatcher's child processes 
# If any appear to be running suspiciously long, then kill 'em.
#############
function DispMon () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $thisFunction	
	
	LogMe "${MODULE_LOGFILE}" "${thisFunction}: Starting up..."
	while : ; do 
		LogMe "${MODULE_LOGFILE}" "${thisFunction}: Wakey wakey! Time to check up on the Dispatcher..."

		# look for any outgoing ssh connections
		local psOutput=($(/bin/ps -o pid,etime,command | grep "${DISPMON_PROCESS_SIGNATURE}" | grep -v grep))
		if [[ ${#psOutput[@]} -eq 0 ]] ; then  # no housecall connection currently running
			printf "No process matching '${DISPMON_PROCESS_SIGNATURE}'\n"
		else # a housecall process is running
			# collect the ps output parameters
			local pid=${psOutput[0]} ; 	
			local etime=${psOutput[1]}; 
			unset psOutput[0] # (shift the array)
			unset psOutput[1] # (shift the array)
			local cmd=${psOutput[@]} # collect all the remaining fields (the command and its args)

			# parse the elapsed time from "DDDD-HH:MM:SS" into seconds 
			# (perl snippet adapted from https://stackoverflow.com/a/18845241/5011245 )
			local etimes=$(echo $etime | perl -ane '@t=reverse split(/[:-]/); $s=$t[0]+$t[1]*60+$t[2]*3600+$t[3]*86400; print "$s"')

			# extract the pod name from the 'ps' string
			local podName=$(echo "${cmd}" | sed -e "s|${SSH_COMMAND}||" | awk '{print $1}')

			# if the housecall connection has been going for suspiciously long time
			if [[ ${etimes} -gt ${DISPMON_HOUSECALL_MAX_DURATION} ]] ; then
				local msg="${callingFrom}: *** Housecall to pod '${podName}' running too long (${etimes} sec). Killing ssh connection..."
				LogException "${msg}"
				LogMe "${MODULE_LOGFILE}" "${msg}"
				kill -TERM "${pid}"
			fi
		fi
		
		LogMe "${MODULE_LOGFILE}" "${thisFunction}: Going back to sleep for ${DEFAULT_DISPMON_INTERVAL} seconds..."
		sleep ${DEFAULT_DISPMON_INTERVAL}
	done

	LogMe "${MODULE_LOGFILE}" "${thisFunction}: Terminated." # (I'm not sure how we'd ever get here, but...)
}



