#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
# Quick-n-dirty recompute the RMS from an archived audio file and its associated factoid

if [[ "$#" -ne 1 ]] ; then
	printf "Usage: $(basename $0) YYYYDDD\n"
	printf "Where YYYYDDD is the year-day of the factoid file.\n"
	exit
fi
yyyydddFactoid="$1"

rmsScript="${PSM_PROJECT}/psm/sh/eLoop-makeRMS.sh"

if [[ $yyyydddFactoid -lt 2017001 ]] || [[ $yyyydddFactoid -gt 2017185 ]] ; then
	printf "Bad date: ${yyyydddFactoid}\n"
	exit
fi

# the date of the audio file is actually the NEXT  day (e.g., at 00:01 AM)
yyyydddAudio="$(expr $yyyydddFactoid + 1)"

audioFile="$(find . -depth 1 -regex ".*${yyyydddAudio}-raw.aif")"
factoidsFile="$(find . -depth 1 -regex ".*${yyyydddFactoid}-factoids.txt")"

if [[ ! -e "${factoidsFile}" ]] ; then
	printf "Can't find factoids file ('${factoidsFile}').\n"
	exit
fi
if [[ ! -e "${audioFile}" ]] ; then
	printf "Can't find audio file ('${audioFile}').\n"
	exit
fi

f_rms="$(grep '^rms:' ${factoidsFile} | sed -e 's|.*:||'  -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' )"
f_db="$(grep '^dbgain:' ${factoidsFile} | sed -e 's|.*:||'  -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
f_g="$(grep '^gamma:' ${factoidsFile} | sed -e 's|.*:||'  -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' )"

printf "Audio file: '${audioFile}'\n"
printf "Factoids file: '${factoidsFile}' (gamma='${f_g}', dbgain='${f_db}', rms='${f_rms}')\n"

if [[ ${f_db} = "" ]] ;  then 
	f_db=0
	printf "db not set. Assuming it to be zero.\n"
fi
if [[ ! -z ${f_rms+x} ]] && [[ ${f_rms} != "" ]] ;  then 
	printf "rms already set (${f_rms}) But let's proceed anyway...\n"
fi
printf "Calculating last 24-hour rms... "
newRMS=$("${rmsScript}" "${audioFile}" "${f_g}" "${f_db}")
printf "$newRMS\n"

