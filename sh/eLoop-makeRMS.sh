#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
ME=`basename $0`	# the name of this script

# Given an audio file, this script computes the rms average value of one scaled day of audio. 
# The result is printed to stdout.
# The scaling factor, gamma, is required in order to compute the duration of a scaled day.
# Script should be executed at the end of the day, after the archive files are saved.

# Exactly three arguments are required: 
#	input file, the time scaling factor (gamma), and the gain correction (dB)
# Gain correction is usually 0dB, but is configurable in the config file to "correct" for
# low-gain sensors with low rms values that result in overly dark color swatches in the website archive calendar.

if [[ $# -eq 3 ]]
then
	aiffPath="$1"
	gamma="$2"
	dBGain="$3"
else
	>&2 printf "Usage: ${ME} audioFile gamma dBGain\n"
	exit
fi

# if the audio file doesn't exist, there's nothing we can do. 
if [ ! -f "$aiffPath" ]; then
	>&2 printf "${ME}: File '${aiffPath}' doesn't exist.\n"
	exit
fi

dir=$(dirname "$aiffPath")			# /foo/bar/seismic1.201609170000.2016261-raw.aif
filename=$(basename "$aiffPath")	# seismic1.201609170000.2016261-raw.aif
cd "$dir"
# Tell sox to look at the last $secondsFromEnd of the file, 
# which corresponds to the last (scaled) day of audio. 
secondsFromEnd=`expr 86400 / ${gamma}`

hipassCorner=20; # Corner frequency (Hz) of pre-filter (to reject subaudible components from the computation)

# get the app path variables
APPS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/APPS.sh" 
source "${APPS}"
soxout=`"${BIN_SOX}" $filename -n gain "${dBGain}" reverse trim 0 $secondsFromEnd reverse highpass $hipassCorner stat 2>&1 >/dev/null | grep -e 'RMS     amplitude'`
rms=`echo $soxout | sed -e 's/RMS amplitude: //'`
printf "$rms"