###################
# For controlling OS X Terminal colors on the fly
# Adapted from https://gist.github.com/avillafiorita/9e626ce370e1da6c6373
# Change colors and fonts of the OSX terminal from the command line:
# 
# $ TerminalForeground lime green
# $ TerminalFont "Oxygen Mono" 12

# In this (jtb's) version, two-word color names are NOT recognized.
# For example: "light blue" is not recognized, but "Lightblue" is. 
# Color names are case-insensitive.

COLORLIST="$( cd ${PSM_PROJECT}/psm/sh/assets && pwd )/colors.csv"	

function ListColors {
    cat ${COLORLIST} | cut -f 1 | more
}

function LookupColor {
    grep -i "$*" ${COLORLIST}
}

function get_apple_color {
	# do a case-insensitive search, and ignore any duplicate entries
	# (e.g., in colors.csv there are entries for "Blue" and "blue" with the same color code
    egrep -i "(^|,)$*(,|\t)" ${COLORLIST} | cut -f 6 | sort | uniq
}


# Fetch the RGB color triad as a space-separated string
function rgbTriad {
    local a=$(egrep -i "(^|,)$*(,|\t)" ${COLORLIST} | cut -f 3-5 | sort | uniq)	
	local b=$(echo $a | sed -e "s/[ \t]/ /g")
	printf "$b"
}


function TerminalBackground {
    color=$(get_apple_color $*)
    if [ "$color" != "" ] ; then
        osascript -e "tell application \"Terminal\" to set background color of window 1 to ${color}"
        printf "\tBackground color set to  : $* ${color}\n"
    else
    	printf "*** Don't recognize background color '${*}'\n"
    fi
}    

function TerminalForeground {
    color=$(get_apple_color $*)
    if [ "$color" != "" ] ; then
        osascript -e "tell application \"Terminal\" to set normal text color of window 1 to ${color}"
        printf "\tNormal text color set to : $* ${color}\n"
    else
    	printf "*** Don't recognize foreground color '${*}'\n"
    fi
}    

function TerminalTheme {
	TerminalForeground $1
	TerminalBackground $2
}    

function TerminalFont {
    osascript -e "tell application \"Terminal\" to set the font name of window 1 to \"$1\""
    osascript -e "tell application \"Terminal\" to set the font size of window 1 to $2"
}
