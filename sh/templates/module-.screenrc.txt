#%#%#% (Lines beginning with '#%#%#%' will be deleted)
#%#%#% This is a template for the screen run command file (.screenrc)
#%#%#% This template is edited by InstallPod() to create a functional version.
# config file for screen(1)
# Created __NOWSTRING__ by __THISFUNCTION__

logfile __SCREEN_LOGFILE__

# enable scrollwheel tracking
termcapinfo xterm* ti@:te@
