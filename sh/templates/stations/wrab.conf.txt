# Created __NOWSTRING__ by __THISFUNCTION__
# Metadata sources
# https://www.fdsn.org/networks/detail/II/
# http://service.iris.edu/irisws/fedcatalog/1/query?net=II&format=text&includeoverlaps=true&nodata=404
# https://ida.ucsd.edu/
# http://www.fdsn.org/station_book/II/WRAB/wrab.html
# https://ida.ucsd.edu/?q=station/wrab

# Required parameters:
STATION="WRAB"
LOCATION="Tennant Creek, NT, Australia"
STATION_LAT=-19.93
STATION_LON=134.36
STATION_URL="https://ida.ucsd.edu/?q=station/wrab"
NETWORK="IRIS/IDA"
NETWORK_URL="https://ida.ucsd.edu/"
SIGNAL_TYPE="seismic"
SIGNAL_SOURCE="solid Earth"
INSTRUMENT_TYPE="seismometer"
INSTRUMENT="Geotech KS54000 triaxial broadband borehole seismometer"
RECORDER="Quanterra Q330"
SAMPLE_RATE=20
DATATYPE="miniseed"
GAMMA=900
HOURS=3
MAXFREQUENCY=1
FIXRATE=44100
CHANNELS="II.WRAB.00.BH1 II.WRAB.00.BH2"
SOURCEDIRS="/Users/jtb/earthsound/slink/inbound/II/WRAB"

# Optional parameters:
# ENDTIME=20161130120304		 # i.e., 12:03:04 Nov 30 2016 
# LOOPMAX=0
# SOXFILTER="highpass -2 90 highpass -2 90"
# SOXCOMPAND="compand 0.001,0.01 6:-80,-50,-40,-10,0,-3"
# SPECTROPOST=YES_PLEASE
# DBGAIN=0
# AUDIO_DEVICE="psmdev1"
# SHOUTCAST="PASSWORD@HOST:PORT"	# credentials for ShoutCasting up to Centova Cast
# CC_PORT="NNNN"	# port number for Centova Cast server
# CC_ACCOUNT="AAAAAA"	# account name for Centova Cast server
# CC_STREAMURL="https://audibleearth.com/proxy/AAAAAA/stream"	# URL for streaming audio

