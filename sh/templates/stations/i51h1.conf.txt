# Created __NOWSTRING__ by __THISFUNCTION__
# Metadata sources
# https://www.fdsn.org/networks/detail/IM/
# http://service.iris.edu/irisws/fedcatalog/1/query?net=IM&format=text&includeoverlaps=true&nodata=404

# Required parameters:
STATION="I51H1"
LOCATION="Bermuda Infrasound Array"
STATION_LAT=32.361542
STATION_LON=-64.698738
STATION_URL="http://service.iris.edu/irisws/fedcatalog/1/query?net=IM&format=text&includeoverlaps=true&nodata=404"
NETWORK="IMS"
NETWORK_URL="https://www.fdsn.org/networks/detail/IM/"
SIGNAL_TYPE="infrasound"
SIGNAL_SOURCE="atmosphere"
INSTRUMENT_TYPE="infrasound sensor"
INSTRUMENT="MB2005"
RECORDER="unknown"
SAMPLE_RATE=20
DATATYPE="miniseed"
GAMMA=10
HOURS=1
MAXFREQUENCY=100
FIXRATE=44100
CHANNELS="IM.I51H1..BDF"
SOURCEDIRS="/Users/jtb/earthsound/slink/inbound/IM/I51H1"

# Optional parameters:
# ENDTIME=20161130120304		 # i.e., 12:03:04 Nov 30 2016 
# LOOPMAX=0
# SOXFILTER="highpass -2 90 highpass -2 90"
# SOXCOMPAND="compand 0.001,0.01 6:-80,-50,-40,-10,0,-3"
# SPECTROPOST=YES_PLEASE
# DBGAIN=0
# AUDIO_DEVICE="psmdev1"
# SHOUTCAST="PASSWORD@HOST:PORT"	# credentials for ShoutCasting up to Centova Cast
# CC_PORT="NNNN"	# port number for Centova Cast server
# CC_ACCOUNT="AAAAAA"	# account name for Centova Cast server
# CC_STREAMURL="https://audibleearth.com/proxy/AAAAAA/stream"	# URL for streaming audio

