# Created __NOWSTRING__ by __THISFUNCTION__
# Metadata sources
# https://www.fdsn.org/networks/detail/IU/
# http://service.iris.edu/irisws/fedcatalog/1/query?net=IU&format=text&includeoverlaps=true&nodata=404
# https://www.fdsn.org/station_book/IU/PMSA/pmsa.html



# Required parameters:
STATION="PMSA"
LOCATION="Palmer Station, Antarctica"
STATION_LAT=-64.7747
STATION_LON=-64.0480
STATION_URL="https://www.fdsn.org/station_book/IU/PMSA/pmsa.html"
NETWORK="IRIS/USGS-GSN"
NETWORK_URL="https://www.fdsn.org/networks/detail/IU/"
SIGNAL_TYPE="seismic"
SIGNAL_SOURCE="solid Earth"
INSTRUMENT_TYPE="seismometer"
INSTRUMENT="STS-1 broadband seismometer"
RECORDER="Quanterra Q680"
SAMPLE_RATE=20
DATATYPE="miniseed"
GAMMA=900
HOURS=3
MAXFREQUENCY=1
FIXRATE=44100
CHANNELS="IU.PMSA.00.BH1 IU.PMSA.00.BHZ"
SOURCEDIRS="/Users/jtb/earthsound/slink/inbound/IU/PMSA"

# Optional parameters:
# ENDTIME=20161130120304		 # i.e., 12:03:04 Nov 30 2016 
# LOOPMAX=0
# SOXFILTER="highpass -2 90 highpass -2 90"
# SOXCOMPAND="compand 0.001,0.01 6:-80,-50,-40,-10,0,-3"
# SPECTROPOST=YES_PLEASE
# DBGAIN=0
# AUDIO_DEVICE="psmdev1"
# SHOUTCAST="PASSWORD@HOST:PORT"	# credentials for ShoutCasting up to Centova Cast
# CC_PORT="NNNN"	# port number for Centova Cast server
# CC_ACCOUNT="AAAAAA"	# account name for Centova Cast server
# CC_STREAMURL="https://audibleearth.com/proxy/AAAAAA/stream"	# URL for streaming audio

