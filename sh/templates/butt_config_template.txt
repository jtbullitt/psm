#%#%#% (Lines beginning with '#%#%#%' will be deleted)
#%#%#% This is a template for a BUTT config file. It's edited by EditButtConfig().
#%#%#%
#This is a configuration file for BUTT (Broadcast Using This Tool)

[main]
server = __BUTT_SERVER__
srv_ent = __BUTT_SRV_ENT__
icy = __BUTT_ICY__
icy_ent = __BUTT_ICY_ENT__
num_of_srv = 1
num_of_icy = 1
song_path = __BUTT_SONG_PATH__
song_update = 1
gain = 1.0
connect_at_startup = 1
log_file =

[audio]
device = __BUTT_DEVICE__
device2 = -1
dev_remember = 1
samplerate = 44100
bitrate = 128
channel = __BUTT_CHANNEL__
left_ch = 1
right_ch = 2
left_ch2 = 1
right_ch2 = 2
codec = mp3
resample_mode = 1
silence_level = 50.000000
signal_level = 50.000000
disable_dithering = 0
buffer_ms = 50
dev_name = __BUTT_DEVICE_NAME__ [Core Audio]
dev2_name = None

[__BUTT_SRV_ENT__]
address = __BUTT_SRV_HOST__
port = __BUTT_SRV_PORT__
password = __BUTT_SRV_PASSWORD__
type = 0
mount = (none)
usr = (none)

[__BUTT_ICY_ENT__]
pub = 0
description = Insert description here
genre = Ambient
url = earthsound.org
irc = 
icq = 
aim = 


[record]
bitrate = 192
codec = mp3
start_rec = 0
stop_rec = 0
rec_after_launch = 0
overwrite_files = 0
sync_to_hour  = 0
split_time = 0
filename = rec_%Y%m%d-%H%M%S.mp3
signal_threshold = 0.000000
silence_threshold = 0.000000
signal_detection = 0
silence_detection = 0
folder = /Users/jtb/Music/

[tls]
cert_file = 
cert_dir = 

[dsp]
equalizer = 0
equalizer_rec = 0
eq_preset = Manual
gain1 = 0.000000
gain2 = 0.000000
gain3 = 0.000000
gain4 = 0.000000
gain5 = 0.000000
gain6 = 0.000000
gain7 = 0.000000
gain8 = 0.000000
gain9 = 0.000000
gain10 = 0.000000
compressor = 0
compressor_rec = 0
aggressive_mode = 0
threshold = -20.000000
ratio = 5.000000
attack = 0.010000
release = 1.000000
makeup_gain = 0.000000

[mixer]
primary_device_gain = 1.000000
primary_device_muted = 0
secondary_device_gain = 1.000000
secondary_device_muted = 0
streaming_gain = 1.000000
recording_gain = 1.000000
cross_fader = 0.000000

[gui]
attach = 0
ontop = 0
hide_log_window = 0
remember_pos = 1
x_pos = 649
y_pos = 361
window_height = 395
lcd_auto = 0
default_stream_info = 0
start_minimized = 0
disable_gain_slider = 0
show_listeners = 1
listeners_update_rate = 10
lang_str = system
vu_low_color = 13762560
vu_mid_color = -421134336
vu_high_color = -939524096
vu_mid_range_start = -12
vu_high_range_start = -6
always_show_vu_tabs = 1
window_title = 
vu_mode = 1

[mp3_codec_stream]
enc_quality = 3
stereo_mode = 0
bitrate_mode = 0
vbr_quality = 4
vbr_min_bitrate = 32
vbr_max_bitrate = 320
vbr_force_min_bitrate = 0
resampling_freq = 0
lowpass_freq_active = 0
lowpass_freq = 0.000000
lowpass_width_active = 0
lowpass_width = 0.000000
highpass_freq_active = 0
highpass_freq = 0.000000
highpass_width_active = 0
highpass_width = 0.000000

[mp3_codec_rec]
enc_quality = 3
stereo_mode = 0
bitrate_mode = 0
vbr_quality = 4
vbr_min_bitrate = 32
vbr_max_bitrate = 320
vbr_force_min_bitrate = 0
resampling_freq = 0
lowpass_freq_active = 0
lowpass_freq = 0.000000
lowpass_width_active = 0
lowpass_width = 0.000000
highpass_freq_active = 0
highpass_freq = 0.000000
highpass_width_active = 0
highpass_width = 0.000000

[vorbis_codec_stream]
bitrate_mode = 0
vbr_quality = 0
vbr_min_bitrate = 0
vbr_max_bitrate = 0

[vorbis_codec_rec]
bitrate_mode = 0
vbr_quality = 0
vbr_min_bitrate = 0
vbr_max_bitrate = 0

[opus_codec_stream]
bitrate_mode = 1
quality = 0
audio_type = 0
bandwidth = 0

[opus_codec_rec]
bitrate_mode = 1
quality = 0
audio_type = 0
bandwidth = 0

[aac_codec_stream]
bitrate_mode = 0
afterburner = 0
profile = 0

[aac_codec_rec]
bitrate_mode = 0
afterburner = 0
profile = 0

[flac_codec_stream]
bit_depth = 16

[flac_codec_rec]
bit_depth = 16

[wav_codec_rec]
bit_depth = 16

[midi]
dev_name = Disabled

[midi_cmd_0]
enabled = 0
channel = 0
msg_num = 0
msg_type = 176
mode = 0
soft_takeover = 0

[midi_cmd_1]
enabled = 0
channel = 0
msg_num = 1
msg_type = 176
mode = 0
soft_takeover = 0

[midi_cmd_2]
enabled = 0
channel = 0
msg_num = 2
msg_type = 176
mode = 0
soft_takeover = 0

[midi_cmd_3]
enabled = 0
channel = 0
msg_num = 3
msg_type = 176
mode = 0
soft_takeover = 0

[midi_cmd_4]
enabled = 0
channel = 0
msg_num = 4
msg_type = 176
mode = 0
soft_takeover = 0

[midi_cmd_5]
enabled = 0
channel = 0
msg_num = 5
msg_type = 176
mode = 0
soft_takeover = 0

[midi_cmd_6]
enabled = 0
channel = 0
msg_num = 6
msg_type = 176
mode = 0
soft_takeover = 0

[midi_cmd_7]
enabled = 0
channel = 0
msg_num = 7
msg_type = 176
mode = 0
soft_takeover = 0

[midi_cmd_8]
enabled = 0
channel = 0
msg_num = 8
msg_type = 176
mode = 0
soft_takeover = 0

[midi_cmd_9]
enabled = 0
channel = 0
msg_num = 9
msg_type = 176
mode = 0
soft_takeover = 0

