#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)

# Where the psm native (XCode) apps and libs live
BINDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )/bin" 
LIBDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )/lib" 

# Where the XCode development apps and libs live
DEV_BINDIR="/Users/jtb/dev/bin"
DEV_LIBDIR="/Users/jtb/dev/lib"

##################
# These must be updated when installing a new version of ImageMagick
# Copy the unzipped ImageMagick tarball to psm/bin (BINDIR). 
# See comments in bundler.sh for details
# DEPRECATED IMAGE_MAGICK_HOME="${BINDIR}/ImageMagick-7.0.10"
# DEPRECATED IMAGE_MAGICK_WAND="libMagickWand-7.Q16HDRI.8.dylib"
# DEPRECATED IMAGE_MAGICK_CORE="libMagickCore-7.Q16HDRI.8.dylib"
# DEPRECATED IMAGE_MAGICK_BIN="${IMAGE_MAGICK_HOME}/bin/magick" # the version of magick that comes with the magick zip archive
# DEPRECATED IMAGE_MAGICK_VERSION=$(basename ${IMAGE_MAGICK_HOME})
##################

# Homebrew installs
BREW_DIR="/opt/homebrew/bin"
BIN_BREW="${BREW_DIR}/brew"
BIN_MAGICK="${BREW_DIR}/magick"
BIN_SOX="${BREW_DIR}/sox"
BIN_GNUPLOT="${BREW_DIR}/gnuplot"
BIN_ID3V2="${BREW_DIR}/id3v2"
BIN_GIT_LFS="${BREW_DIR}/git-lfs"
HOMEBREW_APPS=($BIN_MAGICK $BIN_SOX $BIN_GNUPLOT $BIN_ID3V2 $BIN_GIT_LFS $BIN_BREW)

# these are the formulae that need to be installed
BREW_FORMULAE=(\
	git-lfs\
	boost\
	gnuplot\
	id3v2\
	imagemagick\
	jsoncpp\
	libsamplerate\
	portaudio\
	sox\
	lame\
	portmidi\
	fltk\
)


# native (jtb XCode) clus
BIN_ESOUND="${BINDIR}/esound2"
BIN_AUDIODEV="${BINDIR}/audiodev"
BIN_EVENTOVERLAY="${BINDIR}/eventOverlay"
BIN_SOXOVERLAY="${BINDIR}/soxOverlay"
BIN_SLARCHIVE="${BINDIR}/slarchive" # IRIS's 'slarchive' executable
BIN_RADIOMIX="${BINDIR}/radiomix"
BIN_BUTT="${BINDIR}/buttj"
XCODE_APPS=($BIN_ESOUND $BIN_AUDIODEV $BIN_EVENTOVERLAY $BIN_SOXOVERLAY $BIN_SLARCHIVE $BIN_BUTT) # $BIN_RADIOMIX)

# Apps that require special handling
#BIN_BUTT="/Applications/butt.app/Contents/MacOS/butt"
BIN_LOOPBACK="/Applications/Loopback.app/Contents/macOS/Loopback"
SPECIAL_APPS=($BIN_LOOPBACK) # ("${BIN_BUTT}")

ALL_APPS=( "${XCODE_APPS[@]}" "${HOMEBREW_APPS[@]}" "${SPECIAL_APPS[@]}")
