#! /bin/bash

# Get the constants that concern the apps and libs
APPS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/APPS.sh" 
source "${APPS}"

# Test all the apps to make sure they're present and that they actually run
# In most cases, this is done by invoking their "--version" command line option.
function testApps() {
	printf "Now testing the apps...\n"
	for appPath in "${XCODE_APPS[@]}" "${HOMEBREW_APPS[@]}" ; do
		appName=$(basename $appPath)
		local versionFlag="--version"
		[[ ${appName} = "slarchive" ]] && versionFlag="-V"
		[[ ${appName} = "audiodev" ]] || [[ ${appName} = "buttj" ]] && versionFlag="-v"
		[[ ${appName} = "magick" ]] && versionFlag="-version"
		printf "\t${appName}... "
		if [[ $( ("${appPath}" "${versionFlag}" 2>&1) > /dev/null ; echo $?) -ne 0 ]] ; then
			printf "*** missing or broken. PSM needs repair.\n"
		else
			printf "OK\n"
		fi
	done
	for appPath in "${SPECIAL_APPS[@]}" ; do
		appName=$(basename $appPath)
		printf "\t${appName}..."
		if [[ ${appName} = "Loopback" ]] ; then
			cat << EOT

	==> Will now attempt to launch Loopback.
	==> Once Loopback successfully opens, you may quit it,
	==> along with its accompanying Terminal window.
EOT
			read -p "	Continue? (Y/N) " confirm 
			if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] ; then
				printf "	Now attempting to launch Loopback... "
				if [[ $( (open ${BIN_LOOPBACK} 2>&1) > /dev/null ; echo $?) -ne 0 ]] ; then
					printf  "*** Loopback missing or broken. Can't continue.\n" 
					cat << EOT
		How to install Loopback:
			1. Download the macOS app from https://rogueamoeba.com/loopback/
			2. Run the installer
			3. Launch Loopback
			4. Follow instructions to allow permissions
			5. Purchase a license and enter the license key in Loopback > License...
EOT
					exit
				else
					printf "OK\n"
				fi
			else 
				printf "\tSkipping ${appName}\n"
			fi
		else
			printf "Don't know how to handle app '${appName}'\n"
		fi
	done
}

testApps

