#!/bin/bash -f
# Copyright (C) 2019  JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
# Various functions used by the slink/* scripts

MODULE_TYPE="rtmon"
MODULE_NAME="rtmon"
COMMANDS=()
MODULE_FUNCTION_PATH="${BASH_SOURCE[0]}" # path to this module function file
MODULE_FUNCTION_FILENAME="$( basename ${BASH_SOURCE[0]} )" # handy name of this module function file

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # the containing dir of this script (http://stackoverflow.com/a/246128/5011245)
source "${SCRIPTS_DIR}/DEFAULTS.sh" # get the default params
source "${SCRIPTS_DIR}/shared.sh" # get the common functions and set some crucial variables
RTMON_SCREEN_ID="rtmon" # id for the virtual terminal
MODULE_COMMANDS=(clean help peek status val ver _update)
MODULE_SPECIAL_DIRS=() # special dirs (unique to this module) that must be present 
MODULE_SPECIAL_FILES=(${DEFAULT_MODULE_SCREENRC_FILE})	# special files to be installed from templates
MODULE_REQUIRED_BINARIES=()
MODULE_USER_PREFERENCES=("${MODULE_TYPE}.conf") # user-configurable files (to be saved with the "backup" command)
MODULE_PROTECTED_FILES=("${MODULE_USER_PREFERENCES[@]}" "${MODULE_COMMANDS[@]}" "${MODULE_SPECIAL_FILES[@]}" "demo-${MODULE_TYPE}.conf") # files that will never be clean'ed


# List of parameters to expect in the config file
CONFIG_REQUIRED_ARRAYS=()
CONFIG_REQUIRED_VARIABLES=()
CONFIG_OPTIONAL_ARRAYS=()
CONFIG_OPTIONAL_VARIABLES=(RT_ARCHIVE_STA)
CONFIG_DEPRECATED_VARIABLES=()


#############
# Rtmon_help() -- print a help message
#############
function Rtmon_help () { 
	cat << EOT
A PSM ${MODULE_TYPE} module may be controlled from the command line.
Commands are bash scripts; to invoke a command, type its path. 
For example, to generate this help message from the PSM project directory (${PSM_PROJECT}), 
type '${MODULE_TYPE}/help'. Some commands take optional arguments.

   Command     What it does
   =======     ============
   clean       Delete any temporary or stray files from the module directory and roll over the logfiles.
               (Reminder: be careful adding your own files to this directory, as 'clean' will
               delete them, unless their names end in ".txt".)
   help        Print this help info.
   status      Print the run status of this ${MODULE_TYPE} module
               Usage: ${MODULE_NAME}/status [verbose | code]
                  verbose : also print the last few lines of the log file
                     code : print only a coded summary of the status
   val         Validate the ${MODULE_TYPE} module's config file and print the result.
               Usage: ${MODULE_NAME}/val [count | verbose | oneliner]
                  count    :  print the number of errors
                  verbose  :  print a detailed report
                  oneliner :  print a one-line summary [default]
   ver         Show the codebase version number
               Usage: ${MODULE_NAME}/ver [full]
                      full : print the full (major and minor) version numbers
               (By default, just print the major version number.)
EOT
}



# ValidateParamCallback() -- Validate a single parameter
# This is called by shared.sh::ValidateModule.
# It examines a single variable (name supplied in $1) to see if its value ($@) is appropriate
# If there is an error, print a single line that describes the error.
# If no error, print nothing. 
# shared.sh::ValidateModule takes care of tallying up errors and printing the result.
# For cleaner output formatting, do not include colons in the error message.
function ValidateParamCallback () {	
	local variableName="$1"
	shift ; local args=($@)  # $args is everything after $1
	local s reply=""

	case "$variableName" in
	"FOO") # (for example)
		if [[ "${args[0]}" != "1234" ]] ; then
			reply="*** Wrong value (should be 1234, but is ${args[@]})"
		fi
		;; 
	"RT_ARCHIVE_STA")
		local filePath=${args[0]}
		local base=$( basename ${filePath} )
		# verify that the file has a suitable name
		if [[ ${base} != "archive.sta" ]] ; then
			reply="*** '${base}' is not a RefTek archive file (${filePath})"
		else 
			# verify that the archive config file actually exists
			local missing=()
			if [[ ! -f $filePath ]] ; then
				missing=( "${missing[@]}" "$s" )
			fi
			if [[ ${#missing[@]} -gt 0 ]] ; then
				[[ ${#missing[@]} -eq 1 ]] && s="" || s="s" # pluralizer
				reply="*** Missing RefTek archive file (${filePath})"
			fi
		fi
		;;
	*)
		;;
	esac

	printf "${reply}"
}

#############
# Rtmon__update() -- update the module scripts
#############
function Rtmon__update () { 
	local callingFrom=$(TracePath)
	if [[ $1 != $DEFAULT_PROGRAM_RUN_KEY ]] ; then
		printf "${callingFrom}: Please run pod/update instead.\n"
		exit
	fi
	UpdateModule nogit $@
}


#############
# Rtmon_val() -- Validate the config file
# If an argument is present and is equal to "verbose", then print out info
# about the config file. 
# If no arg "verbose", then just print a count of the number of errors.
#############
function Rtmon_val () {
	ValidateModule $@ # i.e., shared.sh::ValidateModule
}

#
# PrepareParams() -- tidy up config params and construct any needed ones from those in the config
# If $1="verbose" then printf any (non-fatal) warnings
# Returns the number of (non-fatal) warnings
function PrepareParams () {
	local warningCount=0

	return $warningCount
}

#############
# Rtmon_status() -- report the status of the module
#############
function Rtmon_status () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	SourceConfig $callingFrom 

	local s isare errCount=$(Rtmon_val count)
	if [[ $errCount -gt 0 ]] ; then
		[[ $errCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		printf "${callingFrom}: *** The ${MODULE_TYPE} module has ${errCount} error${s}.\n"
		printf "\n"
		Rtmon_val verbose
	fi
	
	printf "Current status of RefTek processes:\n"
	ReportRTstatus "rtcc"
	ReportRTstatus "rtpd"
	
	local dirCount=0
	# count up the number of dirs in the archive dir
	if [[ ! -z ${RT_ARCHIVE_STA+x} ]] && [[ -f ${RT_ARCHIVE_STA} ]] ; then  # if RT_ARCHIVE_STA is set and the file exists
		dirCount=$(find $(dirname ${RT_ARCHIVE_STA}) -depth 1 -type d | wc -l | xargs)  # (xargs handily strips whitespace)
	fi
	[[ $dirCount -eq 1 ]] && { s="y" ; isare="is" ; } || { s="ies" ; isare="are" ; } # pluralizer
	printf "%${DEFAULT_FORMAT_SPACING}s : %d director${s} in %s\n" "dirCount" $dirCount $(dirname ${RT_ARCHIVE_STA})
}


#############
# ReportRTstatus() -- print the status of the named Reftek process
#############
function ReportRTstatus() {
	if [[ $# -ne 1 ]] ; then
		printf " *** rtProc error ***\n"
		return;
	fi
	local procName=$1
	local psOutput=($(ps -x | grep "bin/${procName} " | grep -v grep))
# atmos.local:~/earthsound [6.506] $ ps -x | grep reftek
#   698 ??       24912:01.52 /Users/jtb/reftek/bin/rtpd -bd ini=/Users/jtb/reftek/bin/rtpd.ini
#   538 ttys000    0:59.52 /Users/jtb/reftek/bin/rtcc /Users/jtb/reftek/bin/rtcc.ini
# 40659 ttys001    0:00.00 grep reftek
# atmos.local:~/earthsound [7.507] $ 
	if [[ ${#psOutput[@]} -gt 0 ]] ; then 
		status="${DEFAULT_MODULE_STATE_UP}"
		procUptime=${psOutput[2]}
	else
		status="${DEFAULT_MODULE_STATE_DOWN}"
		procUptime=0
	fi

	printf "%${DEFAULT_FORMAT_SPACING}s : %s (%s)\n" "$procName" "$status" "$procUptime"
}




#############
# Rtmon_ver() -- print the version number of the installed scripts
#############
function Rtmon_ver () { 
	Version $@
}




#############
# Rtmon_clean() -- tidy up the files
#############
function Rtmon_clean () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if ModuleIsRunning rtmon ; then
		printf "${callingFrom}: ${MODULE_NAME} is running. Can't clean. Run '${MODULE_NAME}/stop', then try again.\n"
		exit
	fi
	
	SourceConfig $callingFrom 
	cd "${MODULE_DIR}" &&  LogBreak "${MODULE_LOGFILE}" "${callingFrom}"
	local mBytesBefore="$(du -cm | tail -1 | awk '{print $1}')" # get the current disk space usage

	
	printf "${callingFrom}: "
	DeleteAllExcept 8675309 ${MODULE_PROTECTED_FILES[*]} 	# delete all but the protected files
	find "${MODULE_WORKSPACE_DIR}" -depth 1 -type f -delete  # clean out the workspace

	local mBytesAfter="$(du -cm | tail -1 | awk '{print $1}')" # get the current disk space usage
	local mBytesCleaned=`expr $mBytesBefore - $mBytesAfter`
	printf "${callingFrom}: Deleted ${mBytesCleaned} MB. (OK)\n"
	RotateLogFiles "${MODULE_SCREEN_LOGFILE}"  "${MODULE_EXCEPTION_LOGFILE}"
	# (don't rotate "${MODULE_LOGFILE}" here; UpdateModule's RecreateModule will take care of that)

	UpdateModule nogit && LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_TYPE} module cleaned. (OK)"
}

#############
# Rtmon_peek() -- view  screen.log
#############
function Rtmon_peek () { 
	Module_peek $@
}
