#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
# Various functions used by the slink/* scripts

MODULE_TYPE="slink"
MODULE_NAME="slink"
MODULE_FUNCTION_PATH="${BASH_SOURCE[0]}" # path to this module function file
MODULE_FUNCTION_FILENAME="$( basename ${BASH_SOURCE[0]} )" # handy name of this module function file

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # the containing dir of this script (http://stackoverflow.com/a/246128/5011245)
source "${SCRIPTS_DIR}/DEFAULTS.sh" # get the default params
source "${SCRIPTS_DIR}/shared.sh" # get the common functions and set some crucial variables
SLINK_FILE_STRUCTURE="%n/%s/%n.%s.%l.%c.%Y.%j.%H.#M.#S.miniseed" # the seedlink file/directory structure to be used
SLARCHIVE_SCREEN_ID="slarchive" # id for the virtual terminal
SLINKSWEEPER_SCREEN_ID="slinksweeper" # id for the virtual terminal
MODULE_COMMANDS=(clean help peek start restart status stop val ver _update)
MODULE_SPECIAL_DIRS=($DEFAULT_SLINK_INBOUND_DIR) # special dirs (unique to this module) that must be present 
MODULE_SPECIAL_FILES=(demo-streamlist.txt demo-jamaseis.txt ${DEFAULT_MODULE_SCREENRC_FILE})	# special files to be installed from templates
MODULE_REQUIRED_BINARIES=($BIN_SLARCHIVE)
MODULE_USER_PREFERENCES=(streamlist.txt "${MODULE_TYPE}.conf") # user-configurable files (to be saved with the "backup" command)
MODULE_PROTECTED_FILES=("${MODULE_USER_PREFERENCES[@]}" "${MODULE_COMMANDS[@]}" "${MODULE_SPECIAL_FILES[@]}" "demo-${MODULE_TYPE}.conf") # files that will never be clean'ed


# List of parameters to expect in the config file
CONFIG_REQUIRED_ARRAYS=(SERVERS STREAMLISTS)
CONFIG_REQUIRED_VARIABLES=(KEEPDAYS)
CONFIG_OPTIONAL_VARIABLES=()
CONFIG_DEPRECATED_VARIABLES=(INBOUND_DIR)


#############
# Slink_help() -- print a help message
#############
function Slink_help () { 
	cat << EOT
A PSM ${MODULE_TYPE} module may be controlled from the command line.
Commands are bash scripts; to invoke a command, type its path. 
For example, to generate this help message from the PSM project directory (${PSM_PROJECT}), 
type '${MODULE_TYPE}/help'. Some commands take optional arguments.

   Command     What it does
   =======     ============
   clean       Delete any temporary or stray files from the program directory and roll over the logfiles.
               (Reminder: be careful adding your own files to this directory, as 'clean' will
               delete them, unless their names end in ".txt".)
   help        Print this help info.
   start       Start ${MODULE_NAME} (slarchive and the slinksweeper)
   status      Print the run status of this ${MODULE_TYPE} module
               Usage: ${MODULE_NAME}/status [verbose | code]
                  verbose : also print the last few lines of the log file
                     code : print only a coded summary of the status
   stop        Stop this ${MODULE_TYPE} module (if it is running)
   val         Validate the ${MODULE_TYPE} module's config file and print the result.
               Usage: ${MODULE_NAME}/val [count | verbose | oneliner]
                  count    :  print the number of errors
                  verbose  :  print a detailed report
                  oneliner :  print a one-line summary [default]
   ver         Show the codebase version number
               Usage: ${MODULE_NAME}/ver [full]
                      full : print the full (major and minor) version numbers
               (By default, just print the major version number.)
EOT
}


# ValidateParamCallback() -- Validate a single parameter
# This is called by shared.sh::ValidateModule.
# It examines a single variable (name supplied in $1) to see if its value ($@) is appropriate
# If there is an error, print a single line that describes the error.
# If no error, print nothing. 
# shared.sh::ValidateModule takes care of tallying up errors and printing the result.
# For cleaner output formatting, do not include colons in the error message.
function ValidateParamCallback () {
	local variableName="$1"
	shift ; local args=($@)  # $args is everything after $1
	local s reply=""

	case "$variableName" in
	"FOO") # (for example)
		if [[ "${args[0]}" != "1234" ]] ; then
			reply="*** Wrong value (should be 1234, but is ${args[@]})"
		fi
		;; 
	"STREAMLISTS")
		# verify that the streamlist files are actually present
		local streamList
		local missing=()
		for streamList in "${STREAMLISTS[@]}" ; do
			local s="${MODULE_DIR}/${streamList}"
			if [[ ! -f $s ]] ; then
				missing=( "${missing[@]}" "$s" )
			fi
		done
		if [[ ${#missing[@]} -gt 0 ]] ; then
			[[ ${#missing[@]} -eq 1 ]] && s="" || s="s" # pluralizer
			reply="*** Missing ${#missing[@]} streamlist file${s}: ${missing[@]}"
		fi
		;;
	*)
		;;
	esac

	# some variables interact with each other
#	if [[ ${#SERVERS[@]} -ne ${#STREAMLISTS[@]} ]] ; then
#		otherErrors=( "${otherErrors[@]}" "SERVERS/STREAMLISTS count mismatch. Both must have same number of elements.")
#	else
#		if [[ $outputMode = "verbose" ]] ; then 
#			printf "Count of SERVERS & STREAMLISTS matches (OK)\n"
#		fi
#	fi

	printf "${reply}"
}


#############
# Slink__update() -- update the module scripts
#############
function Slink__update () { 
	local callingFrom=$(TracePath)
	if [[ $1 != $DEFAULT_PROGRAM_RUN_KEY ]] ; then
		printf "${callingFrom}: Please run pod/update instead.\n"
		exit
	fi
	UpdateModule nogit $@
}


#############
# Slink_val() -- Validate the config file
# If an argument is present and is equal to "verbose", then print out info
# about the config file. 
# If no arg "verbose", then just print a count of the number of errors.
#############
function Slink_val () {
	ValidateModule $@ # i.e., shared.sh::ValidateModule
}

#
# PrepareParams() -- tidy up config params and construct any needed ones from those in the config
# If $1="verbose" then printf any (non-fatal) warnings
# Returns the number of (non-fatal) warnings
function PrepareParams () {
	local warningCount=0

	return $warningCount
}


#############
# Slink_status() -- report the status of slink
#############
function Slink_status () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	local s isare errCount=$(Slink_val count)
	if [[ $errCount -gt 0 ]] ; then
		[[ $errCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		printf "${callingFrom}: *** The ${MODULE_TYPE} module has ${errCount} error${s}.\n"
		printf "\n"
		Slink_val verbose
	fi

	# get an array of pids for instances of slarchive
	local pids=( $(ps | grep "${SLARCHIVE_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $1}' ) )
	if [[ -z "${pids+x}" ]] ; then 
		printf "${callingFrom}: slarchive is ${DEFAULT_MODULE_STATE_DOWN}.\n" 
	else 
		((nPids=${#pids[@]}, maxIndex=nPids - 1)) 	# get the number of procesess
		[[ $nPids -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		printf "${callingFrom}: ${nPids} instance${s} of slarchive (${pids[*]})\n"
	fi

	# get an array of pids for instances of slinksweeper
	pids=( $(ps  | grep "${SLINKSWEEPER_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $1}' ) )
	if [[ -z "${pids+x}" ]] ; then 
		printf "${callingFrom}: slinksweeper is ${DEFAULT_MODULE_STATE_DOWN}.\n" 
	else 
		((nPids=${#pids[@]}, maxIndex=nPids - 1)) 	# get the number of procesess
		[[ $nPids -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		printf "${callingFrom}: ${nPids} instance${s} of slinksweeper (${pids[*]})\n"
	fi
	
	InboundInfo

}

#############
# InboundInfo() -- generate a formatted report on the contents of the inbound directory
# !!! NB This depends heavily on how slarchive formats the directory structure (SLINK_FILE_STRUCTURE).
# !!!    So if you change SLINK_FILE_STRUCTURE, you'll have to rewrite the parameter expansion parts of this function!
#############
function InboundInfo () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	printf "${callingFrom}: Collecting stats from inbound directory (this may take a moment)..."
	local inbound="${MODULE_DIR}/${DEFAULT_SLINK_INBOUND_DIR}"
	local dirs=($(find "${inbound}" -depth 2 -type d | sort))
	local nDirs=${#dirs[@]}
	if [[ $nDirs -le 0 ]] ; then
		printf "\n${callingFrom}: *** Inbound directory is empty (${inbound})\n"
		return
	fi
	printf "(OK)\n"
	
	printf "${callingFrom}: Summary of files in the inbound directory (${inbound}):\n"
	local fmt="%10s %10s %10s %6s %20s %20s"
	printf "${fmt}\n" "Network" "Station" "#files" "MB" "Age of newest" "Age of oldest"
	
	local nTotal=0
	local sizeTotal=0
	for d in "${dirs[@]}" ; do
		local sta="${d##*/}" # get the part after the last '/'
		local net="${d%/*}" #strip off the part after the last '/' 
		net="${net##*/}" # get the part after the last '/' of that
		local files=( $(find "${d}" -name \*.miniseed  | sort ) )
		local nFiles=${#files[@]}
		local size=$(du -m $d | awk '{print $1}' )		
		local oldest=${files[0]}
		local newest=${files[@]:(-1)}
		local oAge=$(getNiceFileAge $oldest)
		local nAge=$(getNiceFileAge $newest)
		printf "${fmt}\n" $net $sta $nFiles $size "${nAge}" "${oAge}"
		nTotal=$(( $nTotal + $nFiles )) 
		sizeTotal=$(( $sizeTotal + $size )) 
	done
	
	printf "${fmt}\n" "" "Total" $nTotal $sizeTotal "" ""
	
}

#############
# Slink_restart() -- restart 
# Optional argument(s) are words describing the reason
#############
function Slink_restart () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	SourceConfig $callingFrom
	
	local logtext="${callingFrom}: restarting ${MODULE_NAME}"
	if [[ $# -ge 1 ]] ; then # if there are args
		local reason="$@"
		LogException "${callingFrom}: ${reason}"
		logtext="${logtext} (Exception: ${reason})" # append the exception description to the logtext
	fi
	LogMe "${MODULE_LOGFILE}" "${logtext}"

	# (NB: Don't pass the args (the reason) on to the other programs)
	if ModuleIsRunning slink ; then
		Slink_stop
	fi
	Slink_start
}


#############
# Slink_start() -- Launch the SeedLink data archiver to pull realtime data from IRIS, etc.
#############
function Slink_start () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	local s isare errCount=$(Slink_val count)
	if [[ $errCount -gt 0 ]] ; then
		[[ $errCount -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
		printf "${callingFrom}: *** ${MODULE_NAME} config has ${errCount} error${s}. Can't start.\n"
		Slink_val "${MODULE_CONFIG_FILE}" verbose 
		exit
	fi
	SourceConfig $callingFrom

	# Check that the module isn't already running
	ModuleIsRunning $MODULE_TYPE && { 
		printf "${callingFrom}: *** $MODULE_TYPE module is already running. Can't start.\n" ; 
		exit ;
	}	

	# Check that there's a .screenrc in the program dir
	if [[ ! -e "${MODULE_SCREENRC_FILE}" ]] ; then
		printf "${callingFrom}: *** Missing ${DEFAULT_MODULE_SCREENRC_FILE} file. Can't start ${MODULE_NAME}.\n"
		exit
	fi
	
	RotateLogFiles "${MODULE_LOGFILE}" "${MODULE_SCREEN_LOGFILE}" "${MODULE_EXCEPTION_LOGFILE}"
	LogBreak "${MODULE_LOGFILE}" "${callingFrom}"
	
	local s isare nServers="${#SERVERS[@]}"
	[[ $nServers -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: Attempting to connect to ${nServers} SeedLink data server${s}..."

	local index=0 streamListFile pids screenSessionName tmp
	for server in "${SERVERS[@]}" ; do
		streamListFile="${MODULE_DIR}/${STREAMLISTS[${index}]}"
		tmp="${STREAMLISTS[${index}]}"	# e.g., "jamaseis.txt"
		screenSessionName="${SLARCHIVE_SCREEN_ID}-${tmp%.txt}"  # i.e., slarchive-jamaseis
		# Make sure this one isn't already running
		pids=( $(ps | grep "slarchive -l ${streamListFile}" | grep -v grep | awk '{print $1}' ) )
		if [[ -z "${pids+x}" ]] ; then 
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Launching slarchive (screen id '${screenSessionName}')..."
			screen -c "${MODULE_SCREENRC_FILE}" -L -d -m -S "${screenSessionName}" \
				"${BIN_SLARCHIVE}" -l "${streamListFile}" -A "${MODULE_DIR}/${DEFAULT_SLINK_INBOUND_DIR}/${SLINK_FILE_STRUCTURE}" "${server}"
		else
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: 'slarchive -l ${streamListFile}' is already running (${pids}). Skipping..."
		fi
		((++index))
	done

	# FIXME: 20190227 I added this sleep to give slink a chance to start up before the next ps command...
	# FIXME: (Sometimes slinksweeper isn't starting up as expected)
	printf "...[wait]..."
 	sleep 1
	printf "\n"
 	
	# If an instance of slarchive is running and sending files to the inbound directory, then start a slinksweeper
	pids=( $(ps | grep "${SLARCHIVE_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $1}' ) )
	if [[ ! -z "${pids+x}" ]] ; then 		# slarchive is running, so consider starting a slinksweeper...
		# is there a slinksweeper already running in this inbound dir?
		pids=( $(ps | grep "${SLINKSWEEPER_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $1}' ) )
		if [[ -z "${pids+x}" ]] ; then 		# a slinksweeper is NOT running, so start one...
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: Launching slinksweeper (${MODULE_DIR}/${DEFAULT_SLINK_INBOUND_DIR} > ${KEEPDAYS} days)..."
			screen -c "${MODULE_SCREENRC_FILE}" -L -d -m -S "${SLINKSWEEPER_SCREEN_ID}" \
							/bin/bash -fc "${SLINKSWEEPER_PROCESS_SIGNATURE}"
		else
			LogMe "${MODULE_LOGFILE}" "${callingFrom}: slinksweeper already running (${pids}). Skipping..."
		fi
	fi
	
}

#############
# SlinkSweeper() -- Periodically clean up (delete) old files from the inbound directory.
#############
function SlinkSweeper () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	SourceConfig $thisFunction	
	
	LogMe "${MODULE_LOGFILE}" "${thisFunction}: Starting up."
	LogMe "${MODULE_LOGFILE}" "${thisFunction}: Will poll '${MODULE_DIR}/${DEFAULT_SLINK_INBOUND_DIR}' every ${DEFAULT_SLINK_SWEEP_INTERVAL} seconds."
	LogMe "${MODULE_LOGFILE}" "${thisFunction}: Will delete miniseed files found there older than ${KEEPDAYS} days."
	while : ; do 
		LogMe "${MODULE_LOGFILE}" "${thisFunction}: Wakey wakey! Time to clean up..."
		RemoveExpiredInboundFiles $thisFunction
		LogMe "${MODULE_LOGFILE}" "${thisFunction}: Going back to sleep for ${DEFAULT_SLINK_SWEEP_INTERVAL} seconds..."
		sleep $DEFAULT_SLINK_SWEEP_INTERVAL
	done
}


function RemoveExpiredInboundFiles () {
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	local s isare nFiles=`(find "${MODULE_DIR}/${DEFAULT_SLINK_INBOUND_DIR}" -type f -name \*miniseed -mtime +${KEEPDAYS}d -depth -print | wc -l | sed -e 's/^[[:space:]]*//')`
	[[ $nFiles -eq 1 ]] && { s="" ; isare="is" ; } || { s="s" ; isare="are" ; } # pluralizer
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: Found ${nFiles} expired file${s} (> ${KEEPDAYS} days old)."
	if [[ $nFiles -gt 0 ]] ; then
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Deleting ${nFiles} expired file${s}..."
		find "${MODULE_DIR}/${DEFAULT_SLINK_INBOUND_DIR}" -type f -name \*miniseed -mtime +"${KEEPDAYS}"d -delete
	fi
}



#############
# Slink_stop() -- Stop the SeedLink data archiver and the slinksweeper
#############
function Slink_stop () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 
	
	if ! ModuleIsRunning ${MODULE_NAME} ; then
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_NAME} not running. Nothing to stop."
		return
	fi


	# Kill slarchive, slinksweeper and any of their children
	# collect into an array all the  ppid's of any running instances of slarchive, slinksweeper
	
	local ppid_slarchive=( $(ps -O ppid | grep "${SLARCHIVE_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $2}') )
	local ppid_slinksweeper=( $(ps -O ppid | grep "${SLINKSWEEPER_PROCESS_SIGNATURE}" | grep -v grep | awk '{print $2}') )
	local ppidAll=( ${ppid_slarchive[@]}  ${ppid_slinksweeper[@]} ) 
	local ppidUnique=($(printf "%s\n" "${ppidAll[@]}" | sort -u)); # strip duplicates (https://stackoverflow.com/a/13648438/5011245)
	
	local s1 isare1 s2 isare2
	local n1="${#ppid_slarchive[@]}"
	[[ $n1 -eq 1 ]] && { s1="" ; isare1="is" ; } || { s1="s" ; isare1="are" ; } # pluralizer
	local n2="${#ppid_slinksweeper[@]}"
	[[ $n2 -eq 1 ]] && { s2="" ; isare2="is" ; } || { s2="s" ; isare2="are" ; } # pluralizer
		
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: Killing $n1 instance${s1} of slarchive, $n2 SlinkSweeper${s2}..."

	local p
	for p in "${ppidUnique[@]}" ; do
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: Killing process group ${p}..."
		kill -TERM -"$p"  # (the '-' before $p kills all descendants)
	done

	if  ModuleIsRunning ${MODULE_NAME} ; then
		LogMe "${MODULE_LOGFILE}" "${callingFrom}: *** There was a problem stopping slarchive or SlinkSweeper. Try 'status'."
		return
	fi
	
	LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_NAME} stopped. (OK)"

}

#############
# Slink_ver() -- print the version number of the installed scripts
#############
function Slink_ver () { 
	Version $@
}




#############
# Slink_clean() -- tidy up the files
#############
function Slink_clean () { 
	local callingFrom=$(TracePath)
	local thisFunction="$( basename ${BASH_SOURCE[0]} )::${FUNCNAME[0]}"	# the name of this function 

	if ModuleIsRunning slink ; then
		printf "${callingFrom}: ${MODULE_NAME} is running. Can't clean. Run '${MODULE_NAME}/stop', then try again.\n"
		exit
	fi
	
	SourceConfig $callingFrom 
	cd "${MODULE_DIR}" &&  LogBreak "${MODULE_LOGFILE}" "${callingFrom}"
	local mBytesBefore="$(du -cm | tail -1 | awk '{print $1}')" # get the current disk space usage

	RemoveExpiredInboundFiles $callingFrom
	
	printf "${callingFrom}: "
	DeleteAllExcept 8675309 ${MODULE_PROTECTED_FILES[*]} 	# delete all but the protected files
	find "${MODULE_WORKSPACE_DIR}" -depth 1 -type f -delete  # clean out the workspace

	local mBytesAfter="$(du -cm | tail -1 | awk '{print $1}')" # get the current disk space usage
	local mBytesCleaned=`expr $mBytesBefore - $mBytesAfter`
	printf "${callingFrom}: Deleted ${mBytesCleaned} MB. (OK)\n"
	RotateLogFiles "${MODULE_SCREEN_LOGFILE}"  "${MODULE_EXCEPTION_LOGFILE}"
	# (don't rotate "${MODULE_LOGFILE}" here; UpdateModule's RecreateModule will take care of that)

	UpdateModule nogit && LogMe "${MODULE_LOGFILE}" "${callingFrom}: ${MODULE_TYPE} module cleaned. (OK)"
}

#############
# Slink_peek() -- view  screen.log
#############
function Slink_peek () { 
	Module_peek $@
}
