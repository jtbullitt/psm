#!/bin/bash -f


ANSICOLORS=(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15)
COLORNAMES=(Black Maroon Green Olive Navy Purple Teal Silver \
	Gray Red Lime Yellow Blue Fuchsia Aqua White)

# returns 0 if the arg is a valid ANSI color number
function isColorValid() {
	# Is it a number?
	local re='^[0-9]+$'
	if ! [[ $1 =~ $re ]] ; then
 		return 1
 	fi
 	# is it in a reasonable range?
	if [[ $1 -lt 0 ]] || [[ $1 -ge ${#ANSICOLORS[@]} ]] ; then 
		return 1
	fi

	return 0 # ok!
}

# set_fgColor() -- set the Terminal's foreground color
function set_fgColor() {
	# if no color specified
	if [[ $# -ne 1 ]] ; then 
		printf "" 
		return
	fi
	
	# if color is not in the allowed range
	if [[ $1 -lt 0 ]] | [[ $1 -ge ${#ANSICOLORS[@]} ]] ; then
		printf "" 
		return
	fi
	
#	printf "\e[38;5;${1}m\n"
	osascript -e "tell application \"Terminal\" to set normal text color of window 1 to $(ANSI_to_AppleScript $1)"
}

# set_bgColor() -- set the Terminal's foreground color
function set_bgColor() {
	# if no color specified
	if [[ $# -ne 1 ]] ; then 
		printf "" 
		return
	fi
	
	# if color is not in the allowed range
	if [[ $1 -lt 0 ]] | [[ $1 -ge ${#ANSICOLORS[@]} ]] ; then
		printf "" 
		return
	fi
	
#	printf "\e[48;5;${1}m\n"
	osascript -e "tell application \"Terminal\" to set background color of window 1 to $(ANSI_to_AppleScript $1)"
}

# TerminalTheme() -- set the Terminal's foreground and background colors
function TerminalTheme() {
  	if ! SittingAtConsole ; then # if we're in an ssh session 
		printf "*** Warning: You appear to logged into this pod via ssh. Unable to change the Terminal color theme remotely.\n"
		printf "*** Warning: To change the theme, either run this program from the pod's console, or edit TERMCOLORS in pod.conf and login again.\n"
		return 1
 	fi
 	 	

	if [[ $# != 2 ]] ; then 
		printf "*** ${FUNCNAME[0]}: Bad arg count ($#).\n"
		exit
	fi
	
	if ! isColorValid $1 ; then
		printf "*** ${FUNCNAME[0]}: invalid foreground color \"$1\".\n"
		return 1
	fi
	if ! isColorValid $2 ; then
		printf "*** ${FUNCNAME[0]}: invalid background color \"$2\".\n"
		return 1
	fi
	if [[ $1 -eq $2 ]] ; then
		printf "*** ${FUNCNAME[0]}: foreground and background colors must differ.\n"
		return 1
	fi
	
	set_fgColor $1
	set_bgColor $2
	return 0
}

# colorTable() -- print a table of all possible terminal color combinations
function colorTable() {
	local vlabel="   FOREGROUND"
	local b f c
	local fCount=0
	
	printf "Here are the available Terminal color combinations:\n\n"
	printf "                                       B A C K G R O U N D  C O L O R\n"
	printf "    "
	for b in "${ANSICOLORS[@]}" ; do
		printf "%5d  " $b
	done
	printf "\n"
	printf "     "
	for c in "${COLORNAMES[@]}" ; do
		printf "%7s" "${c:0:6}"
	done
	printf "\n"
	for f in "${ANSICOLORS[@]}" ; do
		printf "%1s %3d " "${vlabel:$fCount:1}" $f
		(( fCount++ ))
		for b in "${ANSICOLORS[@]}" ; do
			printf "\e[38;5;${f}m\e[48;5;${b}m  abc \e[0m "
		done
		printf "\n"
	done
	printf "\n"
}


# A crude mapping of ANSI color numbers to Applescript colors
# See https://jonasjacek.github.io/colors/
function ANSI_to_AppleScript() {
	local rgb=()
	if [[ $# -lt 0 ]] || [[ $# -gt 15 ]] ; then
		rgb=(192 192 192) 	# Silver
	else
		case $1 in
			0) rgb=(0 0 0) ;; 		# Black
			1) rgb=(128 0 0) ;; 	# Maroon
			2) rgb=(0 128 0) ;; 	# Green
			3) rgb=(128 128 0) ;; 	# Olive
			4) rgb=(0 0 128) ;; 	# Navy
			5) rgb=(128 0 128) ;; 	# Purple
			6) rgb=(0 128 128) ;; 	# Teal
			7) rgb=(192 192 192) ;; # Silver
			8) rgb=(128 128 128) ;; # Gray
			9) rgb=(255 0 0) ;; 	# Red
			10) rgb=(0 255 0) ;; 	# Lime
			11) rgb=(255 255 0) ;; 	# Yellow
			12) rgb=(0 0 255) ;; 	# Blue
			13) rgb=(255 0 255) ;; 	# Fuchsia
			14) rgb=(0 255 255) ;;  # Aqua
			15) rgb=(255 255 255) ;; # White
			*) rgb=(192 192 192) ;;	# Silver
		esac
	fi
		
	local as="{$(( ${rgb[0]} * 257 )), $(( ${rgb[1]} * 257 )), $(( ${rgb[2]} * 257 )), 0}"
	printf "$as"
}

