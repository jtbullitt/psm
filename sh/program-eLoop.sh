#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
###############################################	
# 
# 20181018 This script replaces the previous script 'esr.sh'. 
# It refactors the main loop as a function that's executed from a launch script in the program's directory.
# It executes a never-ending loop that:
# 	+ launches esound to generate an audio file of archived seismic data, looking back a fixed number of hours (typically, one week) 
# 	+ filters, normalizes, and otherwise prepares the audio for playback [sox ...]
# 	+ creates a spectrogram of the program [sox ... and ImageMagick's 'convert']
#	+ gets a lists of recent events from USGS and annotates the spectrogram accordingly
#	+ uploads the spectrogram to the web server [sftp...]
# 	+ plays the audio [play...]
# 	+ plays other short voice announcements [say...]
#	+ creates a nightly archive of the audio and associated files, and uploads them to the server
#	+ uploads a "factoid" file that contains runtime information about this program and the server's settings
#	See changelog.txt
###############################################	

# NB The calling script has already source()'ed "program-functions.sh", so no need to source() it again
# (see Program__launch() in program-functions.sh)
source "${SCRIPTS_DIR}/program-eLoop-functions.sh" # get the eLoop functions

# N.B. Variables in ALL_CAPS are treated as global variables, available to eLoop's assorted functions
SCRIPT_ID="eloop"	# a handy name for this script

function eLoopFail () {
	local msg="$@"
	cat << EOT

*** esound loop launch failed.
*** $msg
*** Note: This command is not intended to be invoked from the command line. 
    Usage: $(basename $0) RUNKEY [arg]
    Launches a continuous esound program loop based on the given config file.
    First arg is the "run key" (kinda like a password, but simply to discourage command line invocation).
    If a second optional arg is provided, execute one pass, then exit.	


EOT
	exit
}


# eLoopLaunch() -- the main loop for generating and playing successive esound audio files
# 
# This is invoked by program-functions::Program__launch(), which is in turn invoked from _launch.sh
#
# Arguments:
# 	1st argument must be the DEFAULT_PROGRAM_RUN_KEY, a pseudo-passkey to prevent accidental 
#  		command-line launches 
#	Remaining arguments are in the form of VARIABLE=VALUE 
#
# 	Example:
#		eLoopLaunch lhcfirefly PLAYCHIME=NO  
#
#			PLAYCHIME=YES|NO <== overrides config setting for PLAYCHIME (to control whether to play the
#							audio id and chimes (Default = YES)
#			ENDTIME=xxxxxx <== force-sets the reference time to the given time
#					(This implies that LOOPMAX=1; i.e., esound runs once, then quits_)
#						Default = Now
#			PLAYOUT=YES|NO	<== play the audio (used by Play_syncstart to control audio playback)
#						Default = YES
#
#			SKIP_GRAPHICS=YES|NO	<== generate the graphics (spectrogram, etc.)
#						Default = YES
#
function eLoopLaunch() {
	
	if [[ $# -lt 1 ]]  ; then
		eLoopFail "Wrong number of arguments (${#}). Program run key is missing." 
	fi

	if [[ "$1" != "${DEFAULT_PROGRAM_RUN_KEY}" ]] ; then 	# is the run key correct?
		eLoopFail "Bad run key (${1})." 
	fi
	
	# make sure we have all the required executables and libraries
	CheckDependenciesOrDie


	# load config and override any params in the arg list
	PLAYCHIME="YES" # by default, play the friggin chime at the start of the loop
	PLAYOUT="YES"	# by default, play each audio file after generating it
	SKIP_GRAPHICS="NO" # by default, generate the spectorgram, etc
	LoadConfigOrDie
	printf "${SCRIPT_ID}: Config file looks OK.\n" 
	OverrideConfig $@	

	# set maxLoopCount
	local maxLoopCount=-1		# by default, run forever
	if [[ ! -z ${LOOPMAX+x} ]] ; then # if LOOPMAX is set
		maxLoopCount=$LOOPMAX
		[[ $LOOPMAX -le 0 ]] &&  maxLoopCount=-1 # run forever
	fi
	# if ENDTIME is set, then we always do just one pass
#	[[ ! -z ${ENDTIME+x} ]] && maxLoopCount=1 
# 20230527 commented out the above line to allow loop to continue -- it's useful for testing
	#################


	# get an ALL CAPS version of the program name
	local programALLCAPS=$(echo "$MODULE_NAME" | tr '[:lower:]' '[:upper:]')

	################################################
	# BEGIN GENERAL PARAMETERS
	################################################

	local assetsDir="${MODULE_DIR}/${DEFAULT_MODULE_ASSETS_DIR}" 	# where the shell scripts and prerecorded bits are kept
	local workDir="${MODULE_DIR}/${DEFAULT_MODULE_WORKSPACE_DIR}" 	# where the audio and spectrogram files will be written
	local archDirTop="${MODULE_DIR}/${DEFAULT_MODULE_ARCHIVE_DIR}" 	# where archived files go (year-by-year subdirectories will be created, as needed, inside this)

	local eventOverlayLogFile="${MODULE_DIR}/${DEFAULT_PROGRAM_EVENTLOG}" 	# the local event log file
	local chime="${assetsDir}/intervalSignal.wav"			# the beep between segments
	local timeAnnouncementAudioGain=-6  	# gain correction (in dB) when playing the time signal (the default is too loud, so turn it down a hair)
	local o_latestSpectrogram="${workDir}/_LatestSpectrogram.jpg" 

	# auxiliary files, generated by this and that
	local o_timeAnnouncementAudioFile="${SCRIPT_ID}.timeTmp.aif"	# audio file containing the audio time announcement (generated on the fly)
	local o_eventOverlayGnuplotScript="gp-overlay.txt" 	# the gnuplot script (generated by EventOverlay)
	local o_eventOverlayImage="eventOverlay.png" 	# generated output from EventOverlay

	local hourForArchiving=0	# the hour of the day in which to do the nightly archive

	local doMakeMP3Script="${SCRIPTS_DIR}/eLoop-makeMP3.sh" 		# the script to create an mp3 version
	local doMakeRMSScript="${SCRIPTS_DIR}/eLoop-makeRMS.sh" 		# the script to create a file with the RMS value
	#doUploadScript="${SCRIPTS_DIR}/esound-upload.sh" 	# the script to upload stuff to the website

	local maxLoopIndex=10 # the maximum number of temporary programs to keep around in the workDir (old ones will be overwritten by new)

	# voice synth parameters
	local speakRate=200 
	local voiceForTime="samantha"  # voice for speech synthesizer (other good ones: daniel, alex)
	local speakDateFormat="+  %H hours, %M minutes, Co-ordinayted Universal Time. "   # "+%l %M %p " 	

	################################################
	# END OF GENERAL PARAMETERS
	################################################




	################################################
	# BEGIN TESTS TO SEE THAT WE HAVE ALL THE NEEDED FILES AND PERMISSIONS
	################################################
	#
	# confirm that the required directories are present
	local assets=( "$assetsDir" "$archDirTop" "$workDir")
	local fatalErrorCount=0
	for asset in "${assets[@]}" ; do
		if [ ! -e "${asset}" ] ; then
			printf "${SCRIPT_ID}: Can't find project directory: ${asset}\n"
			(( fatalErrorCount++ ))
		fi
	done
	if [ $fatalErrorCount -gt 0 ] ; then
		printf "${SCRIPT_ID}: Directory '${MODULE_DIR}' does not have valid architecture for a PSM program.\n"
		exit
	fi
	printf "${SCRIPT_ID}: Found all required directories (${#assets[@]}).\n"

	# test that we can create an audio time stamp file
	touch "${workDir}/${o_timeAnnouncementAudioFile}"	


	# gather up the required "assets" into an array in order to check that they're all present and accounted for
	local assets=("$MODULE_DIR" "$archDirTop" "$assetsDir" "$workDir" \
			"$chime" \
			"$doMakeMP3Script" "$doMakeRMSScript" \
			"$eventOverlayLogFile" \
			"$MODULE_CONFIG_FILE" \
			)

	# confirm that all the assets exist and are available
	local fatalErrorCount=0
	for asset in "${assets[@]}" ; do
		if [ ! -e "$asset" ] ; then
			printf "${SCRIPT_ID}: Can't find asset: ${asset}\n"
			(( fatalErrorCount++ ))
		fi
	done

	[[ $fatalErrorCount -eq 1 ]] && s="" || s="s" # pluralizer
	if [ $fatalErrorCount -gt 0 ] ; then
		SayAndDie "${SCRIPT_ID}: $fatalErrorCount missing asset${s}."	
		exit
	fi
	printf "${SCRIPT_ID}: Found all required assets (${#assets[@]}).\n"

	printf "${SCRIPT_ID}: Found all parameters for program '${MODULE_NAME}'.\n"

	local s
	[[ $maxLoopCount -eq 1 ]] && s="" || s="es" # pluralizer
	if [[ $maxLoopCount -gt 0 ]] ; then
		 printf "${SCRIPT_ID}: Will execute ${maxLoopCount} pass${s} through the main loop, then quit.\n"
	else 
		printf "${SCRIPT_ID}: Will run indefinitely.\n"
	fi
	
	# From here on, exit immediately upon failure of a command. This is to prevent the upcoming while() loop
	# from running wild while doing monumentally malicious mischief
	# BUT it means that the slightest little error will make everything stop, often with no error message.
	# So if things should halt inexplicably, start debugging by commenting out the following line.
	set -e 

	# WE NOW HAVE ALL THE NEEDED FILES, PERMISSIONS, FUNCTIONS, and PARAMETERS
	cd "$workDir"		# this is where all the generated files will be written

	##############################################
	# 	A MINEFIELD LURKS BELOW. 
	# 	TINKER AT YOUR OWN RISK.
	##############################################
	export TZ=UTC 	# We use UTC time zone for these broadcasts 
	local loopCount=0 	# a counter of the number of times through the main program loop
	local loopIndex=0 	# an integer that's inserted in the file names; like loopCount, but it resets to 1 every $maxLoopIndex times
	# ready, get set...

	if [[ "${PLAYCHIME}" = "YES" ]] ; then
		"${BIN_SOX}" -q "${chime}" -t coreaudio "${AUDIO_DEVICE}"		# play the chime to announce "hello!"
		local spellName="$(echo ${MODULE_NAME} | sed -e 's/\(.\)/\1,/g')"	# separate the letters with commas
		say -r $speakRate -v $voiceForTime "Starting audio stream, ${spellName}. Please stand by." -o "${workDir}/hello.aiff"
		"${BIN_SOX}" -q "${workDir}/hello.aiff"  -t coreaudio "${AUDIO_DEVICE}" gain "${timeAnnouncementAudioGain}" &
	fi

	local startTimeEpoch=$(date -j +%s) #save the start time (now)
	local previousDay=$(date -j +%Y%j)	# init the "previous day" as today (e.g., 2017123)
	# When eLoop starts, it saves the julian date, calling it the "previous" day. As time marches on in the loop below,
	# we'll compare the current time/date to this "previous" day, to detect when we cross into a new day.

	##############################################
	# MAIN PROGRAM LOOP 
	while [[ $loopCount -lt $maxLoopCount || $maxLoopCount -le 0 ]] ; do
	
		(( loopCount++ ))	# increment the master program counter (continues on to infinity)
		(( loopIndex++ ))	# increment the program index (continues to maxLoopIndex, then resets)
		if [ $loopIndex -ge $maxLoopIndex ] ; then 	# if we've generated more than this many program files, then 
			loopIndex=1;		# reset the program index counter
		fi

		local hour=$(date  "+%H" | sed -e "s/^0//") # get the hour (and strip a leading zero, if any)
	
		#############################
		# The user is free to change config file parameters on the fly, while this script is runnning.
		local prev_SHOUTCAST=$SHOUTCAST  # save the previous SHOUTCAST param before erasing and re-loading the config params
	
# DEBUG 20221006 : I commented out the following command. We don't need to 'source' the config file again.
#	It seems unnecessary to respond on-the-fly to changes in the config file.
#		LoadConfigOrDie	# load the config file
#		OverrideConfig $@	

		# If butt isn't running, but SHOUTCAST is non-empty, then start up butt.
		# If butt is running, then stop butt if SHOUTCAST is now empty, or restart butt if SHOUTCAST changed
		if BUTTisRunning ; then   # if butt is running...
			if [[ -n ${SHOUTCAST} ]] ; then # if SHOUTCAST is set
			 	[[ ${SHOUTCAST} != ${prev_SHOUTCAST} ]] && ${MODULE_DIR}/buttctl restart  # if SHOUTCAST changed, then restart butt
			else
				${MODULE_DIR}/buttctl stop # SHOUTCAST is now empty, so stop butt
			fi
		else # butt is not running
			[[ -n ${SHOUTCAST} ]] && ${MODULE_DIR}/buttctl start
		fi
	
		# construct from useful variables from the config params
		local referenceTime="$(GetReferenceTime)"
		
		
		# Convert referenceTime into a nice date string to be displayed in the spectrogram
		# Example:
		#	referenceTime = 20160628000001
		#	referenceTimeEnglish = Tue Jun 28 00:00:01 UTC 2016
		local referenceTimeEnglish=$(date -j -f "%Y%m%d%H%M%S" $referenceTime "+%a %b %d %T %Z %Y")
		local longTime="${referenceTime: 0:12}"	# keep the first 12 chars of the time  201606280000
		local yyyyddd=$(date -j -f "%Y%m%d%H%M%S" $referenceTime "+%Y%j")	# get the YYYYJJJ from the endTime  -- e.g., 2016123
		local fileNameHead="${MODULE_NAME}.${longTime}.${yyyyddd}"	# the main part of the filenames generated by this program
		local yyyy=${yyyyddd: 0:4}	# year
		local ddd=$(expr ${yyyyddd: 4:3} + 0)	# day (expr ... + 0 converts it to an integer -- i.e., strips any leading 0's)
		local timeWindowHours="-${HOURS}" # note the negative hours (looking backward in time)

		# if the program is shorter than a day, then we'll have to build a special day-long archive
		local doSpecialArchive=false
		local archiveDuration="${HOURS}"
		if [[ $HOURS -lt 24 ]] ; then
			doSpecialArchive=true
			archiveDuration=24 # the minimum duration of an archived program
		fi 		
		# construct the program-specific host directory names
		local webhostArchiveDir="$(GetWebhostArchiveDir)/${MODULE_NAME}"	# e.g., "earthsound.org/archive/data/phme1a
		local webhostInboundDir="$(GetWebhostInboundDir)/${MODULE_NAME}"	# e.g., "earthsound.org/unver/inbound/phme1a"
	
		# The name of the archive files are based on YESTERDAY's date. 
		# For example, if it's currently 00:03 on 2016123, we're going to create archive files
		# whose names contain "2016122". It gets tricky when we cross into a new year. For example,
		# if today is "2017001", then we have to do a little leap-year checking to find that yesterday
		# was "2016366".

		# If we're approaching New Year's, then issue a warning about creating a new directory on the website
		if [[ $ddd -gt 360 ]] ; then 
			(( nextYear = yyyy + 1 )) # the next year
			cat << EOT
${SCRIPT_ID}: *******************************************
${SCRIPT_ID}: *** HAPPY NEW YEAR! HAVE YOU CREATED A NEW ARCHIVE FOLDER ON YOUR REMOTE WEB HOST?
${SCRIPT_ID}: *** TO SAVE ARCHIVES FOR ${nextYear}, YOU NEED TO DO THIS: "mkdir ${webhostArchiveDir}/${nextYear}"
${SCRIPT_ID}: *******************************************

EOT
		fi
	
		# If today is January 1, then do some leap-year checking
		local yyyyYesterday dddYesterday yyyydddYesterday
		if [ $ddd -eq 1 ] ; then
			(( yyyyYesterday = yyyy - 1 )) # the previous year
			if (( ("$yyyyYesterday" % 400) = "0" )) || (( ("$yyyyYesterday" % 4 = "0") && ("$yyyyYesterday" % 100 != "0" ) )); then
				dddYesterday=366
			else
				dddYesterday=365
			fi
		else # If it's not January 1, then just decrement the day-of-year
			yyyyYesterday=$yyyy
			(( dddYesterday = ddd - 1 )) # the previous day
		fi
		dddYesterday=$(printf "%03d" "${dddYesterday}") # convert the integer to a zero-padded string (e.g., "013" for January 13)
		yyyydddYesterday="${yyyyYesterday}${dddYesterday}"	# e.g., 2016123 or 2016366
		local fileNameHeadForArchive="${MODULE_NAME}.${yyyydddYesterday}"	# e.g., phme1.2016123
		local archDir="${archDirTop}/${yyyyYesterday}"	# the name of the archive directory (e.g., /foo/bar/phme1/2016)
		
		# if the archive directory doesn't already exist on the local server, then create it
		if [ ! -e "$archDir" ] ; then
			mkdir "$archDir"
			if [ $? -ne 0 ] ; then # if mkdir threw an error...
				printf "Unable to create directory for archive: ${archDir}\n"
				SayAndDie "Rats! PSM program ${MODULE_NAME} crashed."
			fi
		fi
		
		local nowTimeEpoch=$(date +%s)
		local elapsedTimeSeconds=$(($nowTimeEpoch - $startTimeEpoch))
		local upTime=$(FormattedDuration ${elapsedTimeSeconds})
		local now=$(date)
		cat << EOT


=========== ${MODULE_NAME} ${SCRIPT_ID} ${now} (up ${upTime}; loop #${loopCount} [${loopIndex}/${maxLoopIndex}]  ${fileNameHead})  ==========
EOT

	
		# set a bunch of parameters for this program (generated output files get the prefix "o_")
		local outfilePrefix="${MODULE_NAME}.${loopIndex}" # e.g., "phme1.73"
		local o_audioRawFile="${outfilePrefix}.raw.aif" ;			# e.g., "phme1.73.raw.aif"
		local o_esoundDiagnosticsFile="${outfilePrefix}.raw.info.txt" ;			# e.g., "phme1.73.raw.info.txt"
		local o_audioPostFile="${outfilePrefix}.post.aif" ;		# e.g., "phme1.73.post.aif"
		local o_spectroPlainFile="${outfilePrefix}.spectro-O.jpg"	# spectrogram overlain with mask -- e.g., "phme1.73.spectro-O.jpg"
		local o_spectroEventsFile="${outfilePrefix}.spectro-OE.jpg"	# spectrogram overlain with mask and events -- e.g., "phme1.73.spectro-OE.jpg"
		local o_eventListFile="${outfilePrefix}.events.txt";		# e.g., "phme1.73.events.txt"
		local o_factoidsFile="${outfilePrefix}.factoids.txt"; 	# e.g., "phme1.73.factoids.txt"
	

		# determine which audio file to use for the spectrogram
		local audioFileForSpectrogram
		local gainCorrectionForSpectrogram
		local spectrogramType
		if [[ ! -z ${SPECTROPOST+x} ]] ;  then # if SPECTROPOST is set
			audioFileForSpectrogram="${o_audioPostFile}"	# use the filtered audio
			gainCorrectionForSpectrogram=0					# do not apply any gain correction to the unfiltered audio (for spectrograms and RMS)
			spectrogramType="post-processed"
		else 
			audioFileForSpectrogram="${o_audioRawFile}"	# use the unfiltered audio
			gainCorrectionForSpectrogram="${DBGAIN}"			# apply a gain correction to the unfiltered audio (for spectrograms and RMS)
			spectrogramType="raw"
		fi
		# collect the SoX audio processing parameters
		local soxPostProcessingParameters="${SOXFILTER} ${SOXCOMPAND} fade h .5 0 .5 gain -n -1"
		
		# create filenames for the archive 
		local o_archive_audioRawFile="${fileNameHead}-raw.aif"
		local o_archive_audioPostFile="${fileNameHead}.aif"
		local o_archive_eventListFile="${fileNameHeadForArchive}-events.txt"
		local o_archive_spectroPlainFile="${fileNameHeadForArchive}.jpg"
		local o_archive_spectroEventsFile="${fileNameHeadForArchive}-events.jpg"
		local o_archive_mp3File="${fileNameHeadForArchive}.mp3"	# e.g., "phme1.2016123.mp3"
		local o_archive_factoidsFile="${fileNameHeadForArchive}-factoids.txt"
		
		# make the info for the mp3 ID3 tags
		local mp3SongName="${programALLCAPS}.${yyyydddYesterday}"	# e.g., "phme1.2016123"
		local spectroTitleCaption="Channel ${programALLCAPS} | ${DESCRIPTION} | ending ${referenceTimeEnglish}"	# e.g., "Channel WRAB1A: Tennant Creek, NT, Australia (seismic) | WRAB (IRIS/IDA) - ending Wed May 10..."
	
		# the first time through, dump the factoids (useful for diagnostics)
		if [ $loopCount -eq 1 ]; then
			MakeFactoids "${o_factoidsFile}"
		fi
	
		# generate the audio files, spectrograms, etc., etc.
		
		#if SKIP_GRAPHICS is set and it's set to YES
		if [[ ( ! -z ${SKIP_GRAPHICS+x} ) && ( "${SKIP_GRAPHICS}" = "YES" ) ]] ; then		
			GenerateProgram SKIP_GRAPHICS # run esound but don't generate the graphics
		else
			GenerateProgram # run esound, etc
		fi

		# if we're in single-pass mode, then do the archiving and quit
### 20221028 -- SUPPRESS ALL THIS FOR NOW. IT'S PROBABLY ONLY USEFUL ONLY WHEN TESTING 
###		if [[ $maxLoopCount -eq 1 ]] ; then
###			printf "${MODULE_NAME}: archiving...\n"
###			# if the program was shorter than 24 hours, then we have to create a full 24-hour-long program for the archive
###			if $doSpecialArchive ; then 
###				AssignSpecial 	# assign special values to the parameters for this archive
###				GenerateProgram
###			fi				
###			BuildArchiveSet
###			printf "${MODULE_NAME}: archived.\n"
###			break;		# break out of the program loop
###		fi
	
		# enqueue the files for future upload to the server
		EnqueueFiles "${workDir}/${MODULE_NAME}.inbound.sftp" \
					"${webhostInboundDir}" \
					"${workDir}/${o_spectroEventsFile}" \
					"${workDir}/${o_eventListFile}" \
					"${workDir}/${o_factoidsFile}"

		# Bring back into the foreground the program that's playing in the background 
		if [[ $loopCount -ne 1 ]] ; then
			(( lastProgram = loopCount - 1 ))
			printf "${MODULE_NAME}: Waiting for program #${lastProgram} to finish playing...\n"
		fi
		wait
	
		# program execution resumes here once all background processess (probably 'play') have finished.
		# Once it has finished, we do some time-related housekeeping, announce the time, play station ids, etc., 
		# before moving on to playing the next program
		# the first program gets special handling
		if [ $loopCount -eq 1 ]; then
			if [[ "${PLAYCHIME}" = "YES" ]] ; then
				"${BIN_SOX}" -q "${chime}" -t coreaudio "${AUDIO_DEVICE}" # play the chime to announce "hello!"
				local timeToSay=$(date -v+6S  "+ %H hours, %M minutes, Coordinated Universal Time." | sed -e "s/ 0/ /g" -e "s/ 1 hours/1 hour/" -e "s/ 1 minutes/1 minute/")
				say -r "$speakRate" -v "$voiceForTime" -o "$o_timeAnnouncementAudioFile" "The time is ${timeToSay}."	# announce the current time and save it to a file
				"${BIN_SOX}" -q "$o_timeAnnouncementAudioFile" -t coreaudio "${AUDIO_DEVICE}" gain "$timeAnnouncementAudioGain"  # play the time stamp sound file, adjusting the gain nicely		
			fi

		else 		# this was not the first program...

			# If we've entered a new day...
			if [[ "$yyyyddd" != "$previousDay" ]] ; then  
				# ...and if it's the wee hours of the morning (< 1:00AM)
				if [[ $hour -eq $hourForArchiving ]] ; then
					(
					printf "${MODULE_NAME}: archiving...\n"

					# if the program was shorter than 24 hours, then we have to create a full 24-hour-long program for the archive
					if $doSpecialArchive ; then 
						AssignSpecial 	# assign special values to the parameters for this archive
						GenerateProgram # run esound, etc.
					fi				
					BuildArchiveSet

					# enqueue the files for future upload to the server
					EnqueueFiles "${workDir}/${fileNameHeadForArchive}.archive.sftp" \
								"${webhostArchiveDir}/${yyyyYesterday}" \
								"${archDir}/${o_archive_eventListFile}" \
								"${archDir}/${o_archive_spectroPlainFile}" \
								"${archDir}/${o_archive_spectroEventsFile}" \
								"${archDir}/${o_archive_mp3File}" \
								"${archDir}/${o_archive_factoidsFile}" 
					
					HouseClean # tidy up from the day's labors
					
					) & # this all happens in the background
	
				else 	# it's a new day, but not the wee wee hours, so this is probably just a reboot; no need to upload
					printf "${MODULE_NAME}: skipped daily archive upload (waiting for hour ${hourForArchiving}).\n"
				fi
	
				previousDay=$yyyyddd; 	# update previousDay
			else  # we didn't enter a new day
				: 	# nothing to do (no-op)
			fi
		
		fi
	
		# If playout is set and it's set to "YES", then play the audio (useful to suppress audio in Play_syncstart)
		if [[ ( ! -z ${PLAYOUT+x} ) && ( "${PLAYOUT}" = "YES" ) ]] ; then	
			# restart butt (in case it got hung for some reason) -- jtb 20230413
			${MODULE_DIR}/buttctl restart
			
			# play out the new program. 
			"${BIN_SOX}" --buffer 100000 -q $o_audioPostFile -t coreaudio "${AUDIO_DEVICE}"  & # play the new program in the background (don't redirect to a file -- audible dropouts occur!)
		
			# N.B.: If we've reached the final pass through the loop, then wait for the playout to finish 
			# before exiting the script. Otherwise, if sox is left in the background, it will die
			# when the script exits.
			if [[ $loopCount -ge $maxLoopCount ]] ; then
				wait	# wait for the playout to finish
			fi		
		fi
	
	done		# lather, rinse, repeat

	# Loop is done. Insert cleanup tasks below.
	${MODULE_DIR}/buttctl stop
	cat << EOT

	++===============================================
	||
	|| ${MODULE_NAME}: ${SCRIPT_ID} exited normally after ${loopCount} loops.
	|| Waiting for child processes to terminate...
	||
	++===============================================

EOT

}