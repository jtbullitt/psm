#!/bin/bash -f
# Copyright (C) 2019 JT Bullitt (GNU General Public License v3.0 -- see LICENSE.txt)
##################
# Upgrade from esound versions 133-135 to psm version 136
##################
ME="$(basename $0)";	# the name of this script
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # the containing dir of this script (http://stackoverflow.com/a/246128/5011245)
PSM_DIR="$( cd ${MY_DIR}/../.. && pwd)"
NEWVERSION=136

####
# StashOldPod() -- save a copy of the current pod/
function StashOldPod() {
	if [[ ! -d ${EARTHSOUND_DIR}/pod ]] ; then
		printf "${ME}: *** ${FUNCNAME[0]} can't find old pod. Can't continue.\n"
		exit
	fi
	
	OLDPOD=${EARTHSOUND_DIR}/pod-${OLDVERSION}-$$
	printf "Stashing old pod to ${OLDPOD}..."
	mv ${EARTHSOUND_DIR}/pod ${OLDPOD} 
	printf "(OK)\n"
}


####
# GetVersion() -- print the version number of the current installation
function GetVersion() {
	if [[ ! -e ${EARTHSOUND_DIR}/pod/ver ]] ; then
		printf "0"
	else
		${EARTHSOUND_DIR}/pod/ver
	fi
}

####
# GetConfig() -- source the pod's config file
function GetConfig() {
	if [[ ! -e ${EARTHSOUND_DIR}/pod/pod.conf ]] ; then
		printf "${ME}: *** ${FUNCNAME[0]} can't find pod.conf. Can't continue.\n"
		exit
	fi	
	source ${EARTHSOUND_DIR}/pod/pod.conf
}


####
# TweakProfile() -- Tweak the user's ~.bashrc
function TweakBashrc() {
	cat << EOT >> ~/.bashrc
	
###### Inserted by TweakProfile on $(date) DELETE AFTER ALL PODS ARE UPGRADED TO v 136 ####
export EARTHSOUND_DIR=/Users/jtb/earthsound # Delete this line after all pods upgraded to v 136
export PSM_PROJECT=/Users/jtb/earthsound # Delete this line after all pods upgraded to v 136
export PATH="$PATH:/Users/jtb/earthsound/psm/bin" # Delete this line after all pods upgraded to v 136
########################

EOT
}

###
# EditUpdater() -- modify a program's updater script so that it will work with the new version
function EditUpdater() {
	if [[ $# -ne 1 ]] ; then 
		printf "*** ${FUNCNAME[0]} expects a single argument.\n"
		exit
	fi
	if [[ ! -e $1 ]] ; then 
		printf "*** ${FUNCNAME[0]} can't find file '$1'. Can't continue.\n"
		exit
	fi
	printf "Editing $1..."
	
	ed -s << EOT $1 
%s+source.*get the functions+source "${PSM_DIR}/sh/program-functions.sh" # get the functions+g
1a
# Modified by ${ME} on $(date -u)
.
w
q
EOT
	printf "(OK)\n"
}

##########
# EditConfigVersion() -- edit a program's config file
# Requires that NEWVERSION and OLDVERSION have already been defined
function EditConfigVersion() {
	if [[ $# -ne 1 ]] ; then 
		printf "*** ${FUNCNAME[0]} expects exactly 1 argument.\n"
		exit
	fi
	local theConfigFile=$1
	if [[ ! -e $theConfigFile ]] ; then 
		printf "*** ${FUNCNAME[0]} can't find file '$theConfigFile'. Can't continue.\n"
		exit
	fi
	printf "Editing $theConfigFile..."
	
	cp "$theConfigFile" "${theConfigFile}-${OLDVERSION}-$$"
	
	ed -s << EOT $theConfigFile 
%s+CONFIGVERSION="..."+CONFIGVERSION=${NEWVERSION}
1a
# Modified by ${ME} on $(date -u)
.
w
q
EOT
	printf "(OK)\n"
}


function Patch136() {
	printf "${ME}: Launching patcher to upgrade to version 136...\n"
	
	if [[ ! -z ${EARTHSOUND_DIR+x} ]] ; then
		printf "Environment variable EARTHSOUND_DIR is already set (OK)\n"
	else
		printf "Environment variable EARTHSOUND_DIR is not set. Can't perform patch.\n"
		exit
	fi

	if [[ -z ${PSM_PROJECT+x} ]] ; then 
		printf "Environment variable PSM_PROJECT is not set (OK)\n"
	else 
		printf "Environment variable PSM_PROJECT is already set. Can't perform patch.\n"
		exit
	fi
	
	local OLDVERSION=$(GetVersion)
	
	if [[ $OLDVERSION -ge 133 ]] || [[ $OLDVERSION -le 135 ]] ; then
		printf "Version $OLDVERSION detected (OK)\n"
	else
		printf "Version detected: ${OLDVERSION}. Can't run patcher.\n"
		exit
	fi
	
	GetConfig
	StashOldPod
	TweakBashrc
	
	${PSM_DIR}/installpod
	
	printf "Copying old pod.conf to new..."
	cp ${OLDPOD}/pod.conf ${PSM_DIR}/../pod/.
	printf "(OK)\n"
	
	for program in "${PROGRAMS[@]}" ; do
		EditUpdater	${EARTHSOUND_DIR}/${program}/update # correct the path to the scripts
		EditConfigVersion ${EARTHSOUND_DIR}/${program}/program.conf #update the version number
	done
	
	export PSM_PROJECT=${EARTHSOUND_DIR} # export the path so that the next command will work
	${EARTHSOUND_DIR}/pod/update nogit # update the pod and all its programs
	
	printf ">>>>>>> To access the updated environment, quit this shell and launch another one. <<<<<<<\n"
}


Patch136  # run it!
